/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/* The block cipher created by me */
#include "mcrypto/cipher/block/cblake.h"
#include "mcrypto/macros.h"
#include <stdio.h>
#include <string.h>

static const uint64_t c240 = UINT64_C(0x1BD11BDAA9FC1A22);

static const uint8_t sigma[12][16] = {
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
  { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
  { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
  { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
  { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
  { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
  { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
  { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
  { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
  { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 },
};

/* we don't use this */
#if 0
static const uint64_t u[16] = {
  UINT64_C(0x243f6a8885a308d3), UINT64_C(0x13198a2e03707344),
  UINT64_C(0xa4093822299f31d0), UINT64_C(0x082efa98ec4e6c89),
  UINT64_C(0x452821e638d01377), UINT64_C(0xbe5466cf34e90c6c),
  UINT64_C(0xc0ac29b7c97c50dd), UINT64_C(0x3f84d5b5b5470917),
  UINT64_C(0x9216d5d98979fb1b), UINT64_C(0xd1310ba698dfb5ac),
  UINT64_C(0x2ffd72dbd01adfb7), UINT64_C(0xb8e1afed6a267e96),
  UINT64_C(0xba7c9045f12c7f99), UINT64_C(0x24a19947b3916cf7),
  UINT64_C(0x0801f2e2858efc16), UINT64_C(0x636920d871574e69)
};
#endif

static const uint8_t t_mix[8][4] = { { 3, 6, 9, 12 },  { 2, 5, 8, 15 },
                                     { 1, 4, 11, 14 }, { 0, 7, 10, 13 },
                                     { 2, 5, 8, 15 },  { 1, 4, 11, 14 },
                                     { 0, 7, 10, 13 }, { 3, 6, 9, 12 } };

/* Mix function, G */
#define G(r, i, a, b, c, d)                                                    \
  {                                                                            \
    a += b + (k[sigma[r % 10][2 * i + 0]]);                                    \
    d = ROTR64(d ^ a, 32);                                                     \
    c += d;                                                                    \
    b = ROTR64(b ^ c, 24);                                                     \
    a += b + (k[sigma[r % 10][2 * i + 1]]);                                    \
    d = ROTR64(d ^ a, 16);                                                     \
    c += d;                                                                    \
    b = ROTR64(b ^ c, 63);                                                     \
  }

#define INV_G(r, i, a, b, c, d)                                                \
  {                                                                            \
    b = ROTL64(b, 63) ^ c;                                                     \
    c -= d;                                                                    \
    d = ROTL64(d, 16) ^ a;                                                     \
    a -= b + (k[sigma[r][2 * i + 1]]);                                         \
    b = ROTL64(b, 24) ^ c;                                                     \
    c -= d;                                                                    \
    d = ROTL64(d, 32) ^ a;                                                     \
    a -= b + (k[sigma[r][2 * i + 0]]);                                         \
  }

/* a round */
#define ROUND(r)                                                               \
  {                                                                            \
    /* mix the tweak into the state */                                         \
    v[t_mix[r % 8][0]] += t[r % 4];                                            \
    v[t_mix[r % 8][1]] += t[(r + 1) % 4];                                      \
    v[t_mix[r % 8][2]] += t[(r + 2) % 4];                                      \
    v[t_mix[r % 8][3]] += t[(r + 3) % 4];                                      \
    /* mix the key and the state */                                            \
    G(r, 0, v[0], v[4], v[8], v[12]);                                          \
    G(r, 1, v[1], v[5], v[9], v[13]);                                          \
    G(r, 2, v[2], v[6], v[10], v[14]);                                         \
    G(r, 3, v[3], v[7], v[11], v[15]);                                         \
    G(r, 4, v[0], v[5], v[10], v[15]);                                         \
    G(r, 5, v[4], v[9], v[14], v[3]);                                          \
    G(r, 6, v[8], v[13], v[2], v[7]);                                          \
    G(r, 7, v[12], v[1], v[6], v[11]);                                         \
  }

#define INV_ROUND(r)                                                           \
  {                                                                            \
    /* mix the key and the state */                                            \
    INV_G(r, 7, v[12], v[1], v[6], v[11]);                                     \
    INV_G(r, 6, v[8], v[13], v[2], v[7]);                                      \
    INV_G(r, 5, v[4], v[9], v[14], v[3]);                                      \
    INV_G(r, 4, v[0], v[5], v[10], v[15]);                                     \
    INV_G(r, 3, v[3], v[7], v[11], v[15]);                                     \
    INV_G(r, 2, v[2], v[6], v[10], v[14]);                                     \
    INV_G(r, 1, v[1], v[5], v[9], v[13]);                                      \
    INV_G(r, 0, v[0], v[4], v[8], v[12]);                                      \
    /* mix the tweak into the state */                                         \
    v[t_mix[r % 8][0]] -= t[r % 4];                                            \
    v[t_mix[r % 8][1]] -= t[(r + 1) % 4];                                      \
    v[t_mix[r % 8][2]] -= t[(r + 2) % 4];                                      \
    v[t_mix[r % 8][3]] -= t[(r + 3) % 4];                                      \
  }

void mcrypto_cblake_key(mcrypto_cblake_ctx *ctx, uint8_t *key)
{
  for (int i = 0; i < 16; i++) {
    uint64_t tmp = LOAD64_LE(key + 8 * i);
    ctx->key[i] = tmp;
  }
}

void mcrypto_cblake_tweak(mcrypto_cblake_ctx *ctx, uint8_t *tweak)
{
  ctx->tweak[0] = LOAD64_LE(tweak);
  ctx->tweak[1] = LOAD64_LE(tweak + 8);
  ctx->tweak[2] = ctx->tweak[0] ^ ctx->tweak[1];

  /* xor the c240 constant. This prevents the tweak being all zeros */
  ctx->tweak[3] = c240 ^ ctx->tweak[2];
}

void mcrypto_cblake_encrypt(mcrypto_cblake_ctx *ctx, uint8_t *msg, uint8_t *out,
                            uint64_t length)
{
  uint64_t v[16], k[16], t[4];

  memcpy(k, ctx->key, 128);
  memcpy(t, ctx->tweak, 32);

  for (uint64_t i = 0; i < length / 128; i++) {
    /* Load the messages into array */
    v[0] = LOAD64_LE(msg);
    v[1] = LOAD64_LE(msg + 8);
    v[2] = LOAD64_LE(msg + 16);
    v[3] = LOAD64_LE(msg + 24);
    v[4] = LOAD64_LE(msg + 32);
    v[5] = LOAD64_LE(msg + 40);
    v[6] = LOAD64_LE(msg + 48);
    v[7] = LOAD64_LE(msg + 56);
    v[8] = LOAD64_LE(msg + 64);
    v[9] = LOAD64_LE(msg + 72);
    v[10] = LOAD64_LE(msg + 80);
    v[11] = LOAD64_LE(msg + 88);
    v[12] = LOAD64_LE(msg + 96);
    v[13] = LOAD64_LE(msg + 104);
    v[14] = LOAD64_LE(msg + 112);
    v[15] = LOAD64_LE(msg + 120);

    /* Run 12 rounds */
    ROUND(0);
    ROUND(1);
    ROUND(2);
    ROUND(3);
    ROUND(4);
    ROUND(5);
    ROUND(6);
    ROUND(7);
    ROUND(8);
    ROUND(9);
    ROUND(10);
    ROUND(11);

    STORE64_LE(out, v[0]);
    STORE64_LE(out + 8, v[1]);
    STORE64_LE(out + 16, v[2]);
    STORE64_LE(out + 24, v[3]);
    STORE64_LE(out + 32, v[4]);
    STORE64_LE(out + 40, v[5]);
    STORE64_LE(out + 48, v[6]);
    STORE64_LE(out + 56, v[7]);
    STORE64_LE(out + 64, v[8]);
    STORE64_LE(out + 72, v[9]);
    STORE64_LE(out + 80, v[10]);
    STORE64_LE(out + 88, v[11]);
    STORE64_LE(out + 96, v[12]);
    STORE64_LE(out + 104, v[13]);
    STORE64_LE(out + 112, v[14]);
    STORE64_LE(out + 120, v[15]);
    msg += 128;
    out += 128;
  }
}

void mcrypto_cblake_decrypt(mcrypto_cblake_ctx *ctx, uint8_t *msg, uint8_t *out,
                            uint64_t length)
{
  uint64_t v[16], k[16], t[4];

  memcpy(k, ctx->key, 128);
  memcpy(t, ctx->tweak, 32);

  for (uint64_t i = 0; i < length / 128; i++) {
    /* Load the messages into array */
    /* we don't use loops. */
    v[0] = LOAD64_LE(msg);
    v[1] = LOAD64_LE(msg + 8);
    v[2] = LOAD64_LE(msg + 16);
    v[3] = LOAD64_LE(msg + 24);
    v[4] = LOAD64_LE(msg + 32);
    v[5] = LOAD64_LE(msg + 40);
    v[6] = LOAD64_LE(msg + 48);
    v[7] = LOAD64_LE(msg + 56);
    v[8] = LOAD64_LE(msg + 64);
    v[9] = LOAD64_LE(msg + 72);
    v[10] = LOAD64_LE(msg + 80);
    v[11] = LOAD64_LE(msg + 88);
    v[12] = LOAD64_LE(msg + 96);
    v[13] = LOAD64_LE(msg + 104);
    v[14] = LOAD64_LE(msg + 112);
    v[15] = LOAD64_LE(msg + 120);

    /* Run 12 rounds */
    INV_ROUND(11);
    INV_ROUND(10);
    INV_ROUND(9);
    INV_ROUND(8);
    INV_ROUND(7);
    INV_ROUND(6);
    INV_ROUND(5);
    INV_ROUND(4);
    INV_ROUND(3);
    INV_ROUND(2);
    INV_ROUND(1);
    INV_ROUND(0);

    STORE64_LE(out, v[0]);
    STORE64_LE(out + 8, v[1]);
    STORE64_LE(out + 16, v[2]);
    STORE64_LE(out + 24, v[3]);
    STORE64_LE(out + 32, v[4]);
    STORE64_LE(out + 40, v[5]);
    STORE64_LE(out + 48, v[6]);
    STORE64_LE(out + 56, v[7]);
    STORE64_LE(out + 64, v[8]);
    STORE64_LE(out + 72, v[9]);
    STORE64_LE(out + 80, v[10]);
    STORE64_LE(out + 88, v[11]);
    STORE64_LE(out + 96, v[12]);
    STORE64_LE(out + 104, v[13]);
    STORE64_LE(out + 112, v[14]);
    STORE64_LE(out + 120, v[15]);
    msg += 128;
    out += 128;
  }
}