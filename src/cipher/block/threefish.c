/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mcrypto/cipher/block/threefish.h"
#include "mcrypto/macros.h"
#include <stdint.h>
#include <stdio.h>

static const uint64_t c240 = UINT64_C(0x1BD11BDAA9FC1A22);

static const uint_fast8_t MixTbl4[8][2] = { { 14, 16 }, { 52, 57 }, { 23, 40 },
                                            { 5, 37 },  { 25, 33 }, { 46, 12 },
                                            { 58, 22 }, { 32, 32 } };

static const uint_fast8_t MixTbl8[8][4] = {
  { 46, 36, 19, 37 }, { 33, 27, 14, 42 }, { 17, 49, 36, 39 }, { 44, 9, 54, 56 },
  { 39, 30, 34, 24 }, { 13, 50, 10, 17 }, { 25, 29, 39, 43 }, { 8, 35, 56, 22 }
};

static const uint_fast8_t MixTbl16[8][8] = {
  { 24, 13, 8, 47, 8, 17, 22, 37 },   { 38, 19, 10, 55, 49, 18, 23, 52 },
  { 33, 4, 51, 13, 34, 41, 59, 17 },  { 5, 20, 48, 41, 47, 28, 16, 25 },
  { 41, 9, 37, 31, 12, 47, 44, 30 },  { 16, 34, 56, 51, 4, 53, 42, 41 },
  { 31, 44, 47, 46, 19, 42, 44, 25 }, { 9, 48, 35, 52, 23, 31, 37, 20 }
};

static const uint_fast8_t PerTbl4[4][4] = {
  { 0, 1, 2, 3 }, { 0, 3, 2, 1 }, { 0, 1, 2, 3 }, { 0, 3, 2, 1 }
};
static const uint_fast8_t PerTbl8[4][8] = { { 0, 1, 2, 3, 4, 5, 6, 7 },
                                            { 2, 1, 4, 7, 6, 5, 0, 3 },
                                            { 4, 1, 6, 3, 0, 5, 2, 7 },
                                            { 6, 1, 0, 7, 2, 5, 4, 3 } };

static const uint_fast8_t PerTbl16[4][16] = {
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
  { 0, 9, 2, 13, 6, 11, 4, 15, 10, 7, 12, 3, 14, 5, 8, 1 },
  { 0, 7, 2, 5, 4, 3, 6, 1, 12, 15, 14, 13, 8, 11, 10, 9 },
  { 0, 15, 2, 11, 6, 13, 4, 9, 14, 1, 8, 5, 10, 3, 12, 7 }
};

#define MIX(x, y, r)                                                           \
  {                                                                            \
    (x) += (y);                                                                \
    (y) = ROTL64((y), (r)) ^ (x);                                              \
  }

#define INV_MIX(x, y, r)                                                       \
  {                                                                            \
    (y) = ROTR64(((y) ^ (x)), (r));                                            \
    (x) -= (y);                                                                \
  }

#define PREMUTE_MIX_4(arr, r, inv)                                             \
  {                                                                            \
    inv##MIX((arr)[PerTbl4[r % 4][0]], (arr)[PerTbl4[r % 4][1]],               \
             MixTbl4[r][0]);                                                   \
    inv##MIX((arr)[PerTbl4[r % 4][2]], (arr)[PerTbl4[r % 4][3]],               \
             MixTbl4[r][1]);                                                   \
  }

#define PREMUTE_MIX_8(arr, r, inv)                                             \
  {                                                                            \
    inv##MIX((arr)[PerTbl8[r % 4][0]], (arr)[PerTbl8[r % 4][1]],               \
             MixTbl8[r][0]);                                                   \
    inv##MIX((arr)[PerTbl8[r % 4][2]], (arr)[PerTbl8[r % 4][3]],               \
             MixTbl8[r][1]);                                                   \
    inv##MIX((arr)[PerTbl8[r % 4][4]], (arr)[PerTbl8[r % 4][5]],               \
             MixTbl8[r][2]);                                                   \
    inv##MIX((arr)[PerTbl8[r % 4][6]], (arr)[PerTbl8[r % 4][7]],               \
             MixTbl8[r][3]);                                                   \
  }

#define PREMUTE_MIX_16(arr, r, inv)                                            \
  {                                                                            \
    inv##MIX((arr)[PerTbl16[r % 4][0]], (arr)[PerTbl16[r % 4][1]],             \
             MixTbl16[r][0]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][2]], (arr)[PerTbl16[r % 4][3]],             \
             MixTbl16[r][1]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][4]], (arr)[PerTbl16[r % 4][5]],             \
             MixTbl16[r][2]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][6]], (arr)[PerTbl16[r % 4][7]],             \
             MixTbl16[r][3]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][8]], (arr)[PerTbl16[r % 4][9]],             \
             MixTbl16[r][4]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][10]], (arr)[PerTbl16[r % 4][11]],           \
             MixTbl16[r][5]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][12]], (arr)[PerTbl16[r % 4][13]],           \
             MixTbl16[r][6]);                                                  \
    inv##MIX((arr)[PerTbl16[r % 4][14]], (arr)[PerTbl16[r % 4][15]],           \
             MixTbl16[r][7]);                                                  \
  }

#define PREMUTE_MIX(arr, r, w)                                                 \
  {                                                                            \
    PREMUTE_MIX_##w(arr, (r) % 8, );                                           \
  }

#define PREMUTE_INV_MIX(arr, r, w)                                             \
  {                                                                            \
    PREMUTE_MIX_##w(arr, (r) % 8, INV_);                                       \
  }

#define INJECT_KEY_4(key, tweak, input, r, o)                                  \
  {                                                                            \
    (input)[0] o## = (key)[((r) + (0)) % (5)];                                 \
    (input)[1] o## = (key)[((r) + (1)) % (5)] + tweak[((r)) % 3];              \
    (input)[2] o## = (key)[((r) + (2)) % (5)] + tweak[((r) + 1) % 3];          \
    (input)[3] o## = (key)[((r) + (3)) % (5)] + (r);                           \
  }

#define INJECT_KEY_8(key, tweak, input, r, o)                                  \
  {                                                                            \
    (input)[0] o## = (key)[((r) + (0)) % (9)];                                 \
    (input)[1] o## = (key)[((r) + (1)) % (9)];                                 \
    (input)[2] o## = (key)[((r) + (2)) % (9)];                                 \
    (input)[3] o## = (key)[((r) + (3)) % (9)];                                 \
    (input)[4] o## = (key)[((r) + (4)) % (9)];                                 \
    (input)[5] o## = (key)[((r) + (5)) % (9)] + tweak[((r)) % 3];              \
    (input)[6] o## = (key)[((r) + (6)) % (9)] + tweak[((r) + 1) % 3];          \
    (input)[7] o## = (key)[((r) + (7)) % (9)] + (r);                           \
  }

#define INJECT_KEY_16(key, tweak, input, r, o)                                 \
  {                                                                            \
    (input)[0] o## = (key)[((r) + (0)) % (17)];                                \
    (input)[1] o## = (key)[((r) + (1)) % (17)];                                \
    (input)[2] o## = (key)[((r) + (2)) % (17)];                                \
    (input)[3] o## = (key)[((r) + (3)) % (17)];                                \
    (input)[4] o## = (key)[((r) + (4)) % (17)];                                \
    (input)[5] o## = (key)[((r) + (5)) % (17)];                                \
    (input)[6] o## = (key)[((r) + (6)) % (17)];                                \
    (input)[7] o## = (key)[((r) + (7)) % (17)];                                \
    (input)[8] o## = (key)[((r) + (8)) % (17)];                                \
    (input)[9] o## = (key)[((r) + (9)) % (17)];                                \
    (input)[10] o## = (key)[((r) + (10)) % (17)];                              \
    (input)[11] o## = (key)[((r) + (11)) % (17)];                              \
    (input)[12] o## = (key)[((r) + (12)) % (17)];                              \
    (input)[13] o## = (key)[((r) + (13)) % (17)] + tweak[((r)) % 3];           \
    (input)[14] o## = (key)[((r) + (14)) % (17)] + tweak[((r) + 1) % 3];       \
    (input)[15] o## = (key)[((r) + (15)) % (17)] + (r);                        \
  }

#define INJECT_KEY(key, tweak, input, r, w)                                    \
  {                                                                            \
    INJECT_KEY_##w(key, tweak, input, (r), +)                                  \
  }

#define INV_INJECT_KEY(key, tweak, input, r, w)                                \
  {                                                                            \
    INJECT_KEY_##w(key, tweak, input, (r), -)                                  \
  }

#define ROUND8(key, tweak, block, r, w)                                        \
  {                                                                            \
    INJECT_KEY(key, tweak, block, r * 2, w);                                   \
    PREMUTE_MIX(block, 0, w);                                                  \
    PREMUTE_MIX(block, 1, w);                                                  \
    PREMUTE_MIX(block, 2, w);                                                  \
    PREMUTE_MIX(block, 3, w);                                                  \
    INJECT_KEY(key, tweak, block, r * 2 + 1, w);                               \
    PREMUTE_MIX(block, 4, w);                                                  \
    PREMUTE_MIX(block, 5, w);                                                  \
    PREMUTE_MIX(block, 6, w);                                                  \
    PREMUTE_MIX(block, 7, w);                                                  \
  }

#define ROUND80(key, tweak, block, w)                                          \
  {                                                                            \
    ROUND8(key, tweak, block, 0, w)                                            \
    ROUND8(key, tweak, block, 1, w)                                            \
    ROUND8(key, tweak, block, 2, w)                                            \
    ROUND8(key, tweak, block, 3, w)                                            \
    ROUND8(key, tweak, block, 4, w)                                            \
    ROUND8(key, tweak, block, 5, w)                                            \
    ROUND8(key, tweak, block, 6, w)                                            \
    ROUND8(key, tweak, block, 7, w)                                            \
    ROUND8(key, tweak, block, 8, w)                                            \
    ROUND8(key, tweak, block, 9, w)                                            \
  }

#define ROUND72(key, tweak, block, w)                                          \
  {                                                                            \
    ROUND8(key, tweak, block, 0, w)                                            \
    ROUND8(key, tweak, block, 1, w)                                            \
    ROUND8(key, tweak, block, 2, w)                                            \
    ROUND8(key, tweak, block, 3, w)                                            \
    ROUND8(key, tweak, block, 4, w)                                            \
    ROUND8(key, tweak, block, 5, w)                                            \
    ROUND8(key, tweak, block, 6, w)                                            \
    ROUND8(key, tweak, block, 7, w)                                            \
    ROUND8(key, tweak, block, 8, w)                                            \
  }

#define INV_ROUND8(key, tweak, block, r, w)                                    \
  {                                                                            \
    PREMUTE_INV_MIX(block, 7, w);                                              \
    PREMUTE_INV_MIX(block, 6, w);                                              \
    PREMUTE_INV_MIX(block, 5, w);                                              \
    PREMUTE_INV_MIX(block, 4, w);                                              \
    INV_INJECT_KEY(key, tweak, block, r * 2 + 1, w);                           \
    PREMUTE_INV_MIX(block, 3, w);                                              \
    PREMUTE_INV_MIX(block, 2, w);                                              \
    PREMUTE_INV_MIX(block, 1, w);                                              \
    PREMUTE_INV_MIX(block, 0, w);                                              \
    INV_INJECT_KEY(key, tweak, block, r * 2, w);                               \
  }

#define INV_ROUND80(key, tweak, block, w)                                      \
  {                                                                            \
    INV_ROUND8(key, tweak, block, 9, w)                                        \
    INV_ROUND8(key, tweak, block, 8, w)                                        \
    INV_ROUND8(key, tweak, block, 7, w)                                        \
    INV_ROUND8(key, tweak, block, 6, w)                                        \
    INV_ROUND8(key, tweak, block, 5, w)                                        \
    INV_ROUND8(key, tweak, block, 4, w)                                        \
    INV_ROUND8(key, tweak, block, 3, w)                                        \
    INV_ROUND8(key, tweak, block, 2, w)                                        \
    INV_ROUND8(key, tweak, block, 1, w)                                        \
    INV_ROUND8(key, tweak, block, 0, w)                                        \
  }

#define INV_ROUND72(key, tweak, block, w)                                      \
  {                                                                            \
    INV_ROUND8(key, tweak, block, 8, w)                                        \
    INV_ROUND8(key, tweak, block, 7, w)                                        \
    INV_ROUND8(key, tweak, block, 6, w)                                        \
    INV_ROUND8(key, tweak, block, 5, w)                                        \
    INV_ROUND8(key, tweak, block, 4, w)                                        \
    INV_ROUND8(key, tweak, block, 3, w)                                        \
    INV_ROUND8(key, tweak, block, 2, w)                                        \
    INV_ROUND8(key, tweak, block, 1, w)                                        \
    INV_ROUND8(key, tweak, block, 0, w)                                        \
  }

#define ASSIGN_4(arr1, arr2)                                                   \
  {                                                                            \
    arr1[0] = arr2[0];                                                         \
    arr1[1] = arr2[1];                                                         \
    arr1[2] = arr2[2];                                                         \
    arr1[3] = arr2[3];                                                         \
  }
#define ASSIGN_8(arr1, arr2)                                                   \
  {                                                                            \
    arr1[0] = arr2[0];                                                         \
    arr1[1] = arr2[1];                                                         \
    arr1[2] = arr2[2];                                                         \
    arr1[3] = arr2[3];                                                         \
    arr1[4] = arr2[4];                                                         \
    arr1[5] = arr2[5];                                                         \
    arr1[6] = arr2[6];                                                         \
    arr1[7] = arr2[7];                                                         \
  }
#define ASSIGN_16(arr1, arr2)                                                  \
  {                                                                            \
    arr1[0] = arr2[0];                                                         \
    arr1[1] = arr2[1];                                                         \
    arr1[2] = arr2[2];                                                         \
    arr1[3] = arr2[3];                                                         \
    arr1[4] = arr2[4];                                                         \
    arr1[5] = arr2[5];                                                         \
    arr1[6] = arr2[6];                                                         \
    arr1[7] = arr2[7];                                                         \
    arr1[8] = arr2[8];                                                         \
    arr1[9] = arr2[9];                                                         \
    arr1[10] = arr2[10];                                                       \
    arr1[11] = arr2[11];                                                       \
    arr1[12] = arr2[12];                                                       \
    arr1[13] = arr2[13];                                                       \
    arr1[14] = arr2[14];                                                       \
    arr1[15] = arr2[15];                                                       \
  }

typedef struct {
  uint64_t tweak[3];
  uint64_t key[];
} mcrypto_threefish_ctx;

void store64_le(uint8_t *x, uint64_t l) { STORE64_LE(x, l); }

static inline void mcrypto_threefish_keyw(void *ctx, uint64_t *key,
                                          uint_fast8_t wds)
{
  mcrypto_threefish_ctx *const _ctx = (mcrypto_threefish_ctx *)ctx;
  _ctx->key[wds] = c240;
  for (int i = 0; i < wds; i++) {
    _ctx->key[i] = key[i];
    _ctx->key[wds] ^= key[i];
  }
}

static inline void mcrypto_threefish_key(void *ctx, uint8_t *key,
                                         uint_fast8_t wds)
{
  mcrypto_threefish_ctx *const _ctx = (mcrypto_threefish_ctx *)ctx;
  _ctx->key[wds] = c240;
  for (int i = 0; i < wds; i++) {
    _ctx->key[i] = LOAD64_LE(key + i * 8);
    _ctx->key[wds] ^= _ctx->key[i];
  }
}

static inline void mcrypto_threefish_tweakw(void *ctx, uint64_t *tweak)
{
  mcrypto_threefish_ctx *const _ctx = (mcrypto_threefish_ctx *)ctx;
  _ctx->tweak[0] = tweak[0];
  _ctx->tweak[1] = tweak[1];
  _ctx->tweak[2] = _ctx->tweak[0] ^ _ctx->tweak[1];
}

static inline void mcrypto_threefish_tweak(void *ctx, uint8_t *tweak)
{
  mcrypto_threefish_ctx *const _ctx = (mcrypto_threefish_ctx *)ctx;
  _ctx->tweak[0] = LOAD64_LE(tweak);
  _ctx->tweak[1] = LOAD64_LE(tweak + 8);
  _ctx->tweak[2] = _ctx->tweak[0] ^ _ctx->tweak[1];
}

#define MCRYPTO_THREEFISH_CONFIG_FUNCTIONS(bit, w)                             \
  void mcrypto_threefish##bit##_keyw(mcrypto_threefish##bit##_ctx *ctx,        \
                                     uint64_t *key)                            \
  {                                                                            \
    mcrypto_threefish_keyw(ctx, key, w);                                       \
  }                                                                            \
                                                                               \
  void mcrypto_threefish##bit##_key(mcrypto_threefish##bit##_ctx *ctx,         \
                                    uint8_t *key)                              \
  {                                                                            \
    mcrypto_threefish_key(ctx, key, w);                                        \
  }                                                                            \
                                                                               \
  void mcrypto_threefish##bit##_tweakw(mcrypto_threefish##bit##_ctx *ctx,      \
                                       uint64_t *tweak)                        \
  {                                                                            \
    mcrypto_threefish_tweakw(ctx, tweak);                               \
  }                                                                            \
                                                                               \
  void mcrypto_threefish##bit##_tweak(mcrypto_threefish##bit##_ctx *ctx,       \
                                      uint8_t *tweak)                          \
  {                                                                            \
    mcrypto_threefish_tweak(ctx, tweak);                                       \
  }

MCRYPTO_THREEFISH_CONFIG_FUNCTIONS(1024, 16)
MCRYPTO_THREEFISH_CONFIG_FUNCTIONS(512, 8)
MCRYPTO_THREEFISH_CONFIG_FUNCTIONS(256, 4)

#define MCRYPTO_THREEFISH_ENCRYPT_FUNCTIONS(bit, w, r)                         \
  inline void mcrypto_threefish##bit##_encryptw_one(                           \
      mcrypto_threefish##bit##_ctx *ctx, uint64_t *input, uint64_t *output)    \
  {                                                                            \
    uint64_t _block[w], tweak[3], key[w + 1];                                  \
    ASSIGN_##w(_block, input);                                                 \
    ASSIGN_##w(key, ctx->key);                                                 \
    key[w] = ctx->key[w];                                                      \
    tweak[0] = ctx->tweak[0];                                                  \
    tweak[1] = ctx->tweak[1];                                                  \
    tweak[2] = ctx->tweak[2];                                                  \
    ROUND##r(key, tweak, _block, w);                                           \
    INJECT_KEY(key, tweak, _block, r / 4, w);                                  \
    ASSIGN_##w(output, _block);                                                \
  }                                                                            \
                                                                               \
  void mcrypto_threefish##bit##_encryptw(mcrypto_threefish##bit##_ctx *ctx,    \
                                         uint64_t *input, uint64_t *output,    \
                                         uint64_t length)                      \
  {                                                                            \
    for (uint64_t i = 0; i < length / w; i++) {                                \
      mcrypto_threefish##bit##_encryptw_one(ctx, input + i * w,                \
                                            output + i * w);                   \
    }                                                                          \
  }                                                                            \
                                                                               \
  inline void mcrypto_threefish##bit##_encrypt_one(                            \
      mcrypto_threefish##bit##_ctx *ctx, uint8_t *plaintext,                   \
      uint8_t *ciphertext)                                                     \
  {                                                                            \
    uint64_t block[w];                                                         \
    for (int i = 0; i < w; i++) {                                              \
      block[i] = LOAD64_LE(plaintext + i * 8);                                 \
    }                                                                          \
    mcrypto_threefish##bit##_encryptw_one(ctx, block, block);                  \
    for (int i = 0; i < w; i++) {                                              \
      STORE64_LE(ciphertext + i * 8, block[i]);                                \
    }                                                                          \
  }                                                                            \
                                                                               \
  void mcrypto_threefish##bit##_encrypt(mcrypto_threefish##bit##_ctx *ctx,     \
                                        uint8_t *plaintext,                    \
                                        uint8_t *ciphertext, uint64_t length)  \
  {                                                                            \
    for (uint64_t i = 0; i < length / (bit / 8); i++) {                        \
      mcrypto_threefish##bit##_encrypt_one(ctx, plaintext + i * (bit / 8),     \
                                           ciphertext + i * (bit / 8));        \
    }                                                                          \
  }                                                                            \
                                                                               \
  inline void mcrypto_threefish##bit##_decryptw_one(                           \
      mcrypto_threefish##bit##_ctx *ctx, uint64_t *input, uint64_t *output)    \
  {                                                                            \
    uint64_t _block[w], tweak[3], key[w + 1];                                  \
    ASSIGN_##w(_block, input);                                                 \
    ASSIGN_##w(key, ctx->key);                                                 \
    key[w] = ctx->key[w];                                                      \
    tweak[0] = ctx->tweak[0];                                                  \
    tweak[1] = ctx->tweak[1];                                                  \
    tweak[2] = ctx->tweak[2];                                                  \
    INV_INJECT_KEY(key, tweak, _block, r / 4, w);                              \
    INV_ROUND##r(key, tweak, _block, w);                                       \
    ASSIGN_##w(output, _block);                                                \
  }                                                                            \
  void mcrypto_threefish##bit##_decryptw(mcrypto_threefish##bit##_ctx *ctx,    \
                                         uint64_t *input, uint64_t *output,    \
                                         uint64_t length)                      \
  {                                                                            \
    for (uint64_t i = 0; i < length / w; i++) {                                \
      mcrypto_threefish##bit##_decryptw_one(ctx, input + i * w,                \
                                            output + i * w);                   \
    }                                                                          \
  }                                                                            \
  inline void mcrypto_threefish##bit##_decrypt_one(                            \
      mcrypto_threefish##bit##_ctx *ctx, uint8_t *ciphertext,                  \
      uint8_t *plaintext)                                                      \
  {                                                                            \
    uint64_t block[w];                                                         \
    for (int i = 0; i < w; i++) {                                              \
      block[i] = LOAD64_LE(ciphertext + i * 8);                                \
    }                                                                          \
    mcrypto_threefish##bit##_decryptw_one(ctx, block, block);                  \
                                                                               \
    for (int i = 0; i < w; i++) {                                              \
      STORE64_LE(plaintext + i * 8, block[i]);                                 \
    }                                                                          \
  }                                                                            \
  void mcrypto_threefish##bit##_decrypt(mcrypto_threefish##bit##_ctx *ctx,     \
                                        uint8_t *ciphertext,                   \
                                        uint8_t *plaintext, uint64_t length)   \
  {                                                                            \
    for (uint64_t i = 0; i < length / (bit / 8); i++) {                        \
      mcrypto_threefish##bit##_decrypt_one(ctx, ciphertext + i * (bit / 8),    \
                                           plaintext + i * (bit / 8));         \
    }                                                                          \
  }

MCRYPTO_THREEFISH_ENCRYPT_FUNCTIONS(1024, 16, 80)
MCRYPTO_THREEFISH_ENCRYPT_FUNCTIONS(512, 8, 72)
MCRYPTO_THREEFISH_ENCRYPT_FUNCTIONS(256, 4, 72)