/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mcrypto/cipher/stream/chacha20.h"
#include "mcrypto/macros.h"
#include "mcrypto/mcrypto.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* VARIENTS */
enum {
  MCRYPTO_CORE_CHACHA20 = 1,
  MCRYPTO_CORE_CHACHA20_IEFT,
  MCRYPTO_CORE_CHACHA20_XCHACHA20
};

#define PLUS32(a, b) ((uint32_t)(a) + (uint32_t)b)
#define XOR32(a, b) ((uint32_t)(a) ^ (uint32_t)(b))

#define QUARTERROUND(a, b, c, d)                                               \
  {                                                                            \
    a = PLUS32(a, b);                                                          \
    d = ROTL32(XOR32(d, a), 16);                                               \
    c = PLUS32(c, d);                                                          \
    b = ROTL32(XOR32(b, c), 12);                                               \
    a = PLUS32(a, b);                                                          \
    d = ROTL32(XOR32(d, a), 8);                                                \
    c = PLUS32(c, d);                                                          \
    b = ROTL32(XOR32(b, c), 7);                                                \
  }

#define QUARTERROUND_ARRAY(array, a, b, c, d)                                  \
  QUARTERROUND((array)[a], (array)[b], (array)[c], (array)[d]);

/*
 * A single iteration of chacha20 function
 *  state is a 16-element array
 */
static inline void mcrypto_chacha20_round(uint32_t *state)
{
  QUARTERROUND_ARRAY(state, 0, 4, 8, 12);
  QUARTERROUND_ARRAY(state, 1, 5, 9, 13);
  QUARTERROUND_ARRAY(state, 2, 6, 10, 14);
  QUARTERROUND_ARRAY(state, 3, 7, 11, 15);
  QUARTERROUND_ARRAY(state, 0, 5, 10, 15);
  QUARTERROUND_ARRAY(state, 1, 6, 11, 12);
  QUARTERROUND_ARRAY(state, 2, 7, 8, 13);
  QUARTERROUND_ARRAY(state, 3, 4, 9, 14);
  return;
}

/* chacha20 function
   state, out is a 16-element array */
static inline void mcrypto_chacha20_core(uint32_t *state, uint32_t *out)
{
  /* copy input to output */
  memcpy(out, state, 64);

  /* apply chacha20 function */
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);
  mcrypto_chacha20_round(out);

  /* add the original state */
  out[0] += state[0];
  out[1] += state[1];
  out[2] += state[2];
  out[3] += state[3];
  out[4] += state[4];
  out[5] += state[5];
  out[6] += state[6];
  out[7] += state[7];
  out[8] += state[8];
  out[9] += state[9];
  out[10] += state[10];
  out[11] += state[11];
  out[12] += state[12];
  out[13] += state[13];
  out[14] += state[14];
  out[15] += state[15];
  out[16] += state[16];

  return;
}

inline void mcrypto_chacha20_init(void *ctx)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[0] = UINT32_C(0x61707865);
  _ctx->state[1] = UINT32_C(0x3320646e);
  _ctx->state[2] = UINT32_C(0x79622d32);
  _ctx->state[3] = UINT32_C(0x6b206574);
}

inline void mcrypto_chacha20_key(void *ctx, uint8_t *key)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  for (int i = 0; i < 8; ++i) {
    _ctx->state[i + 4] = LOAD32_LE(key + (i * 4));
  }
}

static inline void mcrypto_chacha20_input(void *ctx, uint8_t *input)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  for (int i = 0; i < 4; ++i) {
    _ctx->state[i + 12] = LOAD32_LE(input + (i * 4));
  }
}

inline void mcrypto_chacha20_nonce(void *ctx, uint8_t *nonce)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  for (int i = 0; i < 2; ++i) {
    _ctx->state[i + 14] = LOAD32_LE(nonce + (i * 4));
  }
}

inline void mcrypto_chacha20_ieft_nonce(void *ctx, uint8_t *nonce)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  for (int i = 0; i < 3; ++i) {
    _ctx->state[i + 13] = LOAD32_LE(nonce + (i * 4));
  }
}

void mcrypto_chacha20_counter_qw(void *ctx, uint64_t counter)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12] = counter & UINT64_C(0xFFFFFFFF);
  _ctx->state[13] = counter >> 32;
}

void mcrypto_chacha20_counter(void *ctx, uint8_t *counter)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12] = LOAD32_LE(counter);
  _ctx->state[13] = LOAD32_LE(counter + 4);
}

void mcrypto_chacha20_ieft_counterw(void *ctx, uint32_t counter)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12] = counter;
}

void mcrypto_chacha20_ieft_counter(void *ctx, uint8_t *counter)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12] = LOAD32_LE(counter);
}

inline void mcrypto_chacha20_increase_counter(void *ctx)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12]++;
  _ctx->state[13] += _ctx->state[12] == 0;
}

inline void mcrypto_chacha20_ieft_increase_counter(void *ctx)
{
  mcrypto_chacha20_ctx *_ctx = (mcrypto_chacha20_ctx *)ctx;
  _ctx->state[12]++;
}

/* hchacha20 function
   state is a 16-element array */
static inline void hchacha(uint8_t *out, uint8_t *key, uint8_t *input)
{

  mcrypto_chacha20_ctx ctx;
  mcrypto_chacha20_init(&ctx);
  mcrypto_chacha20_key(&ctx, key);
  mcrypto_chacha20_input(&ctx, input);

  /* apply chacha20 function */
  for (int i = 0; i < 10; ++i) {
    mcrypto_chacha20_round(ctx.state);
  }

  /* state 0~3 to out 0~15 */
  for (int i = 0; i < 4; ++i) {
    STORE32_LE(out + (i * 4), ctx.state[i]);
  }

  /* state 12~15 to out 16~31 */
  for (int i = 0; i < 4; ++i) {
    STORE32_LE(out + 16 + (i * 4), ctx.state[12 + i]);
  }
}

/* With the initialized state */
static inline void chacha20_do(mcrypto_chacha20_ctx *ctx, uint8_t *input,
                               uint8_t *output, uint64_t length, int varient)
{
  uint32_t keyblock[16] = {0};
  uint32_t tmp_block;
  uint8_t tmp_stream[64];
  size_t position = 0;
  for (uint64_t i = 0; i < length / 64; ++i) {
    mcrypto_chacha20_core(ctx->state, keyblock);

    for (int j = 0; j < 16; j++) {
      if (input == NULL) {
        STORE32_LE(output + position + j * 4, keyblock[j]);
      } else {
        tmp_block = LOAD32_LE(input + position + (j * 4));
        tmp_block = tmp_block ^ keyblock[j];
        STORE32_LE(output + position + (j * 4), tmp_block);
      }
    }

    if (varient == MCRYPTO_CORE_CHACHA20_IEFT) {
      mcrypto_chacha20_ieft_increase_counter(ctx);
    } else {
      mcrypto_chacha20_increase_counter(ctx);
    }
    position += 64;
  }

  if (length % 64 == 0)
    return;

  mcrypto_chacha20_core(ctx->state, keyblock);

  for (int i = 0; i < 16; i++)
    STORE32_LE(tmp_stream + i * 4, keyblock[i]);

  for (int i = 0; i < length % 64; i++) {
    if (input == NULL) {
      *(output + length / 64 * 64 + i) = tmp_stream[i];
    } else {
      *(output + length / 64 * 64 + i) =
          *(input + length / 64 * 64 + i) ^ tmp_stream[i];
    }
  }
}

void mcrypto_xchacha20_setup(void *ctx, uint8_t *key, uint8_t *nonce)
{
  uint8_t tmp_key[32];
  hchacha(tmp_key, key, nonce);
  mcrypto_chacha20_key(ctx, tmp_key);
  mcrypto_chacha20_nonce(ctx, nonce + 16);
}

void mcrypto_chacha20_crypt(void *ctx, uint8_t *input, uint8_t *output,
                            uint64_t length)
{
  chacha20_do(ctx, input, output, length, MCRYPTO_CORE_CHACHA20);
}

void mcrypto_chacha20_keystream(void *ctx, uint8_t *output, uint8_t length)
{
  chacha20_do(ctx, NULL, output, length, MCRYPTO_CORE_CHACHA20);
}

void mcrypto_chacha20_ieft_crypt(void *ctx, uint8_t *input, uint8_t *output,
                                 uint64_t length)
{
  chacha20_do(ctx, input, output, length, MCRYPTO_CORE_CHACHA20_IEFT);
}

void mcrypto_chacha20_ieft_keystream(void *ctx, uint8_t *output, uint8_t length)
{
  chacha20_do(ctx, NULL, output, length, MCRYPTO_CORE_CHACHA20_IEFT);
}
