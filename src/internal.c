#include "mcrypto/macros.h"

extern uint32_t LOAD32_LE(const uint8_t *p);
extern void STORE32_LE(uint8_t *p, const uint32_t v);
extern uint64_t LOAD64_LE(const uint8_t *p);
extern void STORE64_LE(uint8_t *p, const uint64_t v);
extern uint32_t LOAD32_BE(const uint8_t *p);
extern void STORE32_BE(uint8_t *p, const uint32_t v);
extern uint64_t LOAD64_BE(const uint8_t *p);
extern void STORE64_BE(uint8_t *p, const uint64_t v);
