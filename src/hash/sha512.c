/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mcrypto/hash/sha512.h"
#include "mcrypto/macros.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

const static uint64_t h[8] = {
  UINT64_C(0x6a09e667f3bcc908), UINT64_C(0xbb67ae8584caa73b),
  UINT64_C(0x3c6ef372fe94f82b), UINT64_C(0xa54ff53a5f1d36f1),
  UINT64_C(0x510e527fade682d1), UINT64_C(0x9b05688c2b3e6c1f),
  UINT64_C(0x1f83d9abfb41bd6b), UINT64_C(0x5be0cd19137e2179)
};

const static uint64_t k[80] = {
  UINT64_C(0x428a2f98d728ae22), UINT64_C(0x7137449123ef65cd),
  UINT64_C(0xb5c0fbcfec4d3b2f), UINT64_C(0xe9b5dba58189dbbc),
  UINT64_C(0x3956c25bf348b538), UINT64_C(0x59f111f1b605d019),
  UINT64_C(0x923f82a4af194f9b), UINT64_C(0xab1c5ed5da6d8118),
  UINT64_C(0xd807aa98a3030242), UINT64_C(0x12835b0145706fbe),
  UINT64_C(0x243185be4ee4b28c), UINT64_C(0x550c7dc3d5ffb4e2),
  UINT64_C(0x72be5d74f27b896f), UINT64_C(0x80deb1fe3b1696b1),
  UINT64_C(0x9bdc06a725c71235), UINT64_C(0xc19bf174cf692694),
  UINT64_C(0xe49b69c19ef14ad2), UINT64_C(0xefbe4786384f25e3),
  UINT64_C(0x0fc19dc68b8cd5b5), UINT64_C(0x240ca1cc77ac9c65),
  UINT64_C(0x2de92c6f592b0275), UINT64_C(0x4a7484aa6ea6e483),
  UINT64_C(0x5cb0a9dcbd41fbd4), UINT64_C(0x76f988da831153b5),
  UINT64_C(0x983e5152ee66dfab), UINT64_C(0xa831c66d2db43210),
  UINT64_C(0xb00327c898fb213f), UINT64_C(0xbf597fc7beef0ee4),
  UINT64_C(0xc6e00bf33da88fc2), UINT64_C(0xd5a79147930aa725),
  UINT64_C(0x06ca6351e003826f), UINT64_C(0x142929670a0e6e70),
  UINT64_C(0x27b70a8546d22ffc), UINT64_C(0x2e1b21385c26c926),
  UINT64_C(0x4d2c6dfc5ac42aed), UINT64_C(0x53380d139d95b3df),
  UINT64_C(0x650a73548baf63de), UINT64_C(0x766a0abb3c77b2a8),
  UINT64_C(0x81c2c92e47edaee6), UINT64_C(0x92722c851482353b),
  UINT64_C(0xa2bfe8a14cf10364), UINT64_C(0xa81a664bbc423001),
  UINT64_C(0xc24b8b70d0f89791), UINT64_C(0xc76c51a30654be30),
  UINT64_C(0xd192e819d6ef5218), UINT64_C(0xd69906245565a910),
  UINT64_C(0xf40e35855771202a), UINT64_C(0x106aa07032bbd1b8),
  UINT64_C(0x19a4c116b8d2d0c8), UINT64_C(0x1e376c085141ab53),
  UINT64_C(0x2748774cdf8eeb99), UINT64_C(0x34b0bcb5e19b48a8),
  UINT64_C(0x391c0cb3c5c95a63), UINT64_C(0x4ed8aa4ae3418acb),
  UINT64_C(0x5b9cca4f7763e373), UINT64_C(0x682e6ff3d6b2b8a3),
  UINT64_C(0x748f82ee5defb2fc), UINT64_C(0x78a5636f43172f60),
  UINT64_C(0x84c87814a1f0ab72), UINT64_C(0x8cc702081a6439ec),
  UINT64_C(0x90befffa23631e28), UINT64_C(0xa4506cebde82bde9),
  UINT64_C(0xbef9a3f7b2c67915), UINT64_C(0xc67178f2e372532b),
  UINT64_C(0xca273eceea26619c), UINT64_C(0xd186b8c721c0c207),
  UINT64_C(0xeada7dd6cde0eb1e), UINT64_C(0xf57d4f7fee6ed178),
  UINT64_C(0x06f067aa72176fba), UINT64_C(0x0a637dc5a2c898a6),
  UINT64_C(0x113f9804bef90dae), UINT64_C(0x1b710b35131c471b),
  UINT64_C(0x28db77f523047d84), UINT64_C(0x32caab7b40c72493),
  UINT64_C(0x3c9ebe0a15c9bebc), UINT64_C(0x431d67c49c100d4c),
  UINT64_C(0x4cc5d4becb3e42b6), UINT64_C(0x597f299cfc657e2a),
  UINT64_C(0x5fcb6fab3ad6faec), UINT64_C(0x6c44198c4a475817)
};

void mcrypto_sha512_init(mcrypto_sha512_ctx *ctx)
{
  memcpy(ctx->h, &h, 64);
  ctx->counter[0] = 0;
  ctx->counter[1] = 0;
  ctx->buflen = 0;
}

static inline void inc_counter(mcrypto_sha512_ctx *ctx, size_t inc)
{
  ctx->counter[1] += inc;
  if (ctx->counter[1] < inc)
    ctx->counter[0] += 1;
}

static inline void compress(mcrypto_sha512_ctx *ctx, const uint8_t block[128])
{
  uint64_t w[80];
  uint64_t a, b, c, d, e, f, g, h;
  a = ctx->h[0];
  b = ctx->h[1];
  c = ctx->h[2];
  d = ctx->h[3];
  e = ctx->h[4];
  f = ctx->h[5];
  g = ctx->h[6];
  h = ctx->h[7];

  for (int i = 0; i < 16; i++) {
    w[i] = LOAD64_BE(block + (i << 3));
  }
  for (int i = 16; i < 80; i++) {
    uint64_t s0, s1;
    s0 = ROTR64(w[i - 15], 1) ^ ROTR64(w[i - 15], 8) ^ (w[i - 15] >> 7);
    s1 = ROTR64(w[i - 2], 19) ^ ROTR64(w[i - 2], 61) ^ (w[i - 2] >> 6);
    w[i] = w[i - 16] + s0 + w[i - 7] + s1;
  }

  for (int i = 0; i < 80; i++) {
    uint64_t S0, S1, ch, maj, temp1, temp2;
    S0 = ROTR64(a, 28) ^ ROTR64(a, 34) ^ ROTR64(a, 39);
    S1 = ROTR64(e, 14) ^ ROTR64(e, 18) ^ ROTR64(e, 41);
    ch = (e & f) ^ ((~e) & g);
    maj = (a & b) ^ (a & c) ^ (b & c);

    temp1 = h + S1 + ch + k[i] + w[i];
    temp2 = S0 + maj;

    h = g;
    g = f;
    f = e;
    e = d + temp1;
    d = c;
    c = b;
    b = a;
    a = temp1 + temp2;
  }

  ctx->h[0] += a;
  ctx->h[1] += b;
  ctx->h[2] += c;
  ctx->h[3] += d;
  ctx->h[4] += e;
  ctx->h[5] += f;
  ctx->h[6] += g;
  ctx->h[7] += h;
}

void mcrypto_sha512_update(mcrypto_sha512_ctx *ctx, const uint8_t *msg,
                          size_t mlen)
{
  uint8_t buflen = ctx->buflen;
  uint8_t fill = 128 - buflen;

  /* Just fill the buffer */
  if (mlen < (uint64_t)fill) {
    memcpy(ctx->buffer + buflen, msg, mlen);
    ctx->buflen += mlen;
    return;
  } else {
    /* fill the buffer first */
    memcpy(ctx->buffer + buflen, msg, fill);
    msg += fill;
    mlen -= fill;
    compress(ctx, ctx->buffer);
    inc_counter(ctx, 1024);

    /* keep compressing */
    while (mlen >= 128) {
      compress(ctx, msg);
      msg += 128;
      mlen -= 128;
      inc_counter(ctx, 1024);
    }

    /* fill the final block into the buffer */
    memcpy(ctx->buffer, msg, mlen);
    ctx->buflen = mlen;
  }
}

void mcrypto_sha512_final(mcrypto_sha512_ctx *ctx, uint8_t hash[64])
{
  uint8_t left;
  uint8_t buflen = ctx->buflen;
  inc_counter(ctx, buflen * 8);
  ctx->buffer[buflen] = 0x80;
  buflen++;

  left = 128 - buflen;

  /* padding */
  memset(ctx->buffer + buflen, 0, left);

  /* compress the last 2nd block */
  if (left < 16) {
    compress(ctx, ctx->buffer);

    /* zeroing the buffer */
    memset(ctx->buffer, 0, 128);
  }
  /* set the counter */
  STORE64_BE(ctx->buffer + 112, ctx->counter[0]);
  STORE64_BE(ctx->buffer + 120, ctx->counter[1]);
  /* compress the last block */
  compress(ctx, ctx->buffer);

  for (int i = 0; i < 8; i++) {
    STORE64_BE(hash + (i << 3), ctx->h[i]);
  }

  mcrypto_sha512_init(ctx);
}
