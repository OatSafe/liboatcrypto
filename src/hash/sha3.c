#include "mcrypto/hash/sha3.h"
#include "mcrypto/macros.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

const static int r[5][5] = { { 0, 36, 3, 41, 18 },
                             { 1, 44, 10, 45, 2 },
                             { 62, 6, 43, 15, 61 },
                             { 28, 55, 25, 21, 56 },
                             { 27, 20, 39, 8, 14 } };

const static uint64_t RC[24] = {
  UINT64_C(0x0000000000000001), UINT64_C(0x0000000000008082),
  UINT64_C(0x800000000000808A), UINT64_C(0x8000000080008000),
  UINT64_C(0x000000000000808B), UINT64_C(0x0000000080000001),
  UINT64_C(0x8000000080008081), UINT64_C(0x8000000000008009),
  UINT64_C(0x000000000000008A), UINT64_C(0x0000000000000088),
  UINT64_C(0x0000000080008009), UINT64_C(0x000000008000000A),
  UINT64_C(0x000000008000808B), UINT64_C(0x800000000000008B),
  UINT64_C(0x8000000000008089), UINT64_C(0x8000000000008003),
  UINT64_C(0x8000000000008002), UINT64_C(0x8000000000000080),
  UINT64_C(0x000000000000800A), UINT64_C(0x800000008000000A),
  UINT64_C(0x8000000080008081), UINT64_C(0x8000000000008080),
  UINT64_C(0x0000000080000001), UINT64_C(0x8000000080008008)
};

const static uint16_t bufsizes[7] = { 168, 136, 72, 144, 136, 104, 72 };
const static uint8_t suffix[7] = { 0x1F, 0x1F, 0x1F, 0x06, 0x06, 0x06, 0x06 };

#if 0 // we don't use this
const static uint8_t output_length[7] = { 0, 0, 28, 32, 48, 64 };
#endif

static inline void round1600(uint64_t A[5][5], int rc)
{
  uint64_t c[5];
  uint64_t d[5];
  uint64_t B[5][5];

  /* Theta */
  for (int x = 0; x < 5; x++) {
    c[x] = A[0][x] ^ A[1][x] ^ A[2][x] ^ A[3][x] ^ A[4][x];
  }

  for (int i = 0; i < 5; i++) {
    d[i] = c[(i + 4) % 5] ^ ROTL64(c[(i + 1) % 5], 1);
  }

  for (int x = 0; x < 5; x++) {
    for (int y = 0; y < 5; y++) {
      A[y][x] ^= d[x];
    }
  }

  /* rho */
  for (int x = 0; x < 5; x++) {
    for (int y = 0; y < 5; y++) {
      A[y][x] = ROTL64(A[y][x], (r[x][y] % 64));
    }
  }

  /* pi */
  for (int x = 0; x < 5; x++) {
    for (int y = 0; y < 5; y++) {
      B[y][x] = A[x][(x + 3 * y) % 5];
    }
  }

  /* X */
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      A[j][i] = B[j][i] ^ ((~B[j][(i + 1) % 5]) & B[j][(i + 2) % 5]);
    }
  }

  /* l */

  A[0][0] ^= RC[rc];
}

static inline void keccak_f1600(uint64_t A[5][5])
{
  for (int i = 0; i < 24; i++) {
    round1600(A, i);
  }
}

static inline void update_block(uint64_t A[5][5], const uint8_t *buffer, uint16_t len)
{
  uint64_t *ptr = (uint64_t *)A;
  for (int i = 0; i < len / 8; i++) {
    *ptr ^= LOAD64_LE(buffer);
    buffer += 8;
    ptr++;
  }
  keccak_f1600(A);
}

int mcrypto_sha3_init(mcrypto_sha3_ctx *ctx, const unsigned int varient)
{
  /* Invalid varient */
  if (varient > 6) {
    return -1;
  }

  ctx->varient = varient;
  ctx->buflen_pos = 0;
  memset(ctx->state, 0, 200);
  memset(ctx->buffer, 0, 168);

  return 0;
}

void mcrypto_sha3_update(mcrypto_sha3_ctx *ctx, const uint8_t *input, size_t length)
{
  uint16_t buflen = ctx->buflen_pos;
  uint16_t bufsize = bufsizes[ctx->varient];
  uint16_t fill = bufsize - buflen;

  if (length <= fill) {
    memcpy(ctx->buffer + buflen, input, length);
    ctx->buflen_pos += length;
  } else {
    /* fill the buffer first */
    memcpy(ctx->buffer + buflen, input, fill);
    update_block(ctx->state, ctx->buffer, bufsize);
    input += fill;
    length -= fill;

    /* keep updating the state */
    while (length >= bufsize) {
      update_block(ctx->state, input, bufsize);
      input += bufsize;
      length -= bufsize;
    }

    /* store the last block */
    memcpy(ctx->buffer, input, length);
    ctx->buflen_pos = length;
  }
}

void mcrypto_sha3_final(mcrypto_sha3_ctx *ctx)
{
  uint64_t *state = (uint64_t *)ctx->state;
  uint8_t bufsize = bufsizes[ctx->varient];

  /* handle the padding */
  memset(ctx->buffer + ctx->buflen_pos, 0, bufsize - ctx->buflen_pos);
  ctx->buffer[ctx->buflen_pos] = suffix[ctx->varient];
  ctx->buffer[bufsize - 1] ^= 0x80;

  /* update the last block */
  update_block(ctx->state, ctx->buffer, bufsize);
  for (int i = 0; i < bufsize / 8; i++) {
    STORE64_LE(ctx->buffer + i * 8, state[i]);
  }
  ctx->buflen_pos = 0;
}

void mcrypto_sha3_hash(mcrypto_sha3_ctx *ctx, uint8_t *hash, size_t length)
{
  uint8_t bufpos = ctx->buflen_pos;
  uint8_t bufsize = bufsizes[ctx->varient];
  uint8_t buflen = bufsize - bufpos;
  uint64_t *state = (uint64_t *)ctx->state;

  if (length <= buflen) {
    memcpy(hash, ctx->buffer + bufpos, length);
    bufpos += length;
  } else {
    /* copy the current buffer */
    memcpy(hash, ctx->buffer + bufpos, buflen);
    hash += buflen;
    length -= buflen;
    bufpos = 0;

    while (length >= bufsize) {
      /* new block */
      keccak_f1600((uint64_t(*)[5])state);
      for (int i = 0; i < bufsize / 8; i++) {
        STORE64_LE(hash + i * 8, state[i]);
      }
      hash += bufsize;
      length -= bufsize;
    }

    if (length > 0) {
      /* still need new state */
      keccak_f1600((uint64_t(*)[5])state);
      for (int i = 0; i < bufsize / 8; i++) {
        STORE64_LE(ctx->buffer + i * 8, state[i]);
      }
      memcpy(hash, ctx->buffer, length);
      bufpos = length;
    }
  }
  ctx->buflen_pos = bufpos;
}
