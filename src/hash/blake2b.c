/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include "mcrypto/hash/blake2b.h"
#include "mcrypto/macros.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static const uint8_t sigma[12][16] = {
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
  { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
  { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
  { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
  { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
  { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
  { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
  { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
  { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
  { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 },
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
  { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 }
};

static const uint64_t blake2b_IV[8] = {
  UINT64_C(0x6a09e667f3bcc908), UINT64_C(0xbb67ae8584caa73b),
  UINT64_C(0x3c6ef372fe94f82b), UINT64_C(0xa54ff53a5f1d36f1),
  UINT64_C(0x510e527fade682d1), UINT64_C(0x9b05688c2b3e6c1f),
  UINT64_C(0x1f83d9abfb41bd6b), UINT64_C(0x5be0cd19137e2179)
};

/* Mix function, G */
#define G(r, i, a, b, c, d)                                                    \
  {                                                                            \
    a = a + b + m[sigma[r][2 * i + 0]];                                        \
    d = ROTR64(d ^ a, 32);                                                     \
    c = c + d;                                                                 \
    b = ROTR64(b ^ c, 24);                                                     \
    a = a + b + m[sigma[r][2 * i + 1]];                                        \
    d = ROTR64(d ^ a, 16);                                                     \
    c = c + d;                                                                 \
    b = ROTR64(b ^ c, 63);                                                     \
  }

/* a round */
#define ROUND(r)                                                               \
  {                                                                            \
    G(r, 0, v[0], v[4], v[8], v[12]);                                          \
    G(r, 1, v[1], v[5], v[9], v[13]);                                          \
    G(r, 2, v[2], v[6], v[10], v[14]);                                         \
    G(r, 3, v[3], v[7], v[11], v[15]);                                         \
    G(r, 4, v[0], v[5], v[10], v[15]);                                         \
    G(r, 5, v[1], v[6], v[11], v[12]);                                         \
    G(r, 6, v[2], v[7], v[8], v[13]);                                          \
    G(r, 7, v[3], v[4], v[9], v[14]);                                          \
  }

static inline void compress(uint64_t *h, uint8_t *msg, uint64_t *t, int f)
{
  uint64_t v[16], m[16];

  /* Load the internal state */
  memcpy(v, h, 64);
  memcpy(v + 8, blake2b_IV, 64);
  if (f) {
    v[14] = ~v[14];
  }

  v[12] ^= t[0];
  v[13] ^= t[1];

  /* Load the messages into array */
  m[0] = LOAD64_LE(msg);
  m[1] = LOAD64_LE(msg + 8);
  m[2] = LOAD64_LE(msg + 16);
  m[3] = LOAD64_LE(msg + 24);
  m[4] = LOAD64_LE(msg + 32);
  m[5] = LOAD64_LE(msg + 40);
  m[6] = LOAD64_LE(msg + 48);
  m[7] = LOAD64_LE(msg + 56);
  m[8] = LOAD64_LE(msg + 64);
  m[9] = LOAD64_LE(msg + 72);
  m[10] = LOAD64_LE(msg + 80);
  m[11] = LOAD64_LE(msg + 88);
  m[12] = LOAD64_LE(msg + 96);
  m[13] = LOAD64_LE(msg + 104);
  m[14] = LOAD64_LE(msg + 112);
  m[15] = LOAD64_LE(msg + 120);

  /* Run 12 rounds */
  ROUND(0);
  ROUND(1);
  ROUND(2);
  ROUND(3);
  ROUND(4);
  ROUND(5);
  ROUND(6);
  ROUND(7);
  ROUND(8);
  ROUND(9);
  ROUND(10);
  ROUND(11);

  /* Xoring and Feedforward */
  h[0] ^= v[0] ^ v[8];
  h[1] ^= v[1] ^ v[9];
  h[2] ^= v[2] ^ v[10];
  h[3] ^= v[3] ^ v[11];
  h[4] ^= v[4] ^ v[12];
  h[5] ^= v[5] ^ v[13];
  h[6] ^= v[6] ^ v[14];
  h[7] ^= v[7] ^ v[15];
}

void mcrypto_blake2b_update(mcrypto_blake2b_ctx *ctx, uint8_t *msg,
                            uint64_t length)
{
  uint_fast8_t fill = 128 - ctx->buflen;
  uint8_t *in = msg;
  uint64_t counter[2] = { ctx->compressed[0], ctx->compressed[1] };

  if (length <= (uint64_t)fill) {
    /* fill the buffer */
    memcpy(ctx->buffer + ctx->buflen, msg, length);
    ctx->buflen += length;
    return;
  } else {
    /* fill the buffer first */
    memcpy(ctx->buffer + ctx->buflen, msg, fill);
    in += fill;
    length -= fill;

    /* increase the counter */
    counter[0] += 128;
    counter[1] += counter[0] == 0;
    /* compress the buffer */
    compress(ctx->h, ctx->buffer, counter, 0);

    /* keep compressing */
    while (length > 128) {
      /* increase the counter */
      counter[0] += 128;
      counter[1] += counter[0] == 0;
      compress(ctx->h, in, counter, 0);
      in += 128;
      length -= 128;
    }

    /* store the last block, length <= 128 */
    memcpy(ctx->buffer, in, length);
    ctx->buflen = length;
    memcpy(ctx->compressed, counter, 16);
  }
}

int mcrypto_blake2b_init(mcrypto_blake2b_ctx *ctx, uint8_t *key, uint8_t keylen,
                         uint8_t hashlen)
{
  /* check the parameter */
  if (keylen > 64 || hashlen > 64)
    return -1;

  /* init the context */
  memcpy(ctx->h, blake2b_IV, 64);
  ctx->h[0] ^=
      UINT64_C(0x01010000) ^ ((uint64_t)keylen << 8) ^ (uint64_t)hashlen;
  ctx->hash_length = hashlen;
  ctx->compressed[0] = 0;
  ctx->compressed[1] = 0;
  ctx->buflen = 0;

  if (keylen == 0)
    return 0;

  /* zero the buffer */
  memset(ctx->buffer, 0, 128);

  /* copy the key to the buffer */
  memcpy(ctx->buffer, key, keylen);

  /* with padding */
  ctx->buflen = 128;

  return 0;
}

int mcrypto_blake2b_final(mcrypto_blake2b_ctx *ctx, uint8_t *out,
                          uint8_t out_length)
{
  /* check the parameter */
  if (out_length < ctx->hash_length)
    return -1; /* the output buffer is too small */

  /* increase the counter */
  ctx->compressed[0] += ctx->buflen;
  ctx->compressed[1] += ctx->compressed[0] < ctx->buflen;

  /* Padding */
  memset(ctx->buffer + ctx->buflen, 0, 128 - ctx->buflen);
  compress(ctx->h, ctx->buffer, ctx->compressed, 1);

  /* copy the hash */
  memcpy(out, ctx->h, ctx->hash_length);

  /* reset the ctx */
  mcrypto_blake2b_init(ctx, NULL, 0, 64);
  return 0;
}

int mcrypto_blake2b_easy(uint8_t *key, uint8_t key_length, uint8_t *msg,
                         uint64_t length, uint8_t hash_length,
                         uint8_t *hash_out)
{
  mcrypto_blake2b_ctx ctx;

  /* Check the parameters */
  if (hash_length < 0 || hash_length > 64 || key_length > 64)
    return -1;

  if (length != 0 && msg == NULL)
    return -1;

  if (key_length != 0 && key == NULL)
    return -1;

  mcrypto_blake2b_init(&ctx, key, key_length, hash_length);
  mcrypto_blake2b_update(&ctx, msg, length);
  mcrypto_blake2b_final(&ctx, hash_out, hash_length);

  return 0;
}
