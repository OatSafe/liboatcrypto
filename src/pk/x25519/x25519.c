/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mcrypto/pk/x25519.h"
#include "../fe25519/fe.h"
#include "mcrypto/pk/ed25519.h"
#include "mcrypto/random/system.h"
#include "scalarmult.h"
#include <stdint.h>
#include <string.h>

static void clamp(uint8_t *input)
{
  input[0] &= 248;
  input[31] &= 63;
  input[31] |= 64;
}

void mcrypto_x25519_from_ed25519_pk(mcrypto_x25519_pubkey *xpk,
                                    const mcrypto_ed25519_pubkey *epk)
{

}

void mcrypto_x25519_from_ed25519_kp(mcrypto_x25519_keypair *xkp,
                                    const mcrypto_ed25519_keypair *ekp)
{
}

void mcrypto_x25519_kp_from_sk(mcrypto_x25519_keypair *kp, uint8_t sk[32])
{
  memcpy(kp->skey, sk, 32);
  clamp(kp->skey);
  mcrypto_x25519_scalarmult_base(kp->skey, kp->pk.key);
}

int mcrypto_x25519_keygen(mcrypto_x25519_keypair *kp,
                          int (*randombytes)(uint8_t *buffer, size_t size))
{
  if (randombytes == NULL) {
    randombytes = mcrypto_sysrandom_randombytes;
  }

  if (randombytes(kp->skey, 32) != 0) {
    return -1;
  }

  clamp(kp->skey);
  mcrypto_x25519_scalarmult_base(kp->skey, kp->pk.key);

  return 0;
}

void mcrypto_x25519_scalarmult(const uint8_t *input, const uint8_t *scalar,
                               uint8_t *output)
{
  _mcrypto_x25519_scalarmult(output, scalar, input);
}

void mcrypto_x25519_scalarmult_base(const uint8_t *input, uint8_t *output)
{
  static const uint8_t constant[32] = { 9 };
  _mcrypto_x25519_scalarmult(output, input, constant);
}
