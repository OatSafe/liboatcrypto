#ifndef _MCRYPTO_X25519_SCALARMULT_H
#define _MCRYPTO_X25519_SCALARMULT_H
#include <stdint.h>

int _mcrypto_x25519_scalarmult(unsigned char *q, const unsigned char *n,
                               const unsigned char *p);

#endif