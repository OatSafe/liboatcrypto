#ifndef _MCRYPTO_X448_SCALARMULT_H
#define _MCRYPTO_X448_SCALARMULT_H
#include <stdint.h>

int _mcrypto_x448_scalarmult(unsigned char *q, const unsigned char *n,
                             const unsigned char *p);

#endif