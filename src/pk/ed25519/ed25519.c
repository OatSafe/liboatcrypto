/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Copyright (c) 2015 Orson Peters <orsonpeters@gmail.com>
 *
 * The codes in the function 'mcrypto_ed25519_sign' and 
 * mcrypto_ed25519_verify are derived from Orson Peters works,
 * and is licensed under zlib license.
 * Please see NOTICE.
 */

#include "mcrypto/pk/ed25519.h"
#include "ge/ge.h"
#include "mcrypto/hash/sha512.h"
#include "mcrypto/random/system.h"
#include "sc/sc.h"
#include "mcrypto/macros.h"
#include <stdint.h>
#include <string.h>

static inline void clamp(uint8_t *input)
{
  input[0] &= 248;
  input[31] &= 63;
  input[31] |= 64;
}

void mcrypto_ed25519_kp_from_sk(mcrypto_ed25519_keypair *kp, uint8_t sk[32])
{
  mcrypto_sha512_ctx ctx;
  mcrypto_sha512_init(&ctx);
  uint8_t tmp[64];
  memcpy(kp->skey, sk, 32);
  mcrypto_sha512_update(&ctx, kp->skey, 32);
  mcrypto_sha512_final(&ctx, tmp);
  clamp(tmp);
  mcrypto_ed25519_scalarmult_base(tmp, kp->pk.key);
}

int mcrypto_ed25519_keygen(mcrypto_ed25519_keypair *kp,
                           int (*randombytes)(uint8_t *buffer, size_t size))
{
  uint8_t key[57];

  if (randombytes == NULL) {
    randombytes = mcrypto_sysrandom_randombytes;
  }

  if (randombytes(key, 57) != 0) {
    return -1;
  }

  mcrypto_ed25519_kp_from_sk(kp, key);

  return 0;
}

void mcrypto_ed25519_scalarmult_base(uint8_t *input, uint8_t *output)
{
  ge_p3 P;
  ge_scalarmult_base(&P, input);
  ge_p3_tobytes(output, &P);
}

/*
 * Copyright (c) 2015 Orson Peters <orsonpeters@gmail.com>
 *
 * The codes in the function 'mcrypto_ed25519_sign' and 
 * mcrypto_ed25519_verify are derived from Orson Peters works.
 * Please see NOTICE.
 */

void mcrypto_ed25519_sign(uint8_t *sig, const uint8_t *msg, const size_t mlen,
                          const mcrypto_ed25519_keypair *kp)
{
  mcrypto_sha512_ctx hash;
  uint8_t hram[64];
  uint8_t r[64];
  uint8_t skey[64];
  ge_p3 R;

  mcrypto_sha512_init(&hash);
  mcrypto_sha512_update(&hash, kp->skey, 32);
  mcrypto_sha512_final(&hash, skey);
  clamp(skey);

  mcrypto_sha512_init(&hash);
  mcrypto_sha512_update(&hash, skey + 32, 32);
  mcrypto_sha512_update(&hash, msg, mlen);
  mcrypto_sha512_final(&hash, r);

  sc_reduce(r);
  ge_scalarmult_base(&R, r);
  ge_p3_tobytes(sig, &R);

  mcrypto_sha512_init(&hash);
  mcrypto_sha512_update(&hash, sig, 32);
  mcrypto_sha512_update(&hash, kp->pk.key, 32);
  mcrypto_sha512_update(&hash, msg, mlen);
  mcrypto_sha512_final(&hash, hram);

  sc_reduce(hram);
  sc_muladd(sig + 32, hram, skey, r);
}

int mcrypto_ed25519_verify(const uint8_t *msg, size_t mlen, const uint8_t *sig,
                           const mcrypto_ed25519_pubkey *pk)
{
  uint8_t h[64];
  uint8_t checker[32];
  mcrypto_sha512_ctx hash;
  ge_p3 A;
  ge_p2 R;

  if (sig[63] & 224) {
    return -1;
  }

  if (ge_frombytes_negate_vartime(&A, pk->key) != 0) {
    return -1;
  }

  mcrypto_sha512_init(&hash);
  mcrypto_sha512_update(&hash, sig, 32);
  mcrypto_sha512_update(&hash, pk->key, 32);
  mcrypto_sha512_update(&hash, msg, mlen);
  mcrypto_sha512_final(&hash, h);

  sc_reduce(h);
  ge_double_scalarmult_vartime(&R, h, &A, sig + 32);
  ge_tobytes(checker, &R);

  if (memcmp(checker, sig, 32) != 0) {
    return -1;
  }

  return 0;
}
