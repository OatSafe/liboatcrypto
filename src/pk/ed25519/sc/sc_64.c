#include "sc_64.h"
#include "mcrypto/macros.h"
#include <stdint.h>
#include <stdio.h>

/* define 128bit integer type */
typedef __int128_t int128_t;
typedef __uint128_t uint128_t;

static uint64_t load_6(const unsigned char *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  result |= ((uint64_t)in[3]) << 24;
  result |= ((uint64_t)in[4]) << 32;
  result |= ((uint64_t)in[5]) << 40;
  return result;
}

void sc_reduce(uint8_t *s)
{
  int128_t s0 = load_6(s) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s1 = (load_6(s + 5) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s2 = (load_6(s + 10) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s3 = (load_6(s + 15) >> 6);
  int128_t s4 = load_6(s + 21) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s5 = (load_6(s + 26) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s6 = (load_6(s + 31) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s7 = (load_6(s + 36) >> 6);
  int128_t s8 = load_6(s + 42) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s9 = (load_6(s + 47) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s10 = (load_6(s + 52) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int128_t s11 = (load_6(s + 57) >> 6);
  int128_t carry;

  s5 += s11 * INT64_C(986282863635);
  s6 -= s11 * INT64_C(2092548097177);
  s7 -= s11 * INT64_C(1434244213295);
  s11 = 0;

  s4 += s10 * INT64_C(986282863635);
  s5 -= s10 * INT64_C(2092548097177);
  s6 -= s10 * INT64_C(1434244213295);
  s10 = 0;

  s3 += s9 * INT64_C(986282863635);
  s4 -= s9 * INT64_C(2092548097177);
  s5 -= s9 * INT64_C(1434244213295);
  s9 = 0;

  carry = (s3 + ((int128_t)1 << 41)) >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = (s5 + ((int128_t)1 << 41)) >> 42;
  s6 += carry;
  s5 -= carry << 42;
  carry = (s7 + ((int128_t)1 << 41)) >> 42;
  s8 += carry;
  s7 -= carry << 42;
  carry = (s4 + ((int128_t)1 << 41)) >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = (s6 + ((int128_t)1 << 41)) >> 42;
  s7 += carry;
  s6 -= carry << 42;

  s2 += s8 * INT64_C(986282863635);
  s3 -= s8 * INT64_C(2092548097177);
  s4 -= s8 * INT64_C(1434244213295);
  s8 = 0;

  s1 += s7 * INT64_C(986282863635);
  s2 -= s7 * INT64_C(2092548097177);
  s3 -= s7 * INT64_C(1434244213295);
  s7 = 0;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = (s0 + ((int128_t)1 << 41)) >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = (s2 + ((int128_t)1 << 41)) >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = (s4 + ((int128_t)1 << 41)) >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = (s1 + ((int128_t)1 << 41)) >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = (s3 + ((int128_t)1 << 41)) >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = (s5 + ((int128_t)1 << 41)) >> 42;
  s6 += carry;
  s5 -= carry << 42;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = s0 >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = s1 >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = s2 >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = s3 >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = s4 >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = s5 >> 42;
  s6 += carry;
  s5 -= carry << 42;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = s0 >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = s1 >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = s2 >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = s3 >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = s4 >> 42;
  s5 += carry;
  s4 -= carry << 42;

  s[0] = s0;
  s[1] = s0 >> 8;
  s[2] = s0 >> 16;
  s[3] = s0 >> 24;
  s[4] = s0 >> 32;
  s[5] = s0 >> 40 | s1 << 2;
  s[6] = s1 >> 6;
  s[7] = s1 >> 14;
  s[8] = s1 >> 22;
  s[9] = s1 >> 30;
  s[10] = s1 >> 38 | s2 << 4;
  s[11] = s2 >> 4;
  s[12] = s2 >> 12;
  s[13] = s2 >> 20;
  s[14] = s2 >> 28;
  s[15] = s2 >> 36 | s3 << 6;
  s[16] = s3 >> 2;
  s[17] = s3 >> 10;
  s[18] = s3 >> 18;
  s[19] = s3 >> 26;
  s[20] = s3 >> 34;
  s[21] = s4;
  s[22] = s4 >> 8;
  s[23] = s4 >> 16;
  s[24] = s4 >> 24;
  s[25] = s4 >> 32;
  s[26] = s4 >> 40 | s5 << 2;
  s[27] = s5 >> 6;
  s[28] = s5 >> 14;
  s[29] = s5 >> 22;
  s[30] = s5 >> 30;
  s[31] = s5 >> 38;
}

void sc_muladd(unsigned char *s, const unsigned char *a, const unsigned char *b,
               const unsigned char *c)
{
  int64_t a0 = load_6(a) & UINT64_C(0x3FFFFFFFFFF);
  int64_t a1 = (load_6(a + 5) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int64_t a2 = (load_6(a + 10) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int64_t a3 = (load_6(a + 15) >> 6);
  int64_t a4 = load_6(a + 21) & UINT64_C(0x3FFFFFFFFFF);
  int64_t a5 = load_6(a + 26) >> 2;
  int64_t b0 = load_6(b) & UINT64_C(0x3FFFFFFFFFF);
  int64_t b1 = (load_6(b + 5) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int64_t b2 = (load_6(b + 10) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int64_t b3 = (load_6(b + 15) >> 6);
  int64_t b4 = load_6(b + 21) & UINT64_C(0x3FFFFFFFFFF);
  int64_t b5 = load_6(b + 26) >> 2;
  int64_t c0 = load_6(c) & UINT64_C(0x3FFFFFFFFFF);
  int64_t c1 = (load_6(c + 5) >> 2) & UINT64_C(0x3FFFFFFFFFF);
  int64_t c2 = (load_6(c + 10) >> 4) & UINT64_C(0x3FFFFFFFFFF);
  int64_t c3 = (load_6(c + 15) >> 6);
  int64_t c4 = load_6(c + 21) & UINT64_C(0x3FFFFFFFFFF);
  int64_t c5 = load_6(c + 26) >> 2;
  int128_t s0 = c0 + (int128_t)a0 * b0;
  int128_t s1 = c1 + (int128_t)a0 * b1 + (int128_t)a1 * b0;
  int128_t s2 = c2 + (int128_t)a0 * b2 + (int128_t)a1 * b1 + (int128_t)a2 * b0;
  int128_t s3 = c3 + (int128_t)a0 * b3 + (int128_t)a1 * b2 + (int128_t)a2 * b1 +
                (int128_t)a3 * b0;
  int128_t s4 = c4 + (int128_t)a0 * b4 + (int128_t)a1 * b3 + (int128_t)a2 * b2 +
                (int128_t)a3 * b1 + (int128_t)a4 * b0;
  int128_t s5 = c5 + (int128_t)a0 * b5 + (int128_t)a1 * b4 + (int128_t)a2 * b3 +
                (int128_t)a3 * b2 + (int128_t)a4 * b1 + (int128_t)a5 * b0;
  int128_t s6 = (int128_t)a1 * b5 + (int128_t)a2 * b4 + (int128_t)a3 * b3 +
                (int128_t)a4 * b2 + (int128_t)a5 * b1;
  int128_t s7 = (int128_t)a2 * b5 + (int128_t)a3 * b4 + (int128_t)a4 * b3 +
                (int128_t)a5 * b2;
  int128_t s8 = (int128_t)a3 * b5 + (int128_t)a4 * b4 + (int128_t)a5 * b3;
  int128_t s9 = (int128_t)a4 * b5 + (int128_t)a5 * b4;
  int128_t s10 = (int128_t)a5 * b5;
  int128_t s11 = 0;

  int128_t carry;
  carry = (s0 + ((int128_t)1 << 41)) >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = (s1 + ((int128_t)1 << 41)) >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = (s2 + ((int128_t)1 << 41)) >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = (s3 + ((int128_t)1 << 41)) >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = (s4 + ((int128_t)1 << 41)) >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = (s5 + ((int128_t)1 << 41)) >> 42;
  s6 += carry;
  s5 -= carry << 42;
  carry = (s6 + ((int128_t)1 << 41)) >> 42;
  s7 += carry;
  s6 -= carry << 42;
  carry = (s7 + ((int128_t)1 << 41)) >> 42;
  s8 += carry;
  s7 -= carry << 42;
  carry = (s8 + ((int128_t)1 << 41)) >> 42;
  s9 += carry;
  s8 -= carry << 42;
  carry = (s9 + ((int128_t)1 << 41)) >> 42;
  s10 += carry;
  s9 -= carry << 42;
  carry = (s10 + ((int128_t)1 << 41)) >> 42;
  s11 += carry;
  s10 -= carry << 42;

  s5 += s11 * INT64_C(986282863635);
  s6 -= s11 * INT64_C(2092548097177);
  s7 -= s11 * INT64_C(1434244213295);
  s11 = 0;

  s4 += s10 * INT64_C(986282863635);
  s5 -= s10 * INT64_C(2092548097177);
  s6 -= s10 * INT64_C(1434244213295);
  s10 = 0;

  s3 += s9 * INT64_C(986282863635);
  s4 -= s9 * INT64_C(2092548097177);
  s5 -= s9 * INT64_C(1434244213295);
  s9 = 0;

  carry = (s3 + ((int128_t)1 << 41)) >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = (s5 + ((int128_t)1 << 41)) >> 42;
  s6 += carry;
  s5 -= carry << 42;
  carry = (s7 + ((int128_t)1 << 41)) >> 42;
  s8 += carry;
  s7 -= carry << 42;
  carry = (s4 + ((int128_t)1 << 41)) >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = (s6 + ((int128_t)1 << 41)) >> 42;
  s7 += carry;
  s6 -= carry << 42;

  s2 += s8 * INT64_C(986282863635);
  s3 -= s8 * INT64_C(2092548097177);
  s4 -= s8 * INT64_C(1434244213295);
  s8 = 0;

  s1 += s7 * INT64_C(986282863635);
  s2 -= s7 * INT64_C(2092548097177);
  s3 -= s7 * INT64_C(1434244213295);
  s7 = 0;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = (s0 + ((int128_t)1 << 41)) >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = (s2 + ((int128_t)1 << 41)) >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = (s4 + ((int128_t)1 << 41)) >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = (s1 + ((int128_t)1 << 41)) >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = (s3 + ((int128_t)1 << 41)) >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = (s5 + ((int128_t)1 << 41)) >> 42;
  s6 += carry;
  s5 -= carry << 42;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = s0 >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = s1 >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = s2 >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = s3 >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = s4 >> 42;
  s5 += carry;
  s4 -= carry << 42;
  carry = s5 >> 42;
  s6 += carry;
  s5 -= carry << 42;

  s0 += s6 * INT64_C(986282863635);
  s1 -= s6 * INT64_C(2092548097177);
  s2 -= s6 * INT64_C(1434244213295);
  s6 = 0;

  carry = s0 >> 42;
  s1 += carry;
  s0 -= carry << 42;
  carry = s1 >> 42;
  s2 += carry;
  s1 -= carry << 42;
  carry = s2 >> 42;
  s3 += carry;
  s2 -= carry << 42;
  carry = s3 >> 42;
  s4 += carry;
  s3 -= carry << 42;
  carry = s4 >> 42;
  s5 += carry;
  s4 -= carry << 42;

  s[0] = s0;
  s[1] = s0 >> 8;
  s[2] = s0 >> 16;
  s[3] = s0 >> 24;
  s[4] = s0 >> 32;
  s[5] = s0 >> 40 | s1 << 2;
  s[6] = s1 >> 6;
  s[7] = s1 >> 14;
  s[8] = s1 >> 22;
  s[9] = s1 >> 30;
  s[10] = s1 >> 38 | s2 << 4;
  s[11] = s2 >> 4;
  s[12] = s2 >> 12;
  s[13] = s2 >> 20;
  s[14] = s2 >> 28;
  s[15] = s2 >> 36 | s3 << 6;
  s[16] = s3 >> 2;
  s[17] = s3 >> 10;
  s[18] = s3 >> 18;
  s[19] = s3 >> 26;
  s[20] = s3 >> 34;
  s[21] = s4;
  s[22] = s4 >> 8;
  s[23] = s4 >> 16;
  s[24] = s4 >> 24;
  s[25] = s4 >> 32;
  s[26] = s4 >> 40 | s5 << 2;
  s[27] = s5 >> 6;
  s[28] = s5 >> 14;
  s[29] = s5 >> 22;
  s[30] = s5 >> 30;
  s[31] = s5 >> 38;
}