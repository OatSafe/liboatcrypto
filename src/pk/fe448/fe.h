/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MCRYPTO_PK_FE448_FE_H
#define _MCRYPTO_PK_FE448_FE_H

#include "mcrypto/bits.h"
#include <stdint.h>

#define fe_frombytes mcrypto_fe448_frombytes
#define fe_tobytes mcrypto_fe448_tobytes
#define fe_copy mcrypto_fe448_copy
#define fe_isnonzero mcrypto_fe448_isnonzero
#define fe_isnegative mcrypto_fe448_isnegative
#define fe_0 mcrypto_fe448_0
#define fe_1 mcrypto_fe448_1
#define fe_cswap mcrypto_fe448_cswap
#define fe_cmov mcrypto_fe448_cmov
#define fe_add mcrypto_fe448_add
#define fe_sub mcrypto_fe448_sub
#define fe_neg mcrypto_fe448_neg
#define fe_mul mcrypto_fe448_mul
#define fe_sq mcrypto_fe448_sq
#define fe_invert mcrypto_fe448_invert
#define fe_pow_p448s3d4 mcrypto_fe448_pow_p448s3d4
#define fe_mul39081 mcrypto_fe448_mul39081
#define fe_mul39082 mcrypto_fe448_mul39082
#define fe_mulm39081 mcrypto_fe448_mulm39081

#ifdef MCRYPTO_NATIVE_64BIT
#include "fe_64.h"
#else
#include "fe_32.h"
#endif

void fe_frombytes(fe, const uint8_t *);
void fe_tobytes(uint8_t *, const fe);

void fe_copy(fe, const fe);
int fe_isnonzero(const fe);
int fe_isnegative(const fe);
void fe_0(fe);
void fe_1(fe);

void fe_add(fe, const fe, const fe);
void fe_sub(fe, const fe, const fe);
void fe_neg(fe, const fe);
void fe_mul(fe, const fe, const fe);
void fe_sq(fe, const fe);
void fe_invert(fe, const fe);
void fe_mul39081(fe, const fe);
void fe_mul39082(fe, const fe);
void fe_mulm39081(fe, const fe);
void fe_pow_p448s3d4(fe h, const fe f);

#endif
