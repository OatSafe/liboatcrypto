/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CARRY_SIGNED(a, b, c)                                                  \
  {                                                                            \
    c = (a + ((int64_t)1 << 55)) >> 56;                                        \
    b += c;                                                                    \
    a -= c << 56;                                                              \
  }

#define CARRY_SIGNED128(a, b, c)                                               \
  {                                                                            \
    c = (a + ((int128_t)1 << 55)) >> 56;                                       \
    b += c;                                                                    \
    a -= c << 56;                                                              \
  }

#define CARRY_SIGNED_ALL(h, c)                                                 \
  {                                                                            \
    CARRY_SIGNED(h##0, h##1, c);                                               \
    CARRY_SIGNED(h##2, h##3, c);                                               \
    CARRY_SIGNED(h##4, h##5, c);                                               \
    CARRY_SIGNED(h##6, h##7, c);                                               \
    CARRY_SIGNED(h##1, h##2, c);                                               \
    CARRY_SIGNED(h##3, h##4, c);                                               \
    CARRY_SIGNED(h##5, h##6, c);                                               \
  }

#define CARRY_SIGNED128_ALL(h, c)                                              \
  {                                                                            \
    CARRY_SIGNED128(h##0, h##1, c);                                            \
    CARRY_SIGNED128(h##2, h##3, c);                                            \
    CARRY_SIGNED128(h##4, h##5, c);                                            \
    CARRY_SIGNED128(h##6, h##7, c);                                            \
    CARRY_SIGNED128(h##1, h##2, c);                                            \
    CARRY_SIGNED128(h##3, h##4, c);                                            \
    CARRY_SIGNED128(h##5, h##6, c);                                            \
  }

#define DECL(n) int64_t n##0, n##1, n##2, n##3, n##4, n##5, n##6, n##7;

#define DECL128(n) int128_t n##0, n##1, n##2, n##3, n##4, n##5, n##6, n##7;

#define LOAD(n, a)                                                             \
  {                                                                            \
    n##0 = (a)[0];                                                             \
    n##1 = (a)[1];                                                             \
    n##2 = (a)[2];                                                             \
    n##3 = (a)[3];                                                             \
    n##4 = (a)[4];                                                             \
    n##5 = (a)[5];                                                             \
    n##6 = (a)[6];                                                             \
    n##7 = (a)[7];                                                             \
  }

#define STORE(n, a)                                                            \
  {                                                                            \
    (a)[0] = n##0;                                                             \
    (a)[1] = n##1;                                                             \
    (a)[2] = n##2;                                                             \
    (a)[3] = n##3;                                                             \
    (a)[4] = n##4;                                                             \
    (a)[5] = n##5;                                                             \
    (a)[6] = n##6;                                                             \
    (a)[7] = n##7;                                                             \
  }

#define OPERATE(a, b, c, o)                                                    \
  {                                                                            \
    a##0 = b##0 o c##0;                                                        \
    a##1 = b##1 o c##1;                                                        \
    a##2 = b##2 o c##2;                                                        \
    a##3 = b##3 o c##3;                                                        \
    a##4 = b##4 o c##4;                                                        \
    a##5 = b##5 o c##5;                                                        \
    a##6 = b##6 o c##6;                                                        \
    a##7 = b##7 o c##7;                                                        \
  }

static inline uint64_t load_7(const uint8_t *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  result |= ((uint64_t)in[3]) << 24;
  result |= ((uint64_t)in[4]) << 32;
  result |= ((uint64_t)in[5]) << 40;
  result |= ((uint64_t)in[6]) << 48;
  return result;
}

void fe_frombytes(fe h, const uint8_t *s)
{
  int64_t h0 = load_7(s);
  int64_t h1 = load_7(s + 7);
  int64_t h2 = load_7(s + 14);
  int64_t h3 = load_7(s + 21);
  int64_t h4 = load_7(s + 28);
  int64_t h5 = load_7(s + 35);
  int64_t h6 = load_7(s + 42);
  int64_t h7 = load_7(s + 49);

  STORE(h, h);
}

void fe_tobytes(uint8_t *s, const fe h)
{
  DECL(h);
  LOAD(h, h);

  int64_t carry;

  CARRY_SIGNED_ALL(h, carry)
  CARRY_SIGNED(h7, h0, carry);
  h4 += carry;

#define CARRY(a, b, c)                                                         \
  {                                                                            \
    c = a >> 56;                                                               \
    b += c;                                                                    \
    a -= c << 56;                                                              \
  }

/* -2^225 < x < 2^225 */

#define CARRY_ALL(h, c)                                                        \
  {                                                                            \
    CARRY(h##7, h##0, c);                                                      \
    h##4 += c;                                                                 \
    CARRY(h##0, h##1, c);                                                      \
    CARRY(h##1, h##2, c);                                                      \
    CARRY(h##2, h##3, c);                                                      \
    CARRY(h##3, h##4, c);                                                      \
    CARRY(h##4, h##5, c);                                                      \
    CARRY(h##5, h##6, c);                                                      \
    CARRY(h##6, h##7, c);                                                      \
  }

  CARRY_ALL(h, carry);

  carry = h7 >> 56;
  h0 += carry;
  h4 += carry;
  h7 -= carry << 56;

  CARRY_ALL(h, carry);

#undef CARRY
#undef CARRY_ALL

#define STORE8(v, a)                                                           \
  {                                                                            \
    (v)[0] = (uint64_t)a;                                                      \
    (v)[1] = (uint64_t)a >> 8;                                                 \
    (v)[2] = (uint64_t)a >> 16;                                                \
    (v)[3] = (uint64_t)a >> 24;                                                \
    (v)[4] = (uint64_t)a >> 32;                                                \
    (v)[5] = (uint64_t)a >> 40;                                                \
    (v)[6] = (uint64_t)a >> 48;                                                \
  }

  STORE8(s, h0);
  STORE8(s + 7, h1);
  STORE8(s + 14, h2);
  STORE8(s + 21, h3);
  STORE8(s + 28, h4);
  STORE8(s + 35, h5);
  STORE8(s + 42, h6);
  STORE8(s + 49, h7);

#undef STORE8
}

/*
h = f
*/

void fe_copy(fe h, const fe f)
{
  DECL(f);
  LOAD(f, f);
  STORE(f, h);
}

/*
return 1 if f == 0
return 0 if f != 0

Preconditions:
   |f| bounded by 1.1*2^26,1.1*2^25,1.1*2^26,1.1*2^25,etc.
*/

int fe_isnonzero(const fe f)
{
  uint8_t s[56];
  fe_tobytes(s, f);
  uint8_t r = 0;
  for (int i = 0; i < 56; i++)
    r |= s[i];
  return r != 0;
}

int fe_isnegative(const fe f)
{
  uint8_t s[56];
  fe_tobytes(s, f);
  return s[0] & 1;
}

/*
h = 0
*/

void fe_0(fe h)
{
  h[0] = 0;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
  h[5] = 0;
  h[6] = 0;
  h[7] = 0;
}

/*
h = 1
*/

void fe_1(fe h)
{
  h[0] = 1;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
  h[5] = 0;
  h[6] = 0;
  h[7] = 0;
}

/*
Replace (f,g) with (g,f) if b == 1;
replace (f,g) with (f,g) if b == 0.

Preconditions: b in {0,1}.
*/

void fe_cswap(fe f, fe g, uint64_t b)
{
  DECL(f);
  DECL(g);
  DECL(x);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(x, f, g, ^);
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  x5 &= b;
  x6 &= b;
  x7 &= b;
  OPERATE(f, f, x, ^);
  OPERATE(g, g, x, ^);
  STORE(f, f);
  STORE(g, g);
}

/*
Replace (f,g) with (g,g) if b == 1;
replace (f,g) with (f,g) if b == 0.

Preconditions: b in {0,1}.
*/

void fe_cmov(fe f, const fe g, uint64_t b)
{
  DECL(f);
  DECL(g);
  DECL(x);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(x, f, g, ^);
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  x5 &= b;
  x6 &= b;
  x7 &= b;
  OPERATE(f, f, x, ^);
  STORE(f, f);
}

/*
h = f + g
Can overlap h with f or g.

Preconditions:
   |f| bounded by 1.1*2^27,1.1*2^27,1.1*2^27,1.1*2^27,etc.
   |g| bounded by 1.1*2^27,1.1*2^27,1.1*2^27,1.1*2^27,etc.

Postconditions:
   |h| bounded by 1.1*2^28,1.1*2^28,1.1*2^28,1.1*2^28,etc.
*/

void fe_add(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  DECL(h);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(h, f, g, +)
  STORE(h, h);
}

void fe_sub(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  DECL(h);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(h, f, g, -)
  STORE(h, h);
}

void fe_mul(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  LOAD(f, f);
  LOAD(g, g);
  int128_t carry;
  int128_t f0g0 = (int128_t)f0 * g0;
  int128_t f0g1 = (int128_t)f0 * g1;
  int128_t f0g2 = (int128_t)f0 * g2;
  int128_t f0g3 = (int128_t)f0 * g3;
  int128_t f0g4 = (int128_t)f0 * g4;
  int128_t f0g5 = (int128_t)f0 * g5;
  int128_t f0g6 = (int128_t)f0 * g6;
  int128_t f0g7 = (int128_t)f0 * g7;
  int128_t f1g0 = (int128_t)f1 * g0;
  int128_t f1g1 = (int128_t)f1 * g1;
  int128_t f1g2 = (int128_t)f1 * g2;
  int128_t f1g3 = (int128_t)f1 * g3;
  int128_t f1g4 = (int128_t)f1 * g4;
  int128_t f1g5 = (int128_t)f1 * g5;
  int128_t f1g6 = (int128_t)f1 * g6;
  int128_t f1g7 = (int128_t)f1 * g7;
  int128_t f2g0 = (int128_t)f2 * g0;
  int128_t f2g1 = (int128_t)f2 * g1;
  int128_t f2g2 = (int128_t)f2 * g2;
  int128_t f2g3 = (int128_t)f2 * g3;
  int128_t f2g4 = (int128_t)f2 * g4;
  int128_t f2g5 = (int128_t)f2 * g5;
  int128_t f2g6 = (int128_t)f2 * g6;
  int128_t f2g7 = (int128_t)f2 * g7;
  int128_t f3g0 = (int128_t)f3 * g0;
  int128_t f3g1 = (int128_t)f3 * g1;
  int128_t f3g2 = (int128_t)f3 * g2;
  int128_t f3g3 = (int128_t)f3 * g3;
  int128_t f3g4 = (int128_t)f3 * g4;
  int128_t f3g5 = (int128_t)f3 * g5;
  int128_t f3g6 = (int128_t)f3 * g6;
  int128_t f3g7 = (int128_t)f3 * g7;
  int128_t f4g0 = (int128_t)f4 * g0;
  int128_t f4g1 = (int128_t)f4 * g1;
  int128_t f4g2 = (int128_t)f4 * g2;
  int128_t f4g3 = (int128_t)f4 * g3;
  int128_t f4g4 = (int128_t)f4 * g4;
  int128_t f4g5 = (int128_t)f4 * g5;
  int128_t f4g6 = (int128_t)f4 * g6;
  int128_t f4g7 = (int128_t)f4 * g7;
  int128_t f5g0 = (int128_t)f5 * g0;
  int128_t f5g1 = (int128_t)f5 * g1;
  int128_t f5g2 = (int128_t)f5 * g2;
  int128_t f5g3 = (int128_t)f5 * g3;
  int128_t f5g4 = (int128_t)f5 * g4;
  int128_t f5g5 = (int128_t)f5 * g5;
  int128_t f5g6 = (int128_t)f5 * g6;
  int128_t f5g7 = (int128_t)f5 * g7;
  int128_t f6g0 = (int128_t)f6 * g0;
  int128_t f6g1 = (int128_t)f6 * g1;
  int128_t f6g2 = (int128_t)f6 * g2;
  int128_t f6g3 = (int128_t)f6 * g3;
  int128_t f6g4 = (int128_t)f6 * g4;
  int128_t f6g5 = (int128_t)f6 * g5;
  int128_t f6g6 = (int128_t)f6 * g6;
  int128_t f6g7 = (int128_t)f6 * g7;
  int128_t f7g0 = (int128_t)f7 * g0;
  int128_t f7g1 = (int128_t)f7 * g1;
  int128_t f7g2 = (int128_t)f7 * g2;
  int128_t f7g3 = (int128_t)f7 * g3;
  int128_t f7g4 = (int128_t)f7 * g4;
  int128_t f7g5 = (int128_t)f7 * g5;
  int128_t f7g6 = (int128_t)f7 * g6;
  int128_t f7g7 = (int128_t)f7 * g7;
  int128_t h0 = f0g0 + f1g7 + f2g6 + f3g5 + f4g4 + f5g3 + f5g7 + f6g2 + f6g6 +
                f7g1 + f7g5;
  int128_t h1 =
      f0g1 + f1g0 + f2g7 + f3g6 + f4g5 + f5g4 + f6g3 + f6g7 + f7g2 + f7g6;
  int128_t h2 = f0g2 + f1g1 + f2g0 + f3g7 + f4g6 + f5g5 + f6g4 + f7g3 + f7g7;
  int128_t h3 = f0g3 + f1g2 + f2g1 + f3g0 + f4g7 + f5g6 + f6g5 + f7g4;
  int128_t h4 = f0g4 + f1g3 + f1g7 + f2g2 + f2g6 + f3g1 + f3g5 + f4g0 + f4g4 +
                f5g3 + f5g7 + f5g7 + f6g2 + f6g6 + f6g6 + f7g1 + f7g5 + f7g5;
  int128_t h5 = f0g5 + f1g4 + f2g3 + f2g7 + f3g2 + f3g6 + f4g1 + f4g5 + f5g0 +
                f5g4 + f6g3 + f6g7 + f6g7 + f7g2 + f7g6 + f7g6;
  int128_t h6 = f0g6 + f1g5 + f2g4 + f3g3 + f3g7 + f4g2 + f4g6 + f5g1 + f5g5 +
                f6g0 + f6g4 + f7g3 + f7g7 + f7g7;
  int128_t h7 = f0g7 + f1g6 + f2g5 + f3g4 + f4g3 + f4g7 + f5g2 + f5g6 + f6g1 +
                f6g5 + f7g0 + f7g4;

  CARRY_SIGNED128(h0, h1, carry);
  CARRY_SIGNED128(h3, h4, carry);

  CARRY_SIGNED128(h1, h2, carry);
  CARRY_SIGNED128(h4, h5, carry);

  CARRY_SIGNED128(h2, h3, carry);
  CARRY_SIGNED128(h5, h6, carry);

  CARRY_SIGNED128(h3, h4, carry);
  CARRY_SIGNED128(h6, h7, carry);

  CARRY_SIGNED128(h7, h0, carry);
  h4 += carry;

  CARRY_SIGNED128(h0, h1, carry);
  CARRY_SIGNED128(h4, h5, carry);

  STORE(h, h);
}

void fe_sq(fe h, const fe f)
{
  DECL(f);
  LOAD(f, f);
  int128_t carry;
  int128_t f0f0 = (int128_t)f0 * f0;
  int128_t f0f1 = (int128_t)f0 * f1;
  int128_t f0f2 = (int128_t)f0 * f2;
  int128_t f0f3 = (int128_t)f0 * f3;
  int128_t f0f4 = (int128_t)f0 * f4;
  int128_t f0f5 = (int128_t)f0 * f5;
  int128_t f0f6 = (int128_t)f0 * f6;
  int128_t f0f7 = (int128_t)f0 * f7;
  int128_t f1f0 = (int128_t)f1 * f0;
  int128_t f1f1 = (int128_t)f1 * f1;
  int128_t f1f2 = (int128_t)f1 * f2;
  int128_t f1f3 = (int128_t)f1 * f3;
  int128_t f1f4 = (int128_t)f1 * f4;
  int128_t f1f5 = (int128_t)f1 * f5;
  int128_t f1f6 = (int128_t)f1 * f6;
  int128_t f1f7 = (int128_t)f1 * f7;
  int128_t f2f0 = (int128_t)f2 * f0;
  int128_t f2f1 = (int128_t)f2 * f1;
  int128_t f2f2 = (int128_t)f2 * f2;
  int128_t f2f3 = (int128_t)f2 * f3;
  int128_t f2f4 = (int128_t)f2 * f4;
  int128_t f2f5 = (int128_t)f2 * f5;
  int128_t f2f6 = (int128_t)f2 * f6;
  int128_t f2f7 = (int128_t)f2 * f7;
  int128_t f3f0 = (int128_t)f3 * f0;
  int128_t f3f1 = (int128_t)f3 * f1;
  int128_t f3f2 = (int128_t)f3 * f2;
  int128_t f3f3 = (int128_t)f3 * f3;
  int128_t f3f4 = (int128_t)f3 * f4;
  int128_t f3f5 = (int128_t)f3 * f5;
  int128_t f3f6 = (int128_t)f3 * f6;
  int128_t f3f7 = (int128_t)f3 * f7;
  int128_t f4f0 = (int128_t)f4 * f0;
  int128_t f4f1 = (int128_t)f4 * f1;
  int128_t f4f2 = (int128_t)f4 * f2;
  int128_t f4f3 = (int128_t)f4 * f3;
  int128_t f4f4 = (int128_t)f4 * f4;
  int128_t f4f5 = (int128_t)f4 * f5;
  int128_t f4f6 = (int128_t)f4 * f6;
  int128_t f4f7 = (int128_t)f4 * f7;
  int128_t f5f0 = (int128_t)f5 * f0;
  int128_t f5f1 = (int128_t)f5 * f1;
  int128_t f5f2 = (int128_t)f5 * f2;
  int128_t f5f3 = (int128_t)f5 * f3;
  int128_t f5f4 = (int128_t)f5 * f4;
  int128_t f5f5 = (int128_t)f5 * f5;
  int128_t f5f6 = (int128_t)f5 * f6;
  int128_t f5f7 = (int128_t)f5 * f7;
  int128_t f6f0 = (int128_t)f6 * f0;
  int128_t f6f1 = (int128_t)f6 * f1;
  int128_t f6f2 = (int128_t)f6 * f2;
  int128_t f6f3 = (int128_t)f6 * f3;
  int128_t f6f4 = (int128_t)f6 * f4;
  int128_t f6f5 = (int128_t)f6 * f5;
  int128_t f6f6 = (int128_t)f6 * f6;
  int128_t f6f7 = (int128_t)f6 * f7;
  int128_t f7f0 = (int128_t)f7 * f0;
  int128_t f7f1 = (int128_t)f7 * f1;
  int128_t f7f2 = (int128_t)f7 * f2;
  int128_t f7f3 = (int128_t)f7 * f3;
  int128_t f7f4 = (int128_t)f7 * f4;
  int128_t f7f5 = (int128_t)f7 * f5;
  int128_t f7f6 = (int128_t)f7 * f6;
  int128_t f7f7 = (int128_t)f7 * f7;
  int128_t h0 = f0f0 + f1f7 + f2f6 + f3f5 + f4f4 + f5f3 + f5f7 + f6f2 + f6f6 +
                f7f1 + f7f5;
  int128_t h1 =
      f0f1 + f1f0 + f2f7 + f3f6 + f4f5 + f5f4 + f6f3 + f6f7 + f7f2 + f7f6;
  int128_t h2 = f0f2 + f1f1 + f2f0 + f3f7 + f4f6 + f5f5 + f6f4 + f7f3 + f7f7;
  int128_t h3 = f0f3 + f1f2 + f2f1 + f3f0 + f4f7 + f5f6 + f6f5 + f7f4;
  int128_t h4 = f0f4 + f1f3 + f1f7 + f2f2 + f2f6 + f3f1 + f3f5 + f4f0 + f4f4 +
                f5f3 + f5f7 + f5f7 + f6f2 + f6f6 + f6f6 + f7f1 + f7f5 + f7f5;
  int128_t h5 = f0f5 + f1f4 + f2f3 + f2f7 + f3f2 + f3f6 + f4f1 + f4f5 + f5f0 +
                f5f4 + f6f3 + f6f7 + f6f7 + f7f2 + f7f6 + f7f6;
  int128_t h6 = f0f6 + f1f5 + f2f4 + f3f3 + f3f7 + f4f2 + f4f6 + f5f1 + f5f5 +
                f6f0 + f6f4 + f7f3 + f7f7 + f7f7;
  int128_t h7 = f0f7 + f1f6 + f2f5 + f3f4 + f4f3 + f4f7 + f5f2 + f5f6 + f6f1 +
                f6f5 + f7f0 + f7f4;

  CARRY_SIGNED128(h0, h1, carry);
  CARRY_SIGNED128(h3, h4, carry);

  CARRY_SIGNED128(h1, h2, carry);
  CARRY_SIGNED128(h4, h5, carry);

  CARRY_SIGNED128(h2, h3, carry);
  CARRY_SIGNED128(h5, h6, carry);

  CARRY_SIGNED128(h3, h4, carry);
  CARRY_SIGNED128(h6, h7, carry);

  CARRY_SIGNED128(h7, h0, carry);
  h4 += carry;
  CARRY_SIGNED128(h0, h1, carry);
  CARRY_SIGNED128(h4, h5, carry);

  STORE(h, h);
}

void fe_invert(fe out, const fe z)
{
  fe t;
  int i;

  fe_copy(t, z);

  for (i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, z);
  }
  fe_sq(t, t);

  for (i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, z);
  }
  fe_sq(t, t);
  fe_sq(t, t);
  fe_mul(t, t, z);

  fe_copy(out, t);
}

void fe_mulm39081(fe h, const fe f)
{
  DECL128(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= -39081;
  h1 *= -39081;
  h2 *= -39081;
  h3 *= -39081;
  h4 *= -39081;
  h5 *= -39081;
  h6 *= -39081;
  h7 *= -39081;

  CARRY_SIGNED128(h7, h0, carry);
  h4 += carry;
  CARRY_SIGNED128_ALL(h, carry);

  STORE(h, h);
}

void fe_mul39081(fe h, const fe f)
{
  DECL128(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= 39081;
  h1 *= 39081;
  h2 *= 39081;
  h3 *= 39081;
  h4 *= 39081;
  h5 *= 39081;
  h6 *= 39081;
  h7 *= 39081;

  CARRY_SIGNED128(h7, h0, carry);
  h4 += carry;
  CARRY_SIGNED128_ALL(h, carry);

  STORE(h, h);
}

void fe_mul39082(fe h, const fe f)
{
  DECL128(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= 39082;
  h1 *= 39082;
  h2 *= 39082;
  h3 *= 39082;
  h4 *= 39082;
  h5 *= 39082;
  h6 *= 39082;
  h7 *= 39082;

  CARRY_SIGNED128(h7, h0, carry);
  h4 += carry;
  CARRY_SIGNED128_ALL(h, carry);

  STORE(h, h);
}

void fe_pow_p448s3d4(fe h, const fe f)
{
  fe t;
  fe_copy(t, f);
  for (int i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, f);
  }
  fe_sq(t, t);
  for (int i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, f);
  }
  fe_copy(h, t);
}

void fe_neg(fe h, const fe f)
{
  DECL(f);
  LOAD(f, f);
  int64_t h0 = -f0;
  int64_t h1 = -f1;
  int64_t h2 = -f2;
  int64_t h3 = -f3;
  int64_t h4 = -f4;
  int64_t h5 = -f5;
  int64_t h6 = -f6;
  int64_t h7 = -f7;
  STORE(h, h);
}
