/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CARRY_SIGNED(a, b, c)                                                  \
  {                                                                            \
    c = (a + (1 << 27)) >> 28;                                                 \
    b += c;                                                                    \
    a -= c << 28;                                                              \
  }

#define CARRY_SIGNED64(a, b, c)                                                \
  {                                                                            \
    c = (a + (int64_t)(1 << 27)) >> 28;                                        \
    b += c;                                                                    \
    a -= c << 28;                                                              \
  }

#define CARRY_SIGNED_ALL(h, c)                                                 \
  {                                                                            \
    CARRY_SIGNED(h##0, h##1, c);                                               \
    CARRY_SIGNED(h##2, h##3, c);                                               \
    CARRY_SIGNED(h##4, h##5, c);                                               \
    CARRY_SIGNED(h##6, h##7, c);                                               \
    CARRY_SIGNED(h##8, h##9, c);                                               \
    CARRY_SIGNED(h##10, h##11, c);                                             \
    CARRY_SIGNED(h##12, h##13, c);                                             \
    CARRY_SIGNED(h##14, h##15, c);                                             \
    CARRY_SIGNED(h##1, h##2, c);                                               \
    CARRY_SIGNED(h##3, h##4, c);                                               \
    CARRY_SIGNED(h##5, h##6, c);                                               \
    CARRY_SIGNED(h##7, h##8, c);                                               \
    CARRY_SIGNED(h##9, h##10, c);                                              \
    CARRY_SIGNED(h##11, h##12, c);                                             \
    CARRY_SIGNED(h##13, h##14, c);                                             \
  }

#define CARRY_SIGNED64_ALL(h, c)                                               \
  {                                                                            \
    CARRY_SIGNED64(h##0, h##1, c);                                             \
    CARRY_SIGNED64(h##2, h##3, c);                                             \
    CARRY_SIGNED64(h##4, h##5, c);                                             \
    CARRY_SIGNED64(h##6, h##7, c);                                             \
    CARRY_SIGNED64(h##8, h##9, c);                                             \
    CARRY_SIGNED64(h##10, h##11, c);                                           \
    CARRY_SIGNED64(h##12, h##13, c);                                           \
    CARRY_SIGNED64(h##14, h##15, c);                                           \
    CARRY_SIGNED64(h##1, h##2, c);                                             \
    CARRY_SIGNED64(h##3, h##4, c);                                             \
    CARRY_SIGNED64(h##5, h##6, c);                                             \
    CARRY_SIGNED64(h##7, h##8, c);                                             \
    CARRY_SIGNED64(h##9, h##10, c);                                            \
    CARRY_SIGNED64(h##11, h##12, c);                                           \
    CARRY_SIGNED64(h##13, h##14, c);                                           \
  }

#define DECL(n)                                                                \
  int32_t n##0, n##1, n##2, n##3, n##4, n##5, n##6, n##7, n##8, n##9, n##10,   \
      n##11, n##12, n##13, n##14, n##15;

#define DECL64(n)                                                              \
  int64_t n##0, n##1, n##2, n##3, n##4, n##5, n##6, n##7, n##8, n##9, n##10,   \
      n##11, n##12, n##13, n##14, n##15;

#define LOAD(n, a)                                                             \
  {                                                                            \
    n##0 = (a)[0];                                                             \
    n##1 = (a)[1];                                                             \
    n##2 = (a)[2];                                                             \
    n##3 = (a)[3];                                                             \
    n##4 = (a)[4];                                                             \
    n##5 = (a)[5];                                                             \
    n##6 = (a)[6];                                                             \
    n##7 = (a)[7];                                                             \
    n##8 = (a)[8];                                                             \
    n##9 = (a)[9];                                                             \
    n##10 = (a)[10];                                                           \
    n##11 = (a)[11];                                                           \
    n##12 = (a)[12];                                                           \
    n##13 = (a)[13];                                                           \
    n##14 = (a)[14];                                                           \
    n##15 = (a)[15];                                                           \
  }

#define STORE(n, a)                                                            \
  {                                                                            \
    (a)[0] = n##0;                                                             \
    (a)[1] = n##1;                                                             \
    (a)[2] = n##2;                                                             \
    (a)[3] = n##3;                                                             \
    (a)[4] = n##4;                                                             \
    (a)[5] = n##5;                                                             \
    (a)[6] = n##6;                                                             \
    (a)[7] = n##7;                                                             \
    (a)[8] = n##8;                                                             \
    (a)[9] = n##9;                                                             \
    (a)[10] = n##10;                                                           \
    (a)[11] = n##11;                                                           \
    (a)[12] = n##12;                                                           \
    (a)[13] = n##13;                                                           \
    (a)[14] = n##14;                                                           \
    (a)[15] = n##15;                                                           \
  }

#define OPERATE(a, b, c, o)                                                    \
  {                                                                            \
    a##0 = b##0 o c##0;                                                        \
    a##1 = b##1 o c##1;                                                        \
    a##2 = b##2 o c##2;                                                        \
    a##3 = b##3 o c##3;                                                        \
    a##4 = b##4 o c##4;                                                        \
    a##5 = b##5 o c##5;                                                        \
    a##6 = b##6 o c##6;                                                        \
    a##7 = b##7 o c##7;                                                        \
    a##8 = b##8 o c##8;                                                        \
    a##9 = b##9 o c##9;                                                        \
    a##10 = b##10 o c##10;                                                     \
    a##11 = b##11 o c##11;                                                     \
    a##12 = b##12 o c##12;                                                     \
    a##13 = b##13 o c##13;                                                     \
    a##14 = b##14 o c##14;                                                     \
    a##15 = b##15 o c##15;                                                     \
  }

static uint64_t load_3(const uint8_t *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  return result;
}

static uint64_t load_4(const uint8_t *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  result |= ((uint64_t)in[3]) << 24;
  return result;
}

void fe_frombytes(fe h, const uint8_t *s)
{
  int64_t h0 = load_4(s);
  int64_t h1 = load_3(s + 4) << 4;
  int64_t h2 = load_4(s + 7);
  int64_t h3 = load_3(s + 11) << 4;
  int64_t h4 = load_4(s + 14);
  int64_t h5 = load_3(s + 18) << 4;
  int64_t h6 = load_4(s + 21);
  int64_t h7 = load_3(s + 25) << 4;
  int64_t h8 = load_4(s + 28);
  int64_t h9 = load_3(s + 32) << 4;
  int64_t h10 = load_4(s + 35);
  int64_t h11 = load_3(s + 39) << 4;
  int64_t h12 = load_4(s + 42);
  int64_t h13 = load_3(s + 46) << 4;
  int64_t h14 = load_4(s + 49);
  int64_t h15 = load_3(s + 53) << 4;
  int64_t carry;

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;
  CARRY_SIGNED64_ALL(h, carry);

  STORE(h, h);
}

extern inline void fe_carry(fe h)
{
  DECL(h)
  LOAD(h, h);
  int32_t carry;

  CARRY_SIGNED_ALL(h, carry)

  STORE(h, h)
}

void fe_tobytes(uint8_t *s, const fe h)
{
  DECL(h);
  LOAD(h, h);

  int32_t carry;

  CARRY_SIGNED_ALL(h, carry)
  CARRY_SIGNED(h15, h0, carry);
  h8 += carry;

#define CARRY(a, b, c)                                                         \
  {                                                                            \
    c = a >> 28;                                                               \
    b += c;                                                                    \
    a -= c << 28;                                                              \
  }

/* -2^225 < x < 2^225 */

#define CARRY_ALL(h, c)                                                        \
  {                                                                            \
    CARRY(h##15, h##0, c);                                                     \
    h##8 += c;                                                                 \
    CARRY(h##0, h##1, c);                                                      \
    CARRY(h##1, h##2, c);                                                      \
    CARRY(h##2, h##3, c);                                                      \
    CARRY(h##3, h##4, c);                                                      \
    CARRY(h##4, h##5, c);                                                      \
    CARRY(h##5, h##6, c);                                                      \
    CARRY(h##6, h##7, c);                                                      \
    CARRY(h##7, h##8, c);                                                      \
    CARRY(h##8, h##9, c);                                                      \
    CARRY(h##9, h##10, c);                                                     \
    CARRY(h##10, h##11, c);                                                    \
    CARRY(h##11, h##12, c);                                                    \
    CARRY(h##12, h##13, c);                                                    \
    CARRY(h##13, h##14, c);                                                    \
    CARRY(h##14, h##15, c);                                                    \
  }

  CARRY_ALL(h, carry);

  carry = h15 >> 28;
  h0 += carry;
  h8 += carry;
  h15 -= carry << 28;

  CARRY_ALL(h, carry);

#undef CARRY
#undef CARRY_ALL

#define STORE8(v, a, b)                                                        \
  {                                                                            \
    (v)[0] = (uint32_t)a;                                                      \
    (v)[1] = (uint32_t)a >> 8;                                                 \
    (v)[2] = (uint32_t)a >> 16;                                                \
    (v)[3] = (uint32_t)(a >> 24) | (uint32_t)(b << 4);                         \
    (v)[4] = (uint32_t)b >> 4;                                                 \
    (v)[5] = (uint32_t)b >> 12;                                                \
    (v)[6] = (uint32_t)b >> 20;                                                \
  }

  STORE8(s, h0, h1);
  STORE8(s + 7, h2, h3);
  STORE8(s + 14, h4, h5);
  STORE8(s + 21, h6, h7);
  STORE8(s + 28, h8, h9);
  STORE8(s + 35, h10, h11);
  STORE8(s + 42, h12, h13);
  STORE8(s + 49, h14, h15);

#undef STORE8
}

/*
h = f
*/

void fe_copy(fe h, const fe f)
{
  DECL(f);
  LOAD(f, f);
  STORE(f, h);
}

/*
return 1 if f == 0
return 0 if f != 0

Preconditions:
   |f| bounded by 1.1*2^26,1.1*2^25,1.1*2^26,1.1*2^25,etc.
*/

int fe_isnonzero(const fe f)
{
  uint8_t s[56];
  fe_tobytes(s, f);
  uint8_t r = 0;
  for (int i = 0; i < 56; i++)
    r |= s[i];
  return r != 0;
}

int fe_isnegative(const fe f)
{
  uint8_t s[56];
  fe_tobytes(s, f);
  return s[0] & 1;
}

/*
h = 0
*/

void fe_0(fe h)
{
  h[0] = 0;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
  h[5] = 0;
  h[6] = 0;
  h[7] = 0;
  h[8] = 0;
  h[9] = 0;
  h[10] = 0;
  h[11] = 0;
  h[12] = 0;
  h[13] = 0;
  h[14] = 0;
  h[15] = 0;
}

/*
h = 1
*/

void fe_1(fe h)
{
  h[0] = 1;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
  h[5] = 0;
  h[6] = 0;
  h[7] = 0;
  h[8] = 0;
  h[9] = 0;
  h[10] = 0;
  h[11] = 0;
  h[12] = 0;
  h[13] = 0;
  h[14] = 0;
  h[15] = 0;
}

/*
Replace (f,g) with (g,f) if b == 1;
replace (f,g) with (f,g) if b == 0.

Preconditions: b in {0,1}.
*/

void fe_cswap(fe f, fe g, unsigned int b)
{
  DECL(f);
  DECL(g);
  DECL(x);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(x, f, g, ^);
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  x5 &= b;
  x6 &= b;
  x7 &= b;
  x8 &= b;
  x9 &= b;
  x10 &= b;
  x11 &= b;
  x12 &= b;
  x13 &= b;
  x14 &= b;
  x15 &= b;
  OPERATE(f, f, x, ^);
  OPERATE(g, g, x, ^);
  STORE(f, f);
  STORE(g, g);
}

/*
Replace (f,g) with (g,g) if b == 1;
replace (f,g) with (f,g) if b == 0.

Preconditions: b in {0,1}.
*/

void fe_cmov(fe f, const fe g, unsigned int b)
{
  DECL(f);
  DECL(g);
  DECL(x);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(x, f, g, ^);
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  x5 &= b;
  x6 &= b;
  x7 &= b;
  x8 &= b;
  x9 &= b;
  x10 &= b;
  x11 &= b;
  x12 &= b;
  x13 &= b;
  x14 &= b;
  x15 &= b;
  OPERATE(f, f, x, ^);
  STORE(f, f);
}

void fe_add(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  DECL(h);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(h, f, g, +)
  STORE(h, h);
}

void fe_sub(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  DECL(h);
  LOAD(f, f);
  LOAD(g, g);
  OPERATE(h, f, g, -)
  STORE(h, h);
}

void fe_mul(fe h, const fe f, const fe g)
{
  DECL(f);
  DECL(g);
  DECL64(h);
  LOAD(f, f);
  LOAD(g, g);
  int64_t carry;
  int64_t f0g0 = (int64_t)f0 * (int64_t)g0;
  int64_t f0g1 = (int64_t)f0 * (int64_t)g1;
  int64_t f0g2 = (int64_t)f0 * (int64_t)g2;
  int64_t f0g3 = (int64_t)f0 * (int64_t)g3;
  int64_t f0g4 = (int64_t)f0 * (int64_t)g4;
  int64_t f0g5 = (int64_t)f0 * (int64_t)g5;
  int64_t f0g6 = (int64_t)f0 * (int64_t)g6;
  int64_t f0g7 = (int64_t)f0 * (int64_t)g7;
  int64_t f0g8 = (int64_t)f0 * (int64_t)g8;
  int64_t f0g9 = (int64_t)f0 * (int64_t)g9;
  int64_t f0g10 = (int64_t)f0 * (int64_t)g10;
  int64_t f0g11 = (int64_t)f0 * (int64_t)g11;
  int64_t f0g12 = (int64_t)f0 * (int64_t)g12;
  int64_t f0g13 = (int64_t)f0 * (int64_t)g13;
  int64_t f0g14 = (int64_t)f0 * (int64_t)g14;
  int64_t f0g15 = (int64_t)f0 * (int64_t)g15;
  int64_t f1g0 = (int64_t)f1 * (int64_t)g0;
  int64_t f1g1 = (int64_t)f1 * (int64_t)g1;
  int64_t f1g2 = (int64_t)f1 * (int64_t)g2;
  int64_t f1g3 = (int64_t)f1 * (int64_t)g3;
  int64_t f1g4 = (int64_t)f1 * (int64_t)g4;
  int64_t f1g5 = (int64_t)f1 * (int64_t)g5;
  int64_t f1g6 = (int64_t)f1 * (int64_t)g6;
  int64_t f1g7 = (int64_t)f1 * (int64_t)g7;
  int64_t f1g8 = (int64_t)f1 * (int64_t)g8;
  int64_t f1g9 = (int64_t)f1 * (int64_t)g9;
  int64_t f1g10 = (int64_t)f1 * (int64_t)g10;
  int64_t f1g11 = (int64_t)f1 * (int64_t)g11;
  int64_t f1g12 = (int64_t)f1 * (int64_t)g12;
  int64_t f1g13 = (int64_t)f1 * (int64_t)g13;
  int64_t f1g14 = (int64_t)f1 * (int64_t)g14;
  int64_t f1g15 = (int64_t)f1 * (int64_t)g15;
  int64_t f2g0 = (int64_t)f2 * (int64_t)g0;
  int64_t f2g1 = (int64_t)f2 * (int64_t)g1;
  int64_t f2g2 = (int64_t)f2 * (int64_t)g2;
  int64_t f2g3 = (int64_t)f2 * (int64_t)g3;
  int64_t f2g4 = (int64_t)f2 * (int64_t)g4;
  int64_t f2g5 = (int64_t)f2 * (int64_t)g5;
  int64_t f2g6 = (int64_t)f2 * (int64_t)g6;
  int64_t f2g7 = (int64_t)f2 * (int64_t)g7;
  int64_t f2g8 = (int64_t)f2 * (int64_t)g8;
  int64_t f2g9 = (int64_t)f2 * (int64_t)g9;
  int64_t f2g10 = (int64_t)f2 * (int64_t)g10;
  int64_t f2g11 = (int64_t)f2 * (int64_t)g11;
  int64_t f2g12 = (int64_t)f2 * (int64_t)g12;
  int64_t f2g13 = (int64_t)f2 * (int64_t)g13;
  int64_t f2g14 = (int64_t)f2 * (int64_t)g14;
  int64_t f2g15 = (int64_t)f2 * (int64_t)g15;
  int64_t f3g0 = (int64_t)f3 * (int64_t)g0;
  int64_t f3g1 = (int64_t)f3 * (int64_t)g1;
  int64_t f3g2 = (int64_t)f3 * (int64_t)g2;
  int64_t f3g3 = (int64_t)f3 * (int64_t)g3;
  int64_t f3g4 = (int64_t)f3 * (int64_t)g4;
  int64_t f3g5 = (int64_t)f3 * (int64_t)g5;
  int64_t f3g6 = (int64_t)f3 * (int64_t)g6;
  int64_t f3g7 = (int64_t)f3 * (int64_t)g7;
  int64_t f3g8 = (int64_t)f3 * (int64_t)g8;
  int64_t f3g9 = (int64_t)f3 * (int64_t)g9;
  int64_t f3g10 = (int64_t)f3 * (int64_t)g10;
  int64_t f3g11 = (int64_t)f3 * (int64_t)g11;
  int64_t f3g12 = (int64_t)f3 * (int64_t)g12;
  int64_t f3g13 = (int64_t)f3 * (int64_t)g13;
  int64_t f3g14 = (int64_t)f3 * (int64_t)g14;
  int64_t f3g15 = (int64_t)f3 * (int64_t)g15;
  int64_t f4g0 = (int64_t)f4 * (int64_t)g0;
  int64_t f4g1 = (int64_t)f4 * (int64_t)g1;
  int64_t f4g2 = (int64_t)f4 * (int64_t)g2;
  int64_t f4g3 = (int64_t)f4 * (int64_t)g3;
  int64_t f4g4 = (int64_t)f4 * (int64_t)g4;
  int64_t f4g5 = (int64_t)f4 * (int64_t)g5;
  int64_t f4g6 = (int64_t)f4 * (int64_t)g6;
  int64_t f4g7 = (int64_t)f4 * (int64_t)g7;
  int64_t f4g8 = (int64_t)f4 * (int64_t)g8;
  int64_t f4g9 = (int64_t)f4 * (int64_t)g9;
  int64_t f4g10 = (int64_t)f4 * (int64_t)g10;
  int64_t f4g11 = (int64_t)f4 * (int64_t)g11;
  int64_t f4g12 = (int64_t)f4 * (int64_t)g12;
  int64_t f4g13 = (int64_t)f4 * (int64_t)g13;
  int64_t f4g14 = (int64_t)f4 * (int64_t)g14;
  int64_t f4g15 = (int64_t)f4 * (int64_t)g15;
  int64_t f5g0 = (int64_t)f5 * (int64_t)g0;
  int64_t f5g1 = (int64_t)f5 * (int64_t)g1;
  int64_t f5g2 = (int64_t)f5 * (int64_t)g2;
  int64_t f5g3 = (int64_t)f5 * (int64_t)g3;
  int64_t f5g4 = (int64_t)f5 * (int64_t)g4;
  int64_t f5g5 = (int64_t)f5 * (int64_t)g5;
  int64_t f5g6 = (int64_t)f5 * (int64_t)g6;
  int64_t f5g7 = (int64_t)f5 * (int64_t)g7;
  int64_t f5g8 = (int64_t)f5 * (int64_t)g8;
  int64_t f5g9 = (int64_t)f5 * (int64_t)g9;
  int64_t f5g10 = (int64_t)f5 * (int64_t)g10;
  int64_t f5g11 = (int64_t)f5 * (int64_t)g11;
  int64_t f5g12 = (int64_t)f5 * (int64_t)g12;
  int64_t f5g13 = (int64_t)f5 * (int64_t)g13;
  int64_t f5g14 = (int64_t)f5 * (int64_t)g14;
  int64_t f5g15 = (int64_t)f5 * (int64_t)g15;
  int64_t f6g0 = (int64_t)f6 * (int64_t)g0;
  int64_t f6g1 = (int64_t)f6 * (int64_t)g1;
  int64_t f6g2 = (int64_t)f6 * (int64_t)g2;
  int64_t f6g3 = (int64_t)f6 * (int64_t)g3;
  int64_t f6g4 = (int64_t)f6 * (int64_t)g4;
  int64_t f6g5 = (int64_t)f6 * (int64_t)g5;
  int64_t f6g6 = (int64_t)f6 * (int64_t)g6;
  int64_t f6g7 = (int64_t)f6 * (int64_t)g7;
  int64_t f6g8 = (int64_t)f6 * (int64_t)g8;
  int64_t f6g9 = (int64_t)f6 * (int64_t)g9;
  int64_t f6g10 = (int64_t)f6 * (int64_t)g10;
  int64_t f6g11 = (int64_t)f6 * (int64_t)g11;
  int64_t f6g12 = (int64_t)f6 * (int64_t)g12;
  int64_t f6g13 = (int64_t)f6 * (int64_t)g13;
  int64_t f6g14 = (int64_t)f6 * (int64_t)g14;
  int64_t f6g15 = (int64_t)f6 * (int64_t)g15;
  int64_t f7g0 = (int64_t)f7 * (int64_t)g0;
  int64_t f7g1 = (int64_t)f7 * (int64_t)g1;
  int64_t f7g2 = (int64_t)f7 * (int64_t)g2;
  int64_t f7g3 = (int64_t)f7 * (int64_t)g3;
  int64_t f7g4 = (int64_t)f7 * (int64_t)g4;
  int64_t f7g5 = (int64_t)f7 * (int64_t)g5;
  int64_t f7g6 = (int64_t)f7 * (int64_t)g6;
  int64_t f7g7 = (int64_t)f7 * (int64_t)g7;
  int64_t f7g8 = (int64_t)f7 * (int64_t)g8;
  int64_t f7g9 = (int64_t)f7 * (int64_t)g9;
  int64_t f7g10 = (int64_t)f7 * (int64_t)g10;
  int64_t f7g11 = (int64_t)f7 * (int64_t)g11;
  int64_t f7g12 = (int64_t)f7 * (int64_t)g12;
  int64_t f7g13 = (int64_t)f7 * (int64_t)g13;
  int64_t f7g14 = (int64_t)f7 * (int64_t)g14;
  int64_t f7g15 = (int64_t)f7 * (int64_t)g15;
  int64_t f8g0 = (int64_t)f8 * (int64_t)g0;
  int64_t f8g1 = (int64_t)f8 * (int64_t)g1;
  int64_t f8g2 = (int64_t)f8 * (int64_t)g2;
  int64_t f8g3 = (int64_t)f8 * (int64_t)g3;
  int64_t f8g4 = (int64_t)f8 * (int64_t)g4;
  int64_t f8g5 = (int64_t)f8 * (int64_t)g5;
  int64_t f8g6 = (int64_t)f8 * (int64_t)g6;
  int64_t f8g7 = (int64_t)f8 * (int64_t)g7;
  int64_t f8g8 = (int64_t)f8 * (int64_t)g8;
  int64_t f8g9 = (int64_t)f8 * (int64_t)g9;
  int64_t f8g10 = (int64_t)f8 * (int64_t)g10;
  int64_t f8g11 = (int64_t)f8 * (int64_t)g11;
  int64_t f8g12 = (int64_t)f8 * (int64_t)g12;
  int64_t f8g13 = (int64_t)f8 * (int64_t)g13;
  int64_t f8g14 = (int64_t)f8 * (int64_t)g14;
  int64_t f8g15 = (int64_t)f8 * (int64_t)g15;
  int64_t f9g0 = (int64_t)f9 * (int64_t)g0;
  int64_t f9g1 = (int64_t)f9 * (int64_t)g1;
  int64_t f9g2 = (int64_t)f9 * (int64_t)g2;
  int64_t f9g3 = (int64_t)f9 * (int64_t)g3;
  int64_t f9g4 = (int64_t)f9 * (int64_t)g4;
  int64_t f9g5 = (int64_t)f9 * (int64_t)g5;
  int64_t f9g6 = (int64_t)f9 * (int64_t)g6;
  int64_t f9g7 = (int64_t)f9 * (int64_t)g7;
  int64_t f9g8 = (int64_t)f9 * (int64_t)g8;
  int64_t f9g9 = (int64_t)f9 * (int64_t)g9;
  int64_t f9g10 = (int64_t)f9 * (int64_t)g10;
  int64_t f9g11 = (int64_t)f9 * (int64_t)g11;
  int64_t f9g12 = (int64_t)f9 * (int64_t)g12;
  int64_t f9g13 = (int64_t)f9 * (int64_t)g13;
  int64_t f9g14 = (int64_t)f9 * (int64_t)g14;
  int64_t f9g15 = (int64_t)f9 * (int64_t)g15;
  int64_t f10g0 = (int64_t)f10 * (int64_t)g0;
  int64_t f10g1 = (int64_t)f10 * (int64_t)g1;
  int64_t f10g2 = (int64_t)f10 * (int64_t)g2;
  int64_t f10g3 = (int64_t)f10 * (int64_t)g3;
  int64_t f10g4 = (int64_t)f10 * (int64_t)g4;
  int64_t f10g5 = (int64_t)f10 * (int64_t)g5;
  int64_t f10g6 = (int64_t)f10 * (int64_t)g6;
  int64_t f10g7 = (int64_t)f10 * (int64_t)g7;
  int64_t f10g8 = (int64_t)f10 * (int64_t)g8;
  int64_t f10g9 = (int64_t)f10 * (int64_t)g9;
  int64_t f10g10 = (int64_t)f10 * (int64_t)g10;
  int64_t f10g11 = (int64_t)f10 * (int64_t)g11;
  int64_t f10g12 = (int64_t)f10 * (int64_t)g12;
  int64_t f10g13 = (int64_t)f10 * (int64_t)g13;
  int64_t f10g14 = (int64_t)f10 * (int64_t)g14;
  int64_t f10g15 = (int64_t)f10 * (int64_t)g15;
  int64_t f11g0 = (int64_t)f11 * (int64_t)g0;
  int64_t f11g1 = (int64_t)f11 * (int64_t)g1;
  int64_t f11g2 = (int64_t)f11 * (int64_t)g2;
  int64_t f11g3 = (int64_t)f11 * (int64_t)g3;
  int64_t f11g4 = (int64_t)f11 * (int64_t)g4;
  int64_t f11g5 = (int64_t)f11 * (int64_t)g5;
  int64_t f11g6 = (int64_t)f11 * (int64_t)g6;
  int64_t f11g7 = (int64_t)f11 * (int64_t)g7;
  int64_t f11g8 = (int64_t)f11 * (int64_t)g8;
  int64_t f11g9 = (int64_t)f11 * (int64_t)g9;
  int64_t f11g10 = (int64_t)f11 * (int64_t)g10;
  int64_t f11g11 = (int64_t)f11 * (int64_t)g11;
  int64_t f11g12 = (int64_t)f11 * (int64_t)g12;
  int64_t f11g13 = (int64_t)f11 * (int64_t)g13;
  int64_t f11g14 = (int64_t)f11 * (int64_t)g14;
  int64_t f11g15 = (int64_t)f11 * (int64_t)g15;
  int64_t f12g0 = (int64_t)f12 * (int64_t)g0;
  int64_t f12g1 = (int64_t)f12 * (int64_t)g1;
  int64_t f12g2 = (int64_t)f12 * (int64_t)g2;
  int64_t f12g3 = (int64_t)f12 * (int64_t)g3;
  int64_t f12g4 = (int64_t)f12 * (int64_t)g4;
  int64_t f12g5 = (int64_t)f12 * (int64_t)g5;
  int64_t f12g6 = (int64_t)f12 * (int64_t)g6;
  int64_t f12g7 = (int64_t)f12 * (int64_t)g7;
  int64_t f12g8 = (int64_t)f12 * (int64_t)g8;
  int64_t f12g9 = (int64_t)f12 * (int64_t)g9;
  int64_t f12g10 = (int64_t)f12 * (int64_t)g10;
  int64_t f12g11 = (int64_t)f12 * (int64_t)g11;
  int64_t f12g12 = (int64_t)f12 * (int64_t)g12;
  int64_t f12g13 = (int64_t)f12 * (int64_t)g13;
  int64_t f12g14 = (int64_t)f12 * (int64_t)g14;
  int64_t f12g15 = (int64_t)f12 * (int64_t)g15;
  int64_t f13g0 = (int64_t)f13 * (int64_t)g0;
  int64_t f13g1 = (int64_t)f13 * (int64_t)g1;
  int64_t f13g2 = (int64_t)f13 * (int64_t)g2;
  int64_t f13g3 = (int64_t)f13 * (int64_t)g3;
  int64_t f13g4 = (int64_t)f13 * (int64_t)g4;
  int64_t f13g5 = (int64_t)f13 * (int64_t)g5;
  int64_t f13g6 = (int64_t)f13 * (int64_t)g6;
  int64_t f13g7 = (int64_t)f13 * (int64_t)g7;
  int64_t f13g8 = (int64_t)f13 * (int64_t)g8;
  int64_t f13g9 = (int64_t)f13 * (int64_t)g9;
  int64_t f13g10 = (int64_t)f13 * (int64_t)g10;
  int64_t f13g11 = (int64_t)f13 * (int64_t)g11;
  int64_t f13g12 = (int64_t)f13 * (int64_t)g12;
  int64_t f13g13 = (int64_t)f13 * (int64_t)g13;
  int64_t f13g14 = (int64_t)f13 * (int64_t)g14;
  int64_t f13g15 = (int64_t)f13 * (int64_t)g15;
  int64_t f14g0 = (int64_t)f14 * (int64_t)g0;
  int64_t f14g1 = (int64_t)f14 * (int64_t)g1;
  int64_t f14g2 = (int64_t)f14 * (int64_t)g2;
  int64_t f14g3 = (int64_t)f14 * (int64_t)g3;
  int64_t f14g4 = (int64_t)f14 * (int64_t)g4;
  int64_t f14g5 = (int64_t)f14 * (int64_t)g5;
  int64_t f14g6 = (int64_t)f14 * (int64_t)g6;
  int64_t f14g7 = (int64_t)f14 * (int64_t)g7;
  int64_t f14g8 = (int64_t)f14 * (int64_t)g8;
  int64_t f14g9 = (int64_t)f14 * (int64_t)g9;
  int64_t f14g10 = (int64_t)f14 * (int64_t)g10;
  int64_t f14g11 = (int64_t)f14 * (int64_t)g11;
  int64_t f14g12 = (int64_t)f14 * (int64_t)g12;
  int64_t f14g13 = (int64_t)f14 * (int64_t)g13;
  int64_t f14g14 = (int64_t)f14 * (int64_t)g14;
  int64_t f14g15 = (int64_t)f14 * (int64_t)g15;
  int64_t f15g0 = (int64_t)f15 * (int64_t)g0;
  int64_t f15g1 = (int64_t)f15 * (int64_t)g1;
  int64_t f15g2 = (int64_t)f15 * (int64_t)g2;
  int64_t f15g3 = (int64_t)f15 * (int64_t)g3;
  int64_t f15g4 = (int64_t)f15 * (int64_t)g4;
  int64_t f15g5 = (int64_t)f15 * (int64_t)g5;
  int64_t f15g6 = (int64_t)f15 * (int64_t)g6;
  int64_t f15g7 = (int64_t)f15 * (int64_t)g7;
  int64_t f15g8 = (int64_t)f15 * (int64_t)g8;
  int64_t f15g9 = (int64_t)f15 * (int64_t)g9;
  int64_t f15g10 = (int64_t)f15 * (int64_t)g10;
  int64_t f15g11 = (int64_t)f15 * (int64_t)g11;
  int64_t f15g12 = (int64_t)f15 * (int64_t)g12;
  int64_t f15g13 = (int64_t)f15 * (int64_t)g13;
  int64_t f15g14 = (int64_t)f15 * (int64_t)g14;
  int64_t f15g15 = (int64_t)f15 * (int64_t)g15;
  h0 = f0g0 + f1g15 + f2g14 + f3g13 + f4g12 + f5g11 + f6g10 + f7g9 + f8g8 +
       f9g7 + f9g15 + f10g6 + f10g14 + f11g5 + f11g13 + f12g4 + f12g12 + f13g3 +
       f13g11 + f14g2 + f14g10 + f15g1 + f15g9;
  h1 = f0g1 + f1g0 + f2g15 + f3g14 + f4g13 + f5g12 + f6g11 + f7g10 + f8g9 +
       f9g8 + f10g7 + f10g15 + f11g6 + f11g14 + f12g5 + f12g13 + f13g4 +
       f13g12 + f14g3 + f14g11 + f15g2 + f15g10;
  h2 = f0g2 + f1g1 + f2g0 + f3g15 + f4g14 + f5g13 + f6g12 + f7g11 + f8g10 +
       f9g9 + f10g8 + f11g7 + f11g15 + f12g6 + f12g14 + f13g5 + f13g13 + f14g4 +
       f14g12 + f15g3 + f15g11;
  h3 = f0g3 + f1g2 + f2g1 + f3g0 + f4g15 + f5g14 + f6g13 + f7g12 + f8g11 +
       f9g10 + f10g9 + f11g8 + f12g7 + f12g15 + f13g6 + f13g14 + f14g5 +
       f14g13 + f15g4 + f15g12;
  h4 = f0g4 + f1g3 + f2g2 + f3g1 + f4g0 + f5g15 + f6g14 + f7g13 + f8g12 +
       f9g11 + f10g10 + f11g9 + f12g8 + f13g7 + f13g15 + f14g6 + f14g14 +
       f15g5 + f15g13;
  h5 = f0g5 + f1g4 + f2g3 + f3g2 + f4g1 + f5g0 + f6g15 + f7g14 + f8g13 + f9g12 +
       f10g11 + f11g10 + f12g9 + f13g8 + f14g7 + f14g15 + f15g6 + f15g14;
  h6 = f0g6 + f1g5 + f2g4 + f3g3 + f4g2 + f5g1 + f6g0 + f7g15 + f8g14 + f9g13 +
       f10g12 + f11g11 + f12g10 + f13g9 + f14g8 + f15g7 + f15g15;
  h7 = f0g7 + f1g6 + f2g5 + f3g4 + f4g3 + f5g2 + f6g1 + f7g0 + f8g15 + f9g14 +
       f10g13 + f11g12 + f12g11 + f13g10 + f14g9 + f15g8;
  h8 = f0g8 + f1g7 + f1g15 + f2g6 + f2g14 + f3g5 + f3g13 + f4g4 + f4g12 + f5g3 +
       f5g11 + f6g2 + f6g10 + f7g1 + f7g9 + f8g0 + f8g8 + f9g7 + f9g15 + f9g15 +
       f10g6 + f10g14 + f10g14 + f11g5 + f11g13 + f11g13 + f12g4 + f12g12 +
       f12g12 + f13g3 + f13g11 + f13g11 + f14g2 + f14g10 + f14g10 + f15g1 +
       f15g9 + f15g9;
  h9 = f0g9 + f1g8 + f2g7 + f2g15 + f3g6 + f3g14 + f4g5 + f4g13 + f5g4 + f5g12 +
       f6g3 + f6g11 + f7g2 + f7g10 + f8g1 + f8g9 + f9g0 + f9g8 + f10g7 +
       f10g15 + f10g15 + f11g6 + f11g14 + f11g14 + f12g5 + f12g13 + f12g13 +
       f13g4 + f13g12 + f13g12 + f14g3 + f14g11 + f14g11 + f15g2 + f15g10 +
       f15g10;
  h10 = f0g10 + f1g9 + f2g8 + f3g7 + f3g15 + f4g6 + f4g14 + f5g5 + f5g13 +
        f6g4 + f6g12 + f7g3 + f7g11 + f8g2 + f8g10 + f9g1 + f9g9 + f10g0 +
        f10g8 + f11g7 + f11g15 + f11g15 + f12g6 + f12g14 + f12g14 + f13g5 +
        f13g13 + f13g13 + f14g4 + f14g12 + f14g12 + f15g3 + f15g11 + f15g11;
  h11 = f0g11 + f1g10 + f2g9 + f3g8 + f4g7 + f4g15 + f5g6 + f5g14 + f6g5 +
        f6g13 + f7g4 + f7g12 + f8g3 + f8g11 + f9g2 + f9g10 + f10g1 + f10g9 +
        f11g0 + f11g8 + f12g7 + f12g15 + f12g15 + f13g6 + f13g14 + f13g14 +
        f14g5 + f14g13 + f14g13 + f15g4 + f15g12 + f15g12;
  h12 = f0g12 + f1g11 + f2g10 + f3g9 + f4g8 + f5g7 + f5g15 + f6g6 + f6g14 +
        f7g5 + f7g13 + f8g4 + f8g12 + f9g3 + f9g11 + f10g2 + f10g10 + f11g1 +
        f11g9 + f12g0 + f12g8 + f13g7 + f13g15 + f13g15 + f14g6 + f14g14 +
        f14g14 + f15g5 + f15g13 + f15g13;
  h13 = f0g13 + f1g12 + f2g11 + f3g10 + f4g9 + f5g8 + f6g7 + f6g15 + f7g6 +
        f7g14 + f8g5 + f8g13 + f9g4 + f9g12 + f10g3 + f10g11 + f11g2 + f11g10 +
        f12g1 + f12g9 + f13g0 + f13g8 + f14g7 + f14g15 + f14g15 + f15g6 +
        f15g14 + f15g14;
  h14 = f0g14 + f1g13 + f2g12 + f3g11 + f4g10 + f5g9 + f6g8 + f7g7 + f7g15 +
        f8g6 + f8g14 + f9g5 + f9g13 + f10g4 + f10g12 + f11g3 + f11g11 + f12g2 +
        f12g10 + f13g1 + f13g9 + f14g0 + f14g8 + f15g7 + f15g15 + f15g15;
  h15 = f0g15 + f1g14 + f2g13 + f3g12 + f4g11 + f5g10 + f6g9 + f7g8 + f8g7 +
        f8g15 + f9g6 + f9g14 + f10g5 + f10g13 + f11g4 + f11g12 + f12g3 +
        f12g11 + f13g2 + f13g10 + f14g1 + f14g9 + f15g0 + f15g8;

  CARRY_SIGNED64(h0, h1, carry);
  CARRY_SIGNED64(h7, h8, carry);

  CARRY_SIGNED64(h1, h2, carry);
  CARRY_SIGNED64(h8, h9, carry);

  CARRY_SIGNED64(h2, h3, carry);
  CARRY_SIGNED64(h9, h10, carry);

  CARRY_SIGNED64(h3, h4, carry);
  CARRY_SIGNED64(h10, h11, carry);

  CARRY_SIGNED64(h4, h5, carry);
  CARRY_SIGNED64(h11, h12, carry);

  CARRY_SIGNED64(h5, h6, carry);
  CARRY_SIGNED64(h12, h13, carry);

  CARRY_SIGNED64(h6, h7, carry);
  CARRY_SIGNED64(h13, h14, carry);

  CARRY_SIGNED64(h7, h8, carry);
  CARRY_SIGNED64(h14, h15, carry);

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;

  CARRY_SIGNED64(h0, h1, carry);
  CARRY_SIGNED64(h8, h9, carry);

  STORE(h, h);
}

void fe_sq(fe h, const fe f)
{
  DECL(f);
  DECL64(h);
  LOAD(f, f);
  int64_t carry;
  int64_t f0f0 = (int64_t)f0 * (int64_t)f0;
  int64_t f0f1 = (int64_t)f0 * (int64_t)f1;
  int64_t f0f2 = (int64_t)f0 * (int64_t)f2;
  int64_t f0f3 = (int64_t)f0 * (int64_t)f3;
  int64_t f0f4 = (int64_t)f0 * (int64_t)f4;
  int64_t f0f5 = (int64_t)f0 * (int64_t)f5;
  int64_t f0f6 = (int64_t)f0 * (int64_t)f6;
  int64_t f0f7 = (int64_t)f0 * (int64_t)f7;
  int64_t f0f8 = (int64_t)f0 * (int64_t)f8;
  int64_t f0f9 = (int64_t)f0 * (int64_t)f9;
  int64_t f0f10 = (int64_t)f0 * (int64_t)f10;
  int64_t f0f11 = (int64_t)f0 * (int64_t)f11;
  int64_t f0f12 = (int64_t)f0 * (int64_t)f12;
  int64_t f0f13 = (int64_t)f0 * (int64_t)f13;
  int64_t f0f14 = (int64_t)f0 * (int64_t)f14;
  int64_t f0f15 = (int64_t)f0 * (int64_t)f15;
  int64_t f1f0 = (int64_t)f1 * (int64_t)f0;
  int64_t f1f1 = (int64_t)f1 * (int64_t)f1;
  int64_t f1f2 = (int64_t)f1 * (int64_t)f2;
  int64_t f1f3 = (int64_t)f1 * (int64_t)f3;
  int64_t f1f4 = (int64_t)f1 * (int64_t)f4;
  int64_t f1f5 = (int64_t)f1 * (int64_t)f5;
  int64_t f1f6 = (int64_t)f1 * (int64_t)f6;
  int64_t f1f7 = (int64_t)f1 * (int64_t)f7;
  int64_t f1f8 = (int64_t)f1 * (int64_t)f8;
  int64_t f1f9 = (int64_t)f1 * (int64_t)f9;
  int64_t f1f10 = (int64_t)f1 * (int64_t)f10;
  int64_t f1f11 = (int64_t)f1 * (int64_t)f11;
  int64_t f1f12 = (int64_t)f1 * (int64_t)f12;
  int64_t f1f13 = (int64_t)f1 * (int64_t)f13;
  int64_t f1f14 = (int64_t)f1 * (int64_t)f14;
  int64_t f1f15 = (int64_t)f1 * (int64_t)f15;
  int64_t f2f0 = (int64_t)f2 * (int64_t)f0;
  int64_t f2f1 = (int64_t)f2 * (int64_t)f1;
  int64_t f2f2 = (int64_t)f2 * (int64_t)f2;
  int64_t f2f3 = (int64_t)f2 * (int64_t)f3;
  int64_t f2f4 = (int64_t)f2 * (int64_t)f4;
  int64_t f2f5 = (int64_t)f2 * (int64_t)f5;
  int64_t f2f6 = (int64_t)f2 * (int64_t)f6;
  int64_t f2f7 = (int64_t)f2 * (int64_t)f7;
  int64_t f2f8 = (int64_t)f2 * (int64_t)f8;
  int64_t f2f9 = (int64_t)f2 * (int64_t)f9;
  int64_t f2f10 = (int64_t)f2 * (int64_t)f10;
  int64_t f2f11 = (int64_t)f2 * (int64_t)f11;
  int64_t f2f12 = (int64_t)f2 * (int64_t)f12;
  int64_t f2f13 = (int64_t)f2 * (int64_t)f13;
  int64_t f2f14 = (int64_t)f2 * (int64_t)f14;
  int64_t f2f15 = (int64_t)f2 * (int64_t)f15;
  int64_t f3f0 = (int64_t)f3 * (int64_t)f0;
  int64_t f3f1 = (int64_t)f3 * (int64_t)f1;
  int64_t f3f2 = (int64_t)f3 * (int64_t)f2;
  int64_t f3f3 = (int64_t)f3 * (int64_t)f3;
  int64_t f3f4 = (int64_t)f3 * (int64_t)f4;
  int64_t f3f5 = (int64_t)f3 * (int64_t)f5;
  int64_t f3f6 = (int64_t)f3 * (int64_t)f6;
  int64_t f3f7 = (int64_t)f3 * (int64_t)f7;
  int64_t f3f8 = (int64_t)f3 * (int64_t)f8;
  int64_t f3f9 = (int64_t)f3 * (int64_t)f9;
  int64_t f3f10 = (int64_t)f3 * (int64_t)f10;
  int64_t f3f11 = (int64_t)f3 * (int64_t)f11;
  int64_t f3f12 = (int64_t)f3 * (int64_t)f12;
  int64_t f3f13 = (int64_t)f3 * (int64_t)f13;
  int64_t f3f14 = (int64_t)f3 * (int64_t)f14;
  int64_t f3f15 = (int64_t)f3 * (int64_t)f15;
  int64_t f4f0 = (int64_t)f4 * (int64_t)f0;
  int64_t f4f1 = (int64_t)f4 * (int64_t)f1;
  int64_t f4f2 = (int64_t)f4 * (int64_t)f2;
  int64_t f4f3 = (int64_t)f4 * (int64_t)f3;
  int64_t f4f4 = (int64_t)f4 * (int64_t)f4;
  int64_t f4f5 = (int64_t)f4 * (int64_t)f5;
  int64_t f4f6 = (int64_t)f4 * (int64_t)f6;
  int64_t f4f7 = (int64_t)f4 * (int64_t)f7;
  int64_t f4f8 = (int64_t)f4 * (int64_t)f8;
  int64_t f4f9 = (int64_t)f4 * (int64_t)f9;
  int64_t f4f10 = (int64_t)f4 * (int64_t)f10;
  int64_t f4f11 = (int64_t)f4 * (int64_t)f11;
  int64_t f4f12 = (int64_t)f4 * (int64_t)f12;
  int64_t f4f13 = (int64_t)f4 * (int64_t)f13;
  int64_t f4f14 = (int64_t)f4 * (int64_t)f14;
  int64_t f4f15 = (int64_t)f4 * (int64_t)f15;
  int64_t f5f0 = (int64_t)f5 * (int64_t)f0;
  int64_t f5f1 = (int64_t)f5 * (int64_t)f1;
  int64_t f5f2 = (int64_t)f5 * (int64_t)f2;
  int64_t f5f3 = (int64_t)f5 * (int64_t)f3;
  int64_t f5f4 = (int64_t)f5 * (int64_t)f4;
  int64_t f5f5 = (int64_t)f5 * (int64_t)f5;
  int64_t f5f6 = (int64_t)f5 * (int64_t)f6;
  int64_t f5f7 = (int64_t)f5 * (int64_t)f7;
  int64_t f5f8 = (int64_t)f5 * (int64_t)f8;
  int64_t f5f9 = (int64_t)f5 * (int64_t)f9;
  int64_t f5f10 = (int64_t)f5 * (int64_t)f10;
  int64_t f5f11 = (int64_t)f5 * (int64_t)f11;
  int64_t f5f12 = (int64_t)f5 * (int64_t)f12;
  int64_t f5f13 = (int64_t)f5 * (int64_t)f13;
  int64_t f5f14 = (int64_t)f5 * (int64_t)f14;
  int64_t f5f15 = (int64_t)f5 * (int64_t)f15;
  int64_t f6f0 = (int64_t)f6 * (int64_t)f0;
  int64_t f6f1 = (int64_t)f6 * (int64_t)f1;
  int64_t f6f2 = (int64_t)f6 * (int64_t)f2;
  int64_t f6f3 = (int64_t)f6 * (int64_t)f3;
  int64_t f6f4 = (int64_t)f6 * (int64_t)f4;
  int64_t f6f5 = (int64_t)f6 * (int64_t)f5;
  int64_t f6f6 = (int64_t)f6 * (int64_t)f6;
  int64_t f6f7 = (int64_t)f6 * (int64_t)f7;
  int64_t f6f8 = (int64_t)f6 * (int64_t)f8;
  int64_t f6f9 = (int64_t)f6 * (int64_t)f9;
  int64_t f6f10 = (int64_t)f6 * (int64_t)f10;
  int64_t f6f11 = (int64_t)f6 * (int64_t)f11;
  int64_t f6f12 = (int64_t)f6 * (int64_t)f12;
  int64_t f6f13 = (int64_t)f6 * (int64_t)f13;
  int64_t f6f14 = (int64_t)f6 * (int64_t)f14;
  int64_t f6f15 = (int64_t)f6 * (int64_t)f15;
  int64_t f7f0 = (int64_t)f7 * (int64_t)f0;
  int64_t f7f1 = (int64_t)f7 * (int64_t)f1;
  int64_t f7f2 = (int64_t)f7 * (int64_t)f2;
  int64_t f7f3 = (int64_t)f7 * (int64_t)f3;
  int64_t f7f4 = (int64_t)f7 * (int64_t)f4;
  int64_t f7f5 = (int64_t)f7 * (int64_t)f5;
  int64_t f7f6 = (int64_t)f7 * (int64_t)f6;
  int64_t f7f7 = (int64_t)f7 * (int64_t)f7;
  int64_t f7f8 = (int64_t)f7 * (int64_t)f8;
  int64_t f7f9 = (int64_t)f7 * (int64_t)f9;
  int64_t f7f10 = (int64_t)f7 * (int64_t)f10;
  int64_t f7f11 = (int64_t)f7 * (int64_t)f11;
  int64_t f7f12 = (int64_t)f7 * (int64_t)f12;
  int64_t f7f13 = (int64_t)f7 * (int64_t)f13;
  int64_t f7f14 = (int64_t)f7 * (int64_t)f14;
  int64_t f7f15 = (int64_t)f7 * (int64_t)f15;
  int64_t f8f0 = (int64_t)f8 * (int64_t)f0;
  int64_t f8f1 = (int64_t)f8 * (int64_t)f1;
  int64_t f8f2 = (int64_t)f8 * (int64_t)f2;
  int64_t f8f3 = (int64_t)f8 * (int64_t)f3;
  int64_t f8f4 = (int64_t)f8 * (int64_t)f4;
  int64_t f8f5 = (int64_t)f8 * (int64_t)f5;
  int64_t f8f6 = (int64_t)f8 * (int64_t)f6;
  int64_t f8f7 = (int64_t)f8 * (int64_t)f7;
  int64_t f8f8 = (int64_t)f8 * (int64_t)f8;
  int64_t f8f9 = (int64_t)f8 * (int64_t)f9;
  int64_t f8f10 = (int64_t)f8 * (int64_t)f10;
  int64_t f8f11 = (int64_t)f8 * (int64_t)f11;
  int64_t f8f12 = (int64_t)f8 * (int64_t)f12;
  int64_t f8f13 = (int64_t)f8 * (int64_t)f13;
  int64_t f8f14 = (int64_t)f8 * (int64_t)f14;
  int64_t f8f15 = (int64_t)f8 * (int64_t)f15;
  int64_t f9f0 = (int64_t)f9 * (int64_t)f0;
  int64_t f9f1 = (int64_t)f9 * (int64_t)f1;
  int64_t f9f2 = (int64_t)f9 * (int64_t)f2;
  int64_t f9f3 = (int64_t)f9 * (int64_t)f3;
  int64_t f9f4 = (int64_t)f9 * (int64_t)f4;
  int64_t f9f5 = (int64_t)f9 * (int64_t)f5;
  int64_t f9f6 = (int64_t)f9 * (int64_t)f6;
  int64_t f9f7 = (int64_t)f9 * (int64_t)f7;
  int64_t f9f8 = (int64_t)f9 * (int64_t)f8;
  int64_t f9f9 = (int64_t)f9 * (int64_t)f9;
  int64_t f9f10 = (int64_t)f9 * (int64_t)f10;
  int64_t f9f11 = (int64_t)f9 * (int64_t)f11;
  int64_t f9f12 = (int64_t)f9 * (int64_t)f12;
  int64_t f9f13 = (int64_t)f9 * (int64_t)f13;
  int64_t f9f14 = (int64_t)f9 * (int64_t)f14;
  int64_t f9f15 = (int64_t)f9 * (int64_t)f15;
  int64_t f10f0 = (int64_t)f10 * (int64_t)f0;
  int64_t f10f1 = (int64_t)f10 * (int64_t)f1;
  int64_t f10f2 = (int64_t)f10 * (int64_t)f2;
  int64_t f10f3 = (int64_t)f10 * (int64_t)f3;
  int64_t f10f4 = (int64_t)f10 * (int64_t)f4;
  int64_t f10f5 = (int64_t)f10 * (int64_t)f5;
  int64_t f10f6 = (int64_t)f10 * (int64_t)f6;
  int64_t f10f7 = (int64_t)f10 * (int64_t)f7;
  int64_t f10f8 = (int64_t)f10 * (int64_t)f8;
  int64_t f10f9 = (int64_t)f10 * (int64_t)f9;
  int64_t f10f10 = (int64_t)f10 * (int64_t)f10;
  int64_t f10f11 = (int64_t)f10 * (int64_t)f11;
  int64_t f10f12 = (int64_t)f10 * (int64_t)f12;
  int64_t f10f13 = (int64_t)f10 * (int64_t)f13;
  int64_t f10f14 = (int64_t)f10 * (int64_t)f14;
  int64_t f10f15 = (int64_t)f10 * (int64_t)f15;
  int64_t f11f0 = (int64_t)f11 * (int64_t)f0;
  int64_t f11f1 = (int64_t)f11 * (int64_t)f1;
  int64_t f11f2 = (int64_t)f11 * (int64_t)f2;
  int64_t f11f3 = (int64_t)f11 * (int64_t)f3;
  int64_t f11f4 = (int64_t)f11 * (int64_t)f4;
  int64_t f11f5 = (int64_t)f11 * (int64_t)f5;
  int64_t f11f6 = (int64_t)f11 * (int64_t)f6;
  int64_t f11f7 = (int64_t)f11 * (int64_t)f7;
  int64_t f11f8 = (int64_t)f11 * (int64_t)f8;
  int64_t f11f9 = (int64_t)f11 * (int64_t)f9;
  int64_t f11f10 = (int64_t)f11 * (int64_t)f10;
  int64_t f11f11 = (int64_t)f11 * (int64_t)f11;
  int64_t f11f12 = (int64_t)f11 * (int64_t)f12;
  int64_t f11f13 = (int64_t)f11 * (int64_t)f13;
  int64_t f11f14 = (int64_t)f11 * (int64_t)f14;
  int64_t f11f15 = (int64_t)f11 * (int64_t)f15;
  int64_t f12f0 = (int64_t)f12 * (int64_t)f0;
  int64_t f12f1 = (int64_t)f12 * (int64_t)f1;
  int64_t f12f2 = (int64_t)f12 * (int64_t)f2;
  int64_t f12f3 = (int64_t)f12 * (int64_t)f3;
  int64_t f12f4 = (int64_t)f12 * (int64_t)f4;
  int64_t f12f5 = (int64_t)f12 * (int64_t)f5;
  int64_t f12f6 = (int64_t)f12 * (int64_t)f6;
  int64_t f12f7 = (int64_t)f12 * (int64_t)f7;
  int64_t f12f8 = (int64_t)f12 * (int64_t)f8;
  int64_t f12f9 = (int64_t)f12 * (int64_t)f9;
  int64_t f12f10 = (int64_t)f12 * (int64_t)f10;
  int64_t f12f11 = (int64_t)f12 * (int64_t)f11;
  int64_t f12f12 = (int64_t)f12 * (int64_t)f12;
  int64_t f12f13 = (int64_t)f12 * (int64_t)f13;
  int64_t f12f14 = (int64_t)f12 * (int64_t)f14;
  int64_t f12f15 = (int64_t)f12 * (int64_t)f15;
  int64_t f13f0 = (int64_t)f13 * (int64_t)f0;
  int64_t f13f1 = (int64_t)f13 * (int64_t)f1;
  int64_t f13f2 = (int64_t)f13 * (int64_t)f2;
  int64_t f13f3 = (int64_t)f13 * (int64_t)f3;
  int64_t f13f4 = (int64_t)f13 * (int64_t)f4;
  int64_t f13f5 = (int64_t)f13 * (int64_t)f5;
  int64_t f13f6 = (int64_t)f13 * (int64_t)f6;
  int64_t f13f7 = (int64_t)f13 * (int64_t)f7;
  int64_t f13f8 = (int64_t)f13 * (int64_t)f8;
  int64_t f13f9 = (int64_t)f13 * (int64_t)f9;
  int64_t f13f10 = (int64_t)f13 * (int64_t)f10;
  int64_t f13f11 = (int64_t)f13 * (int64_t)f11;
  int64_t f13f12 = (int64_t)f13 * (int64_t)f12;
  int64_t f13f13 = (int64_t)f13 * (int64_t)f13;
  int64_t f13f14 = (int64_t)f13 * (int64_t)f14;
  int64_t f13f15 = (int64_t)f13 * (int64_t)f15;
  int64_t f14f0 = (int64_t)f14 * (int64_t)f0;
  int64_t f14f1 = (int64_t)f14 * (int64_t)f1;
  int64_t f14f2 = (int64_t)f14 * (int64_t)f2;
  int64_t f14f3 = (int64_t)f14 * (int64_t)f3;
  int64_t f14f4 = (int64_t)f14 * (int64_t)f4;
  int64_t f14f5 = (int64_t)f14 * (int64_t)f5;
  int64_t f14f6 = (int64_t)f14 * (int64_t)f6;
  int64_t f14f7 = (int64_t)f14 * (int64_t)f7;
  int64_t f14f8 = (int64_t)f14 * (int64_t)f8;
  int64_t f14f9 = (int64_t)f14 * (int64_t)f9;
  int64_t f14f10 = (int64_t)f14 * (int64_t)f10;
  int64_t f14f11 = (int64_t)f14 * (int64_t)f11;
  int64_t f14f12 = (int64_t)f14 * (int64_t)f12;
  int64_t f14f13 = (int64_t)f14 * (int64_t)f13;
  int64_t f14f14 = (int64_t)f14 * (int64_t)f14;
  int64_t f14f15 = (int64_t)f14 * (int64_t)f15;
  int64_t f15f0 = (int64_t)f15 * (int64_t)f0;
  int64_t f15f1 = (int64_t)f15 * (int64_t)f1;
  int64_t f15f2 = (int64_t)f15 * (int64_t)f2;
  int64_t f15f3 = (int64_t)f15 * (int64_t)f3;
  int64_t f15f4 = (int64_t)f15 * (int64_t)f4;
  int64_t f15f5 = (int64_t)f15 * (int64_t)f5;
  int64_t f15f6 = (int64_t)f15 * (int64_t)f6;
  int64_t f15f7 = (int64_t)f15 * (int64_t)f7;
  int64_t f15f8 = (int64_t)f15 * (int64_t)f8;
  int64_t f15f9 = (int64_t)f15 * (int64_t)f9;
  int64_t f15f10 = (int64_t)f15 * (int64_t)f10;
  int64_t f15f11 = (int64_t)f15 * (int64_t)f11;
  int64_t f15f12 = (int64_t)f15 * (int64_t)f12;
  int64_t f15f13 = (int64_t)f15 * (int64_t)f13;
  int64_t f15f14 = (int64_t)f15 * (int64_t)f14;
  int64_t f15f15 = (int64_t)f15 * (int64_t)f15;
  h0 = f0f0 + f1f15 + f2f14 + f3f13 + f4f12 + f5f11 + f6f10 + f7f9 + f8f8 +
       f9f7 + f9f15 + f10f6 + f10f14 + f11f5 + f11f13 + f12f4 + f12f12 + f13f3 +
       f13f11 + f14f2 + f14f10 + f15f1 + f15f9;
  h1 = f0f1 + f1f0 + f2f15 + f3f14 + f4f13 + f5f12 + f6f11 + f7f10 + f8f9 +
       f9f8 + f10f7 + f10f15 + f11f6 + f11f14 + f12f5 + f12f13 + f13f4 +
       f13f12 + f14f3 + f14f11 + f15f2 + f15f10;
  h2 = f0f2 + f1f1 + f2f0 + f3f15 + f4f14 + f5f13 + f6f12 + f7f11 + f8f10 +
       f9f9 + f10f8 + f11f7 + f11f15 + f12f6 + f12f14 + f13f5 + f13f13 + f14f4 +
       f14f12 + f15f3 + f15f11;
  h3 = f0f3 + f1f2 + f2f1 + f3f0 + f4f15 + f5f14 + f6f13 + f7f12 + f8f11 +
       f9f10 + f10f9 + f11f8 + f12f7 + f12f15 + f13f6 + f13f14 + f14f5 +
       f14f13 + f15f4 + f15f12;
  h4 = f0f4 + f1f3 + f2f2 + f3f1 + f4f0 + f5f15 + f6f14 + f7f13 + f8f12 +
       f9f11 + f10f10 + f11f9 + f12f8 + f13f7 + f13f15 + f14f6 + f14f14 +
       f15f5 + f15f13;
  h5 = f0f5 + f1f4 + f2f3 + f3f2 + f4f1 + f5f0 + f6f15 + f7f14 + f8f13 + f9f12 +
       f10f11 + f11f10 + f12f9 + f13f8 + f14f7 + f14f15 + f15f6 + f15f14;
  h6 = f0f6 + f1f5 + f2f4 + f3f3 + f4f2 + f5f1 + f6f0 + f7f15 + f8f14 + f9f13 +
       f10f12 + f11f11 + f12f10 + f13f9 + f14f8 + f15f7 + f15f15;
  h7 = f0f7 + f1f6 + f2f5 + f3f4 + f4f3 + f5f2 + f6f1 + f7f0 + f8f15 + f9f14 +
       f10f13 + f11f12 + f12f11 + f13f10 + f14f9 + f15f8;
  h8 = f0f8 + f1f7 + f1f15 + f2f6 + f2f14 + f3f5 + f3f13 + f4f4 + f4f12 + f5f3 +
       f5f11 + f6f2 + f6f10 + f7f1 + f7f9 + f8f0 + f8f8 + f9f7 + f9f15 + f9f15 +
       f10f6 + f10f14 + f10f14 + f11f5 + f11f13 + f11f13 + f12f4 + f12f12 +
       f12f12 + f13f3 + f13f11 + f13f11 + f14f2 + f14f10 + f14f10 + f15f1 +
       f15f9 + f15f9;
  h9 = f0f9 + f1f8 + f2f7 + f2f15 + f3f6 + f3f14 + f4f5 + f4f13 + f5f4 + f5f12 +
       f6f3 + f6f11 + f7f2 + f7f10 + f8f1 + f8f9 + f9f0 + f9f8 + f10f7 +
       f10f15 + f10f15 + f11f6 + f11f14 + f11f14 + f12f5 + f12f13 + f12f13 +
       f13f4 + f13f12 + f13f12 + f14f3 + f14f11 + f14f11 + f15f2 + f15f10 +
       f15f10;
  h10 = f0f10 + f1f9 + f2f8 + f3f7 + f3f15 + f4f6 + f4f14 + f5f5 + f5f13 +
        f6f4 + f6f12 + f7f3 + f7f11 + f8f2 + f8f10 + f9f1 + f9f9 + f10f0 +
        f10f8 + f11f7 + f11f15 + f11f15 + f12f6 + f12f14 + f12f14 + f13f5 +
        f13f13 + f13f13 + f14f4 + f14f12 + f14f12 + f15f3 + f15f11 + f15f11;
  h11 = f0f11 + f1f10 + f2f9 + f3f8 + f4f7 + f4f15 + f5f6 + f5f14 + f6f5 +
        f6f13 + f7f4 + f7f12 + f8f3 + f8f11 + f9f2 + f9f10 + f10f1 + f10f9 +
        f11f0 + f11f8 + f12f7 + f12f15 + f12f15 + f13f6 + f13f14 + f13f14 +
        f14f5 + f14f13 + f14f13 + f15f4 + f15f12 + f15f12;
  h12 = f0f12 + f1f11 + f2f10 + f3f9 + f4f8 + f5f7 + f5f15 + f6f6 + f6f14 +
        f7f5 + f7f13 + f8f4 + f8f12 + f9f3 + f9f11 + f10f2 + f10f10 + f11f1 +
        f11f9 + f12f0 + f12f8 + f13f7 + f13f15 + f13f15 + f14f6 + f14f14 +
        f14f14 + f15f5 + f15f13 + f15f13;
  h13 = f0f13 + f1f12 + f2f11 + f3f10 + f4f9 + f5f8 + f6f7 + f6f15 + f7f6 +
        f7f14 + f8f5 + f8f13 + f9f4 + f9f12 + f10f3 + f10f11 + f11f2 + f11f10 +
        f12f1 + f12f9 + f13f0 + f13f8 + f14f7 + f14f15 + f14f15 + f15f6 +
        f15f14 + f15f14;
  h14 = f0f14 + f1f13 + f2f12 + f3f11 + f4f10 + f5f9 + f6f8 + f7f7 + f7f15 +
        f8f6 + f8f14 + f9f5 + f9f13 + f10f4 + f10f12 + f11f3 + f11f11 + f12f2 +
        f12f10 + f13f1 + f13f9 + f14f0 + f14f8 + f15f7 + f15f15 + f15f15;
  h15 = f0f15 + f1f14 + f2f13 + f3f12 + f4f11 + f5f10 + f6f9 + f7f8 + f8f7 +
        f8f15 + f9f6 + f9f14 + f10f5 + f10f13 + f11f4 + f11f12 + f12f3 +
        f12f11 + f13f2 + f13f10 + f14f1 + f14f9 + f15f0 + f15f8;

  CARRY_SIGNED64(h0, h1, carry);
  CARRY_SIGNED64(h7, h8, carry);

  CARRY_SIGNED64(h1, h2, carry);
  CARRY_SIGNED64(h8, h9, carry);

  CARRY_SIGNED64(h2, h3, carry);
  CARRY_SIGNED64(h9, h10, carry);

  CARRY_SIGNED64(h3, h4, carry);
  CARRY_SIGNED64(h10, h11, carry);

  CARRY_SIGNED64(h4, h5, carry);
  CARRY_SIGNED64(h11, h12, carry);

  CARRY_SIGNED64(h5, h6, carry);
  CARRY_SIGNED64(h12, h13, carry);

  CARRY_SIGNED64(h6, h7, carry);
  CARRY_SIGNED64(h13, h14, carry);

  CARRY_SIGNED64(h7, h8, carry);
  CARRY_SIGNED64(h14, h15, carry);

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;

  CARRY_SIGNED64(h0, h1, carry);
  CARRY_SIGNED64(h8, h9, carry);

  STORE(h, h);
}

void fe_invert(fe out, const fe z)
{
  fe t;
  int i;

  fe_copy(t, z);

  for (i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, z);
  }
  fe_sq(t, t);

  for (i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, z);
  }
  fe_sq(t, t);
  fe_sq(t, t);
  fe_mul(t, t, z);

  fe_copy(out, t);
}

void fe_mul39081(fe h, const fe f)
{
  DECL64(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= 39081;
  h1 *= 39081;
  h2 *= 39081;
  h3 *= 39081;
  h4 *= 39081;
  h5 *= 39081;
  h6 *= 39081;
  h7 *= 39081;
  h8 *= 39081;
  h9 *= 39081;
  h10 *= 39081;
  h11 *= 39081;
  h12 *= 39081;
  h13 *= 39081;
  h14 *= 39081;
  h15 *= 39081;

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;
  CARRY_SIGNED64_ALL(h, carry);

  STORE(h, h);
}

void fe_mul39082(fe h, const fe f)
{
  DECL64(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= 39082;
  h1 *= 39082;
  h2 *= 39082;
  h3 *= 39082;
  h4 *= 39082;
  h5 *= 39082;
  h6 *= 39082;
  h7 *= 39082;
  h8 *= 39082;
  h9 *= 39082;
  h10 *= 39082;
  h11 *= 39082;
  h12 *= 39082;
  h13 *= 39082;
  h14 *= 39082;
  h15 *= 39082;

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;
  CARRY_SIGNED64_ALL(h, carry);

  STORE(h, h);
}

void fe_mulm39081(fe h, const fe f)
{
  DECL64(h);
  LOAD(h, f);
  int64_t carry;
  h0 *= -39081;
  h1 *= -39081;
  h2 *= -39081;
  h3 *= -39081;
  h4 *= -39081;
  h5 *= -39081;
  h6 *= -39081;
  h7 *= -39081;
  h8 *= -39081;
  h9 *= -39081;
  h10 *= -39081;
  h11 *= -39081;
  h12 *= -39081;
  h13 *= -39081;
  h14 *= -39081;
  h15 *= -39081;

  CARRY_SIGNED64(h15, h0, carry);
  h8 += carry;
  CARRY_SIGNED64_ALL(h, carry);

  STORE(h, h);
}

void fe_pow_p448s3d4(fe h, const fe f)
{
  fe t;
  fe_copy(t, f);
  for (int i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, f);
  }
  fe_sq(t, t);
  for (int i = 0; i < 222; i++) {
    fe_sq(t, t);
    fe_mul(t, t, f);
  }
  fe_copy(h, t);
}

void fe_neg(fe h, const fe f)
{
  DECL(f);
  LOAD(f, f);
  int32_t h0 = -f0;
  int32_t h1 = -f1;
  int32_t h2 = -f2;
  int32_t h3 = -f3;
  int32_t h4 = -f4;
  int32_t h5 = -f5;
  int32_t h6 = -f6;
  int32_t h7 = -f7;
  int32_t h8 = -f8;
  int32_t h9 = -f9;
  int32_t h10 = -f10;
  int32_t h11 = -f11;
  int32_t h12 = -f12;
  int32_t h13 = -f13;
  int32_t h14 = -f14;
  int32_t h15 = -f15;
  STORE(h, h);
}
