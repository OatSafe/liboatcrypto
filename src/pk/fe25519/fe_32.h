#include <stdint.h>

typedef int32_t fe[10];

/*
fe means field element.
Here the field is \Z/(2^255-19).
An element t, entries t[0]...t[9], represents the integer
t[0]+2^26 t[1]+2^51 t[2]+2^77 t[3]+2^102 t[4]+...+2^230 t[9].
Bounds on each t[i] vary depending on context.
*/


extern void fe_cswap(fe, fe, uint32_t);
extern void fe_cmov(fe, const fe, uint32_t);
