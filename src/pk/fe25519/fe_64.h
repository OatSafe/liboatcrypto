#include <stdint.h>

typedef int64_t fe[5];

void fe_cswap(fe f, fe g, uint64_t b);
void fe_cmov(fe f, const fe g, uint64_t b);