#ifndef _INTERNAL_MCRYPTO_FE25519_FE_H
#define _INTERNAL_MCRYPTO_FE25519_FE_H
#include "mcrypto/bits.h"
#include <stdint.h>

#define fe_frombytes mcrypto_fe25519_frombytes
#define fe_tobytes mcrypto_fe25519_tobytes
#define fe_copy mcrypto_fe25519_copy
#define fe_isnonzero mcrypto_fe25519_isnonzero
#define fe_isnegative mcrypto_fe25519_isnegative
#define fe_0 mcrypto_fe25519_0
#define fe_1 mcrypto_fe25519_1
#define fe_cswap mcrypto_fe25519_cswap
#define fe_cmov mcrypto_fe25519_cmov
#define fe_add mcrypto_fe25519_add
#define fe_sub mcrypto_fe25519_sub
#define fe_neg mcrypto_fe25519_neg
#define fe_mul mcrypto_fe25519_mul
#define fe_sq mcrypto_fe25519_sq
#define fe_sq2 mcrypto_fe25519_sq2
#define fe_mul121666 mcrypto_fe25519_mul121666
#define fe_invert mcrypto_fe25519_invert
#define fe_pow22523 mcrypto_fe25519_pow22523

#ifdef MCRYPTO_NATIVE_64BIT
#include "fe_64.h"
#else
#include "fe_32.h"
#endif

void fe_frombytes(fe, const uint8_t *);
void fe_tobytes(uint8_t *, const fe);

void fe_copy(fe, const fe);
int fe_isnonzero(const fe);
int fe_isnegative(const fe);
void fe_0(fe);
void fe_1(fe);

void fe_add(fe, const fe, const fe);
void fe_sub(fe, const fe, const fe);
void fe_neg(fe, const fe);
void fe_mul(fe, const fe, const fe);
void fe_sq(fe, const fe);
void fe_sq2(fe, const fe);
void fe_mul121666(fe, const fe);
void fe_invert(fe, const fe);
void fe_pow22523(fe, const fe);

#endif
