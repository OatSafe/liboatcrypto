static inline uint128_t load_8(const uint8_t *in)
{
  uint128_t result;
  result = (uint128_t)in[0];
  result |= ((uint128_t)in[1]) << 8;
  result |= ((uint128_t)in[2]) << 16;
  result |= ((uint128_t)in[3]) << 24;
  result |= ((uint128_t)in[4]) << 32;
  result |= ((uint128_t)in[5]) << 40;
  result |= ((uint128_t)in[6]) << 48;
  result |= ((uint128_t)in[7]) << 56;
  return result;
}

static inline uint128_t load_6(const uint8_t *in)
{
  uint128_t result;
  result = (uint128_t)in[0];
  result |= ((uint128_t)in[1]) << 8;
  result |= ((uint128_t)in[2]) << 16;
  result |= ((uint128_t)in[3]) << 24;
  result |= ((uint128_t)in[4]) << 32;
  result |= ((uint128_t)in[5]) << 40;
  return result;
}


void fe_frombytes(fe h, const uint8_t *s)
{
  /* h0 < 2^64 */
  int128_t h0 = load_8(s);
  /* h1 < 2^60 */
  int128_t h1 = load_6(s + 8) << 13;
  /* h2 < 2^58 */
  int128_t h2 = load_6(s + 14) << 10;
  /* h3 < 2^55 */
  int128_t h3 = load_6(s + 20) << 7;
  /* h4 < 2^51 */
  int128_t h4 = (load_6(s + 26) & UINT64_C(0x7FFFFFFFFFFF)) << 4;
  int128_t carry;

  carry = (h4 + ((int128_t)1 << 50)) >> 51;
  h0 += carry * 19;
  h4 -= carry << 51;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h1 + ((int128_t)1 << 50)) >> 51;
  h2 += carry;
  h1 -= carry << 51;

  carry = (h3 + ((int128_t)1 << 50)) >> 51;
  h4 += carry;
  h3 -= carry << 51;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_tobytes(uint8_t *s, const fe h)
{
  int64_t h0 = h[0];
  int64_t h1 = h[1];
  int64_t h2 = h[2];
  int64_t h3 = h[3];
  int64_t h4 = h[4];
  int64_t q;
  int64_t carry0;
  int64_t carry1;
  int64_t carry2;
  int64_t carry3;
  int64_t carry4;

  q = (19 * h4 + (((int64_t)1) << 50)) >> 51;
  q = (h0 + q) >> 51;
  q = (h1 + q) >> 51;
  q = (h2 + q) >> 51;
  q = (h3 + q) >> 51;
  q = (h4 + q) >> 51;

  /* Goal: Output h-(2^255-19)q, which is between 0 and 2^255-20. */
  h0 += 19 * q;
  /* Goal: Output h-2^255 q, which is between 0 and 2^255-20. */

  carry0 = h0 >> 51;
  h1 += carry0;
  h0 -= carry0 << 51;
  carry1 = h1 >> 51;
  h2 += carry1;
  h1 -= carry1 << 51;
  carry2 = h2 >> 51;
  h3 += carry2;
  h2 -= carry2 << 51;
  carry3 = h3 >> 51;
  h4 += carry3;
  h3 -= carry3 << 51;
  carry4 = h4 >> 51;
  h4 -= carry4 << 51;


  s[0] = h0 >> 0;
  s[1] = h0 >> 8;
  s[2] = h0 >> 16;
  s[3] = h0 >> 24;
  s[4] = h0 >> 32;
  s[5] = h0 >> 40;
  s[6] = h0 >> 48 | h1 << 3;
  s[7] = h1 >> 5;
  s[8] = h1 >> 13;
  s[9] = h1 >> 21;
  s[10] = h1 >> 29;
  s[11] = h1 >> 37;
  s[12] = h1 >> 45 | h2 << 6;
  s[13] = h2 >> 2;
  s[14] = h2 >> 10;
  s[15] = h2 >> 18;
  s[16] = h2 >> 26;
  s[17] = h2 >> 34;
  s[18] = h2 >> 42;
  s[19] = h2 >> 50 | h3 << 1;
  s[20] = h3 >> 7;
  s[21] = h3 >> 15;
  s[22] = h3 >> 23;
  s[23] = h3 >> 31;
  s[24] = h3 >> 39;
  s[25] = h3 >> 47 | h4 << 4;
  s[26] = h4 >> 4;
  s[27] = h4 >> 12;
  s[28] = h4 >> 20;
  s[29] = h4 >> 28;
  s[30] = h4 >> 36;
  s[31] = h4 >> 44;
}

void fe_copy(fe h, const fe f)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  h[0] = f0;
  h[1] = f1;
  h[2] = f2;
  h[3] = f3;
  h[4] = f4;
}

int fe_isnonzero(const fe f)
{
  uint8_t s[32];
  fe_tobytes(s, f);
  uint8_t r = 0;
  for (int i = 0; i < 32; i++)
    r |= s[i];
  return r != 0;
}

int fe_isnegative(const fe f)
{
  uint8_t s[32];
  fe_tobytes(s, f);
  return s[0] & 1;
}

void fe_0(fe h)
{
  h[0] = 0;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
}


void fe_1(fe h)
{
  h[0] = 1;
  h[1] = 0;
  h[2] = 0;
  h[3] = 0;
  h[4] = 0;
}

void fe_cswap(fe f, fe g, uint64_t b)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t g0 = g[0];
  int64_t g1 = g[1];
  int64_t g2 = g[2];
  int64_t g3 = g[3];
  int64_t g4 = g[4];
  int64_t x0 = f0 ^ g0;
  int64_t x1 = f1 ^ g1;
  int64_t x2 = f2 ^ g2;
  int64_t x3 = f3 ^ g3;
  int64_t x4 = f4 ^ g4;
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  f[0] = f0 ^ x0;
  f[1] = f1 ^ x1;
  f[2] = f2 ^ x2;
  f[3] = f3 ^ x3;
  f[4] = f4 ^ x4;
  g[0] = g0 ^ x0;
  g[1] = g1 ^ x1;
  g[2] = g2 ^ x2;
  g[3] = g3 ^ x3;
  g[4] = g4 ^ x4;
}

void fe_cmov(fe f, const fe g, uint64_t b)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t g0 = g[0];
  int64_t g1 = g[1];
  int64_t g2 = g[2];
  int64_t g3 = g[3];
  int64_t g4 = g[4];
  int64_t x0 = f0 ^ g0;
  int64_t x1 = f1 ^ g1;
  int64_t x2 = f2 ^ g2;
  int64_t x3 = f3 ^ g3;
  int64_t x4 = f4 ^ g4;
  b = -b;
  x0 &= b;
  x1 &= b;
  x2 &= b;
  x3 &= b;
  x4 &= b;
  f[0] = f0 ^ x0;
  f[1] = f1 ^ x1;
  f[2] = f2 ^ x2;
  f[3] = f3 ^ x3;
  f[4] = f4 ^ x4;
}

void fe_add(fe h, const fe f, const fe g)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];

  int64_t g0 = g[0];
  int64_t g1 = g[1];
  int64_t g2 = g[2];
  int64_t g3 = g[3];
  int64_t g4 = g[4];

  int64_t h0 = f0 + g0;
  int64_t h1 = f1 + g1;
  int64_t h2 = f2 + g2;
  int64_t h3 = f3 + g3;
  int64_t h4 = f4 + g4;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_sub(fe h, const fe f, const fe g)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];

  int64_t g0 = g[0];
  int64_t g1 = g[1];
  int64_t g2 = g[2];
  int64_t g3 = g[3];
  int64_t g4 = g[4];

  int64_t h0 = f0 - g0;
  int64_t h1 = f1 - g1;
  int64_t h2 = f2 - g2;
  int64_t h3 = f3 - g3;
  int64_t h4 = f4 - g4;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}


void fe_neg(fe h, const fe f)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t h0 = -f0;
  int64_t h1 = -f1;
  int64_t h2 = -f2;
  int64_t h3 = -f3;
  int64_t h4 = -f4;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_mul(fe h, const fe f, const fe g)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t g0 = g[0];
  int64_t g1 = g[1];
  int64_t g2 = g[2];
  int64_t g3 = g[3];
  int64_t g4 = g[4];

  int64_t g1_19 = g1 * 19;
  int64_t g2_19 = g2 * 19;
  int64_t g3_19 = g3 * 19;
  int64_t g4_19 = g4 * 19;

  int128_t carry;
  int128_t f0g0 = (int128_t)f0 * g0;
  int128_t f0g1 = (int128_t)f0 * g1;
  int128_t f0g2 = (int128_t)f0 * g2;
  int128_t f0g3 = (int128_t)f0 * g3;
  int128_t f0g4 = (int128_t)f0 * g4;
  int128_t f1g0 = (int128_t)f1 * g0;
  int128_t f1g1 = (int128_t)f1 * g1;
  int128_t f1g2 = (int128_t)f1 * g2;
  int128_t f1g3 = (int128_t)f1 * g3;
  int128_t f1g4_19 = (int128_t)f1 * g4_19;
  int128_t f2g0 = (int128_t)f2 * g0;
  int128_t f2g1 = (int128_t)f2 * g1;
  int128_t f2g2 = (int128_t)f2 * g2;
  int128_t f2g3_19 = (int128_t)f2 * g3_19;
  int128_t f2g4_19 = (int128_t)f2 * g4_19;
  int128_t f3g0 = (int128_t)f3 * g0;
  int128_t f3g1 = (int128_t)f3 * g1;
  int128_t f3g2_19 = (int128_t)f3 * g2_19;
  int128_t f3g3_19 = (int128_t)f3 * g3_19;
  int128_t f3g4_19 = (int128_t)f3 * g4_19;
  int128_t f4g0 = (int128_t)f4 * g0;
  int128_t f4g1_19 = (int128_t)f4 * g1_19;
  int128_t f4g2_19 = (int128_t)f4 * g2_19;
  int128_t f4g3_19 = (int128_t)f4 * g3_19;
  int128_t f4g4_19 = (int128_t)f4 * g4_19;
  int128_t h0 = f0g0 + f1g4_19 + f2g3_19 + f3g2_19 + f4g1_19;
  int128_t h1 = f0g1 + f1g0 + f2g4_19 + f3g3_19 + f4g2_19;
  int128_t h2 = f0g2 + f1g1 + f2g0 + f3g4_19 + f4g3_19;
  int128_t h3 = f0g3 + f1g2 + f2g1 + f3g0 + f4g4_19;
  int128_t h4 = f0g4 + f1g3 + f2g2 + f3g1 + f4g0;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h1 + ((int128_t)1 << 50)) >> 51;
  h2 += carry;
  h1 -= carry << 51;

  carry = (h3 + ((int128_t)1 << 50)) >> 51;
  h4 += carry;
  h3 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h4 + ((int128_t)1 << 50)) >> 51;
  h0 += carry * 19;
  h4 -= carry << 51;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_sq(fe h, const fe f)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t f1_19 = f1 * 19;
  int64_t f2_19 = f2 * 19;
  int64_t f3_19 = f3 * 19;
  int64_t f4_19 = f4 * 19;

  int128_t carry;
  int128_t f0f0 = (int128_t)f0 * f0;
  int128_t f0f1 = (int128_t)f0 * f1;
  int128_t f0f2 = (int128_t)f0 * f2;
  int128_t f0f3 = (int128_t)f0 * f3;
  int128_t f0f4 = (int128_t)f0 * f4;
  int128_t f1f0 = (int128_t)f1 * f0;
  int128_t f1f1 = (int128_t)f1 * f1;
  int128_t f1f2 = (int128_t)f1 * f2;
  int128_t f1f3 = (int128_t)f1 * f3;
  int128_t f1f4_19 = (int128_t)f1 * f4_19;
  int128_t f2f0 = (int128_t)f2 * f0;
  int128_t f2f1 = (int128_t)f2 * f1;
  int128_t f2f2 = (int128_t)f2 * f2;
  int128_t f2f3_19 = (int128_t)f2 * f3_19;
  int128_t f2f4_19 = (int128_t)f2 * f4_19;
  int128_t f3f0 = (int128_t)f3 * f0;
  int128_t f3f1 = (int128_t)f3 * f1;
  int128_t f3f2_19 = (int128_t)f3 * f2_19;
  int128_t f3f3_19 = (int128_t)f3 * f3_19;
  int128_t f3f4_19 = (int128_t)f3 * f4_19;
  int128_t f4f0 = (int128_t)f4 * f0;
  int128_t f4f1_19 = (int128_t)f4 * f1_19;
  int128_t f4f2_19 = (int128_t)f4 * f2_19;
  int128_t f4f3_19 = (int128_t)f4 * f3_19;
  int128_t f4f4_19 = (int128_t)f4 * f4_19;
  int128_t h0 = f0f0 + f1f4_19 + f2f3_19 + f3f2_19 + f4f1_19;
  int128_t h1 = f0f1 + f1f0 + f2f4_19 + f3f3_19 + f4f2_19;
  int128_t h2 = f0f2 + f1f1 + f2f0 + f3f4_19 + f4f3_19;
  int128_t h3 = f0f3 + f1f2 + f2f1 + f3f0 + f4f4_19;
  int128_t h4 = f0f4 + f1f3 + f2f2 + f3f1 + f4f0;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h1 + ((int128_t)1 << 50)) >> 51;
  h2 += carry;
  h1 -= carry << 51;

  carry = (h3 + ((int128_t)1 << 50)) >> 51;
  h4 += carry;
  h3 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h4 + ((int128_t)1 << 50)) >> 51;
  h0 += carry * 19;
  h4 -= carry << 51;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_sq2(fe h, const fe f)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int64_t f1_19 = f1 * 19;
  int64_t f2_19 = f2 * 19;
  int64_t f3_19 = f3 * 19;
  int64_t f4_19 = f4 * 19;

  int128_t carry;
  int128_t f0f0 = (int128_t)f0 * f0;
  int128_t f0f1 = (int128_t)f0 * f1;
  int128_t f0f2 = (int128_t)f0 * f2;
  int128_t f0f3 = (int128_t)f0 * f3;
  int128_t f0f4 = (int128_t)f0 * f4;
  int128_t f1f0 = (int128_t)f1 * f0;
  int128_t f1f1 = (int128_t)f1 * f1;
  int128_t f1f2 = (int128_t)f1 * f2;
  int128_t f1f3 = (int128_t)f1 * f3;
  int128_t f1f4_19 = (int128_t)f1 * f4_19;
  int128_t f2f0 = (int128_t)f2 * f0;
  int128_t f2f1 = (int128_t)f2 * f1;
  int128_t f2f2 = (int128_t)f2 * f2;
  int128_t f2f3_19 = (int128_t)f2 * f3_19;
  int128_t f2f4_19 = (int128_t)f2 * f4_19;
  int128_t f3f0 = (int128_t)f3 * f0;
  int128_t f3f1 = (int128_t)f3 * f1;
  int128_t f3f2_19 = (int128_t)f3 * f2_19;
  int128_t f3f3_19 = (int128_t)f3 * f3_19;
  int128_t f3f4_19 = (int128_t)f3 * f4_19;
  int128_t f4f0 = (int128_t)f4 * f0;
  int128_t f4f1_19 = (int128_t)f4 * f1_19;
  int128_t f4f2_19 = (int128_t)f4 * f2_19;
  int128_t f4f3_19 = (int128_t)f4 * f3_19;
  int128_t f4f4_19 = (int128_t)f4 * f4_19;
  int128_t h0 = f0f0 + f1f4_19 + f2f3_19 + f3f2_19 + f4f1_19;
  int128_t h1 = f0f1 + f1f0 + f2f4_19 + f3f3_19 + f4f2_19;
  int128_t h2 = f0f2 + f1f1 + f2f0 + f3f4_19 + f4f3_19;
  int128_t h3 = f0f3 + f1f2 + f2f1 + f3f0 + f4f4_19;
  int128_t h4 = f0f4 + f1f3 + f2f2 + f3f1 + f4f0;

  h0 += h0;
  h1 += h1;
  h2 += h2;
  h3 += h3;
  h4 += h4;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;
  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h1 + ((int128_t)1 << 50)) >> 51;
  h2 += carry;
  h1 -= carry << 51;
  carry = (h3 + ((int128_t)1 << 50)) >> 51;
  h4 += carry;
  h3 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;
  carry = (h4 + ((int128_t)1 << 50)) >> 51;
  h0 += carry * 19;
  h4 -= carry << 51;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}

void fe_invert(fe out, const fe z)
{
  fe t0;
  fe t1;
  fe t2;
  fe t3;
  int i;

  fe_sq(t0, z);
  fe_sq(t1, t0);
  fe_sq(t1, t1);
  fe_mul(t1, z, t1);
  fe_mul(t0, t0, t1);
  fe_sq(t2, t0);
  fe_mul(t1, t1, t2);
  fe_sq(t2, t1);
  for (i = 1; i < 5; ++i)
    fe_sq(t2, t2);

  fe_mul(t1, t2, t1);
  fe_sq(t2, t1);
  for (i = 1; i < 10; ++i)
    fe_sq(t2, t2);

  fe_mul(t2, t2, t1);
  fe_sq(t3, t2);
  for (i = 1; i < 20; ++i)
    fe_sq(t3, t3);

  fe_mul(t2, t3, t2);
  fe_sq(t2, t2);
  for (i = 1; i < 10; ++i)
    fe_sq(t2, t2);

  fe_mul(t1, t2, t1);
  fe_sq(t2, t1);
  for (i = 1; i < 50; ++i)
    fe_sq(t2, t2);

  fe_mul(t2, t2, t1);
  fe_sq(t3, t2);
  for (i = 1; i < 100; ++i)
    fe_sq(t3, t3);

  fe_mul(t2, t3, t2);
  fe_sq(t2, t2);
  for (i = 1; i < 50; ++i)
    fe_sq(t2, t2);

  fe_mul(t1, t2, t1);
  fe_sq(t1, t1);
  for (i = 1; i < 5; ++i)
    fe_sq(t1, t1);

  fe_mul(out, t1, t0);
  return;
}

void fe_pow22523(fe out, const fe z)
{
  fe t0;
  fe t1;
  fe t2;
  int i;

  fe_sq(t0, z);
  fe_sq(t1, t0);
  fe_sq(t1, t1);
  fe_mul(t1, z, t1);
  fe_mul(t0, t0, t1);
  fe_sq(t0, t0);
  fe_mul(t0, t1, t0);
  fe_sq(t1, t0);
  for (i = 1; i < 5; ++i)
    fe_sq(t1, t1);

  fe_mul(t0, t1, t0);
  fe_sq(t1, t0);
  for (i = 1; i < 10; ++i)
    fe_sq(t1, t1);

  fe_mul(t1, t1, t0);
  fe_sq(t2, t1);
  for (i = 1; i < 20; ++i)
    fe_sq(t2, t2);

  fe_mul(t1, t2, t1);
  fe_sq(t1, t1);
  for (i = 1; i < 10; ++i)
    fe_sq(t1, t1);

  fe_mul(t0, t1, t0);
  fe_sq(t1, t0);
  for (i = 1; i < 50; ++i)
    fe_sq(t1, t1);

  fe_mul(t1, t1, t0);
  fe_sq(t2, t1);
  for (i = 1; i < 100; ++i)
    fe_sq(t2, t2);

  fe_mul(t1, t2, t1);
  fe_sq(t1, t1);
  for (i = 1; i < 50; ++i)
    fe_sq(t1, t1);

  fe_mul(t0, t1, t0);
  fe_sq(t0, t0);
  fe_sq(t0, t0);
  fe_mul(out, t0, z);

  return;
}

/*
h = f * 121666
Can overlap h with f.

Preconditions:
   |f| bounded by 1.1*2^26,1.1*2^25,1.1*2^26,1.1*2^25,etc.

Postconditions:
   |h| bounded by 1.1*2^25,1.1*2^24,1.1*2^25,1.1*2^24,etc.
*/

void fe_mul121666(fe h, const fe f)
{
  int64_t f0 = f[0];
  int64_t f1 = f[1];
  int64_t f2 = f[2];
  int64_t f3 = f[3];
  int64_t f4 = f[4];
  int128_t h0 = f0 * (int128_t)121666;
  int128_t h1 = f1 * (int128_t)121666;
  int128_t h2 = f2 * (int128_t)121666;
  int128_t h3 = f3 * (int128_t)121666;
  int128_t h4 = f4 * (int128_t)121666;
  int128_t carry;

  carry = (h4 + ((int128_t)1 << 50)) >> 51;
  h0 += carry * 19;
  h4 -= carry << 51;

  carry = (h0 + ((int128_t)1 << 50)) >> 51;
  h1 += carry;
  h0 -= carry << 51;

  carry = (h2 + ((int128_t)1 << 50)) >> 51;
  h3 += carry;
  h2 -= carry << 51;

  carry = (h1 + ((int128_t)1 << 50)) >> 51;
  h2 += carry;
  h1 -= carry << 51;

  carry = (h3 + ((int128_t)1 << 50)) >> 51;
  h4 += carry;
  h3 -= carry << 51;

  h[0] = h0;
  h[1] = h1;
  h[2] = h2;
  h[3] = h3;
  h[4] = h4;
}
