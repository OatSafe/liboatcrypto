#ifndef _INTERNAL_MCRYPTO_ED448_GE_H
#define _INTERNAL_MCRYPTO_ED448_GE_H

/*
ge means group element.

Here the group is the set of pairs (x,y) of field elements (see fe.h)
satisfying -x^2 + y^2 = 1 + d x^2y^2
where d = -121665/121666.

Representations:
  ge_p2 (projective): (X:Y:Z) satisfying x=X/Z, y=Y/Z
  ge_p3 (extended): (X:Y:Z:T) satisfying x=X/Z, y=Y/Z, XY=ZT
  ge_p1p1 (completed): ((X:Z),(Y:T)) satisfying x=X/Z, y=Y/T
  ge_precomp (Duif): (y+x,y-x,2dxy)
*/

#include "../../fe448/fe.h"

typedef struct {
  fe X;
  fe Y;
} ge_p1;

typedef struct {
  fe X;
  fe Y;
  fe Z;
} ge_p2;

typedef struct {
  fe X;
  fe Y;
  fe Z;
  fe T;
} ge_p3;

typedef struct {
  fe X;
  fe Y;
  fe Z;
  fe T;
} ge_p1p1;

typedef struct {
  fe X;
  fe Y;
  fe YpX;
  fe YmX;
  fe dXY;
  fe mdXY;
} ge_precomp;

typedef struct {
  fe X;
  fe Y;
  fe Z;
  fe YpX;
  fe YmX;
  fe dT;
  fe mdT;
} ge_cached;

#define ge_frombytes_negate_vartime mcrypto_ed448_ge_frombytes_negate_vartime
#define ge_tobytes mcrypto_ed448_ge_tobytes
#define ge_p2_tobytes mcrypto_ed448_ge_p2_tobytes
#define ge_p3_tobytes mcrypto_ed448_ge_p3_tobytes
#define ge_scalarmult_base mcrypto_ed448_ge_scalarmult_base
#define ge_double_scalarmult_vartime mcrypto_ed448_ge_double_scalarmult_vartime

extern void ge_tobytes(unsigned char *s, const ge_p1 *h);
extern void ge_p2_tobytes(unsigned char *s, const ge_p2 *h);
extern void ge_p3_tobytes(unsigned char *s, const ge_p3 *h);
extern int ge_frombytes_negate_vartime(ge_p3 *h, const unsigned char *s);
extern void ge_scalarmult_base(ge_p3 *h, const unsigned char *a);
void ge_double_scalarmult_vartime(ge_p2 *r, const unsigned char *a,
                                  const ge_p3 *A, const unsigned char *b);

#endif
