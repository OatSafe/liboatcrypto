#include "ge.h"
#include "mcrypto/bits.h"
#ifdef MCRYPTO_NATIVE_64BIT
#include "ge_precomp_64.h"
#else
#include "ge_precomp.h"
#endif
#include <stdio.h>

void ge_tobytes(unsigned char *s, const ge_p1 *h)
{
  fe_tobytes(s, h->Y);
  s[56] = 0;
  s[56] ^= fe_isnegative(h->X) << 7;
}

void ge_p2_tobytes(unsigned char *s, const ge_p2 *h)
{
  fe recip;
  fe x;
  fe y;

  fe_invert(recip, h->Z);
  fe_mul(x, h->X, recip);
  fe_mul(y, h->Y, recip);
  fe_tobytes(s, y);
  s[56] = 0;
  s[56] ^= fe_isnegative(x) << 7;
}

void ge_p3_tobytes(unsigned char *s, const ge_p3 *h)
{
  fe recip;
  fe x;
  fe y;

  fe_invert(recip, h->Z);
  fe_mul(x, h->X, recip);
  fe_mul(y, h->Y, recip);
  fe_tobytes(s, y);
  s[56] = 0;
  s[56] ^= fe_isnegative(x) << 7;
}

int ge_frombytes_negate_vartime(ge_p3 *h, const unsigned char *s)
{
  fe u;
  fe v;
  fe v3;
  fe u3;
  fe u5;
  fe check;

  fe_frombytes(h->Y, s);
  fe_1(h->Z);
  fe_sq(u, h->Y);
  fe_mulm39081(v, u);
  fe_sub(u, u, h->Z); /* u = y^2-1 */
  fe_sub(v, v, h->Z); /* v = dy^2-1 */

  fe_sq(v3, v);
  fe_mul(v3, v3, v); /* v3 = v^3 */

  fe_sq(u5, u);
  fe_mul(u3, u5, u); /* u3 = u^3 */
  fe_sq(u5, u5);
  fe_mul(u5, u5, u); /* u5 = u^5 */

  fe_mul(h->X, u5, v3); /* x= u^5 v^3 */

  fe_pow_p448s3d4(h->X, h->X); /* x = (u^5 v^3)^((q-3)/4) */
  fe_mul(h->X, h->X, u3);
  fe_mul(h->X, h->X, v); /* x = u^3 v (u^5 v^3)^((q-3)/4) */

  fe_sq(check, h->X);
  fe_mul(check, check, v);
  fe_sub(check, check, u); /* vx^2-u */
  if (fe_isnonzero(check)) {
    return -1;
  }

  if (fe_isnegative(h->X) == (s[56] >> 7))
    fe_neg(h->X, h->X);

  fe_mul(h->T, h->X, h->Y);
  return 0;
}

#if 0
static inline void ge_add(ge_p1p1 *r, const ge_p3 *p, const ge_p3 *q)
{
  fe t0, t1;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mulm39081(r->X, q->T);
  fe_mul(r->Z, p->T, r->X);
  fe_mul(t1, p->Z, q->Z);
  fe_add(r->X, p->X, p->Y);
  fe_add(r->T, q->X, q->Y);
  fe_mul(r->X, r->X, r->T);
  fe_sub(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, t1, r->Z);
  fe_add(r->Z, t1, r->Z);
  fe_sub(r->Y, t0, r->Y);
}

static inline void ge_sub(ge_p1p1 *r, const ge_p3 *p, const ge_p3 *q)
{
  fe t0, t1;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mul39081(r->X, q->T);
  fe_mul(r->Z, p->T, r->X);
  fe_mul(t1, p->Z, q->Z);
  fe_add(r->X, p->X, p->Y);
  fe_sub(r->T, q->Y, q->X);
  fe_mul(r->X, r->X, r->T);
  fe_add(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, t1, r->Z);
  fe_add(r->Z, t1, r->Z);
  fe_add(r->Y, t0, r->Y);
}
#endif

static inline void ge_cached_add(ge_p1p1 *r, const ge_p3 *p, const ge_cached *q)
{
  fe t0, t1;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mul(r->Z, p->T, q->dT);
  fe_mul(t1, p->Z, q->Z);
  fe_add(r->X, p->X, p->Y);
  fe_mul(r->X, r->X, q->YpX);
  fe_sub(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, t1, r->Z);
  fe_add(r->Z, t1, r->Z);
  fe_sub(r->Y, t0, r->Y);
}

static inline void ge_cached_sub(ge_p1p1 *r, const ge_p3 *p, const ge_cached *q)
{
  fe t0, t1;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mul(r->Z, p->T, q->mdT);
  fe_mul(t1, p->Z, q->Z);
  fe_add(r->X, p->X, p->Y);
  fe_mul(r->X, r->X, q->YmX);
  fe_add(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, t1, r->Z);
  fe_add(r->Z, t1, r->Z);
  fe_add(r->Y, t0, r->Y);
}

static inline void ge_madd(ge_p1p1 *r, const ge_p3 *p, const ge_precomp *q)
{
  fe t0;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mul(r->Z, p->T, q->dXY);
  fe_add(r->X, p->X, p->Y);
  fe_mul(r->X, r->X, q->YpX);
  fe_sub(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, p->Z, r->Z);
  fe_add(r->Z, p->Z, r->Z);
  fe_sub(r->Y, t0, r->Y);
}

static inline void ge_msub(ge_p1p1 *r, const ge_p3 *p, const ge_precomp *q)
{
  fe t0;
  fe_mul(r->Y, p->X, q->X);
  fe_mul(t0, p->Y, q->Y);
  fe_mul(r->Z, p->T, q->mdXY);
  fe_add(r->X, p->X, p->Y);
  fe_copy(r->T, q->YmX);
  fe_mul(r->X, r->X, r->T);
  fe_add(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_sub(r->T, p->Z, r->Z);
  fe_add(r->Z, p->Z, r->Z);
  fe_add(r->Y, t0, r->Y);
}

static inline void ge_dbl(ge_p1p1 *r, const ge_p2 *p)
{
  fe t0;
  fe_sq(r->Y, p->X);
  fe_sq(t0, p->Y);
  fe_sq(r->X, p->Z);
  fe_add(r->T, r->X, r->X);
  fe_add(r->X, p->X, p->Y);
  fe_sq(r->X, r->X);
  fe_sub(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_add(r->Z, r->Y, t0);
  fe_sub(r->T, r->Z, r->T);
  fe_sub(r->Y, r->Y, t0);
}

static inline void ge_p3_dbl(ge_p1p1 *r, const ge_p3 *p)
{
  fe t0;
  fe_sq(r->Y, p->X);
  fe_sq(t0, p->Y);
  fe_sq(r->X, p->Z);
  fe_add(r->T, r->X, r->X);
  fe_add(r->X, p->X, p->Y);
  fe_sq(r->X, r->X);
  fe_sub(r->X, r->X, r->Y);
  fe_sub(r->X, r->X, t0);
  fe_add(r->Z, r->Y, t0);
  fe_sub(r->T, r->Z, r->T);
  fe_sub(r->Y, r->Y, t0);
}

static inline void ge_p3_0(ge_p3 *h)
{
  fe_0(h->X);
  fe_1(h->Y);
  fe_1(h->Z);
  fe_0(h->T);
}

static inline void ge_p2_0(ge_p2 *h)
{
  fe_0(h->X);
  fe_1(h->Y);
  fe_1(h->Z);
}

void ge_cached_0(ge_cached *h)
{
  fe_0(h->X);
  fe_1(h->Y);
  fe_1(h->Z);
  fe_1(h->YpX);
  fe_1(h->YmX);
  fe_0(h->dT);
  fe_0(h->mdT);
}

static inline void ge_precomp_0(ge_precomp *h)
{
  fe_0(h->X);
  fe_1(h->Y);
  fe_1(h->YpX);
  fe_1(h->YmX);
  fe_0(h->dXY);
  fe_0(h->mdXY);
}

static inline void ge_p3_to_cached(ge_cached *r, const ge_p3 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_copy(r->Z, p->Z);
  fe_add(r->YpX, p->Y, p->X);
  fe_sub(r->YmX, p->Y, p->X);
  fe_mulm39081(r->dT, p->T);
  fe_mul39081(r->mdT, p->T);
}
#if 0
static inline void ge_p2_to_cached(ge_cached *r, const ge_p2 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_copy(r->Z, p->Z);
  fe_mul(r->YpX, p->X, p->Y);
  fe_add(r->YmX, p->Y, p->X);
  fe_sub(r->dT, p->Y, p->X);
  fe_mulm39081(r->mdT, r->YpX);
  fe_mul39081(r->YpX, r->YpX);
}

static inline void ge_p3_to_p2(ge_p2 *r, const ge_p3 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_copy(r->Z, p->Z);
}


static inline void ge_p2_to_p3(ge_p3 *r, const ge_p2 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_copy(r->Z, p->Z);
  fe_mul(r->T, p->X, p->Y);
}

static inline void ge_p1_to_p3(ge_p3 *r, const ge_p1 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_1(r->Z);
  fe_mul(r->T, p->X, p->Y);
}

static inline void ge_p3_to_p1(ge_p1 *r, const ge_p3 *p)
{
  fe_invert(r->Y, p->Z);
  fe_mul(r->X, p->X, r->Y);
  fe_mul(r->Y, p->Y, r->Y);
}


static inline void ge_p1_to_p2(ge_p2 *r, const ge_p1 *p)
{
  fe_copy(r->X, p->X);
  fe_copy(r->Y, p->Y);
  fe_1(r->Z);
}

static inline void ge_p2_to_p1(ge_p1 *r, const ge_p2 *p)
{
  fe_invert(r->Y, p->Z);
  fe_mul(r->X, p->X, r->Y);
  fe_mul(r->Y, p->Y, r->Y);
}

#endif

static inline void ge_p1p1_to_cached(ge_cached *r, const ge_p1p1 *p)
{
  fe_mul(r->X, p->X, p->T);
  fe_mul(r->Y, p->Y, p->Z);
  fe_mul(r->Z, p->Z, p->T);
  fe_mul(r->mdT, p->X, p->Y);
  fe_add(r->YpX, r->X, r->Y);
  fe_sub(r->YmX, r->Y, r->X);
  fe_mulm39081(r->dT, r->mdT);
  fe_mul39081(r->mdT, r->mdT);
}

static inline void ge_p1p1_to_p3(ge_p3 *r, const ge_p1p1 *p)
{
  fe_mul(r->X, p->X, p->T);
  fe_mul(r->Y, p->Y, p->Z);
  fe_mul(r->Z, p->Z, p->T);
  fe_mul(r->T, p->X, p->Y);
}

static inline void ge_p1p1_to_p2(ge_p2 *r, const ge_p1p1 *p)
{
  fe_mul(r->X, p->X, p->T);
  fe_mul(r->Y, p->Y, p->Z);
  fe_mul(r->Z, p->Z, p->T);
}

static inline unsigned char equal(signed char b, signed char c)
{
  unsigned char ub = b;
  unsigned char uc = c;
  unsigned char x = ub ^ uc; /* 0: yes; 1..255: no */
  uint32_t y = x;            /* 0: yes; 1..255: no */
  y -= 1;                    /* 4294967295: yes; 0..254: no */
  y >>= 31;                  /* 1: yes; 0: no */
  return y;
}

static inline unsigned char negative(signed char b)
{
  unsigned long long x =
      b;    /* 18446744073709551361..18446744073709551615: yes; 0..255: no */
  x >>= 63; /* 1: yes; 0: no */
  return x;
}

static inline void cmov(ge_precomp *t, ge_precomp *u, unsigned char b)
{
  fe_cmov(t->X, u->X, b);
  fe_cmov(t->Y, u->Y, b);
  fe_cmov(t->YpX, u->YpX, b);
  fe_cmov(t->YmX, u->YmX, b);
  fe_cmov(t->dXY, u->dXY, b);
  fe_cmov(t->mdXY, u->mdXY, b);
}

static inline void select(ge_precomp *t, int pos, signed char b)
{
  ge_precomp minust;
  unsigned char bnegative = negative(b);
  unsigned char babs = b - (((-bnegative) & b) << 1);

  ge_precomp_0(t);
  cmov(t, &base[pos][0], equal(babs, 1));
  cmov(t, &base[pos][1], equal(babs, 2));
  cmov(t, &base[pos][2], equal(babs, 3));
  cmov(t, &base[pos][3], equal(babs, 4));
  cmov(t, &base[pos][4], equal(babs, 5));
  cmov(t, &base[pos][5], equal(babs, 6));
  cmov(t, &base[pos][6], equal(babs, 7));
  cmov(t, &base[pos][7], equal(babs, 8));

  fe_copy(minust.Y, t->Y);
  fe_copy(minust.YpX, t->YmX);
  fe_copy(minust.YmX, t->YpX);
  fe_copy(minust.dXY, t->mdXY);
  fe_copy(minust.mdXY, t->dXY);
  fe_neg(minust.X, t->X);
  cmov(t, &minust, bnegative);
}

void ge_scalarmult_base(ge_p3 *h, const unsigned char *a)
{
  signed char e[114] = { 0 };
  signed char carry;
  ge_p1p1 r;
  ge_p2 s;
  ge_precomp t;
  int i;

  for (i = 0; i < 57; ++i) {
    e[2 * i + 0] = (a[i] >> 0) & 15;
    e[2 * i + 1] = (a[i] >> 4) & 15;
  }
  /* each e[i] is between 0 and 15 */
  /* e[63] is between 0 and 7 */

  carry = 0;
  for (i = 0; i < 113; ++i) {
    e[i] += carry;
    carry = e[i] + 8;
    carry >>= 4;
    e[i] -= carry << 4;
  }
  e[113] += carry;
  /* each e[i] is between -8 and 8 */

  ge_p3_0(h);
  for (i = 1; i < 114; i += 2) {
    select(&t, i / 2, e[i]);
    ge_madd(&r, h, &t);
    ge_p1p1_to_p3(h, &r);
  }

  ge_p3_dbl(&r, h);
  ge_p1p1_to_p2(&s, &r);
  ge_dbl(&r, &s);
  ge_p1p1_to_p2(&s, &r);
  ge_dbl(&r, &s);
  ge_p1p1_to_p2(&s, &r);
  ge_dbl(&r, &s);
  ge_p1p1_to_p3(h, &r);

  for (i = 0; i < 114; i += 2) {
    select(&t, i / 2, e[i]);
    ge_madd(&r, h, &t);
    ge_p1p1_to_p3(h, &r);
  }
}

static inline void slide(signed char *r, const unsigned char *a)
{
  int i;
  int b;
  int k;

  for (i = 0; i < 456; ++i)
    r[i] = 1 & (a[i >> 3] >> (i & 7));

  for (i = 0; i < 456; ++i)
    if (r[i]) {
      for (b = 1; b <= 6 && i + b < 456; ++b) {
        if (r[i + b]) {
          if (r[i] + (r[i + b] << b) <= 15) {
            r[i] += r[i + b] << b;
            r[i + b] = 0;
          } else if (r[i] - (r[i + b] << b) >= -15) {
            r[i] -= r[i + b] << b;
            for (k = i + b; k < 456; ++k) {
              if (!r[k]) {
                r[k] = 1;
                break;
              }
              r[k] = 0;
            }
          } else
            break;
        }
      }
    }
}

/*
r = a * A + b * B
where a = a[0]+256*a[1]+...+256^31 a[31].
and b = b[0]+256*b[1]+...+256^31 b[31].
B is the Ed25519 base point (x,4/5) with x positive.
*/

void ge_double_scalarmult_vartime(ge_p2 *r, const unsigned char *a,
                                  const ge_p3 *A, const unsigned char *b)
{
  signed char aslide[456];
  signed char bslide[456];
  ge_cached Ai[8]; /* A,3A,5A,7A,9A,11A,13A,15A */
  ge_p1p1 t;
  ge_p3 u;
  ge_p3 A2;
  int i;

  slide(aslide, a);
  slide(bslide, b);

  ge_p3_to_cached(&Ai[0], A);
  ge_p3_dbl(&t, A);
  ge_p1p1_to_p3(&A2, &t);
  ge_cached_add(&t, &A2, &Ai[0]);
  ge_p1p1_to_cached(&Ai[1], &t);
  ge_cached_add(&t, &A2, &Ai[1]);
  ge_p1p1_to_cached(&Ai[2], &t);
  ge_cached_add(&t, &A2, &Ai[2]);
  ge_p1p1_to_cached(&Ai[3], &t);
  ge_cached_add(&t, &A2, &Ai[3]);
  ge_p1p1_to_cached(&Ai[4], &t);
  ge_cached_add(&t, &A2, &Ai[4]);
  ge_p1p1_to_cached(&Ai[5], &t);
  ge_cached_add(&t, &A2, &Ai[5]);
  ge_p1p1_to_cached(&Ai[6], &t);
  ge_cached_add(&t, &A2, &Ai[6]);
  ge_p1p1_to_cached(&Ai[7], &t);

  ge_p2_0(r);

  for (i = 455; i >= 0; --i) {
    if (aslide[i] || bslide[i])
      break;
  }

  for (; i >= 0; --i) {
    ge_dbl(&t, r);

    if (aslide[i] > 0) {
      ge_p1p1_to_p3(&u, &t);
      ge_cached_add(&t, &u, &Ai[aslide[i] / 2]);
    } else if (aslide[i] < 0) {
      ge_p1p1_to_p3(&u, &t);
      ge_cached_sub(&t, &u, &Ai[(-aslide[i]) / 2]);
    }

    if (bslide[i] > 0) {
      ge_p1p1_to_p3(&u, &t);
      ge_madd(&t, &u, &Bi[bslide[i] / 2]);
    } else if (bslide[i] < 0) {
      ge_p1p1_to_p3(&u, &t);
      ge_msub(&t, &u, &Bi[(-bslide[i]) / 2]);
    }

    ge_p1p1_to_p2(r, &t);
  }
}
