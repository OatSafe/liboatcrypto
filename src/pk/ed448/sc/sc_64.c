#include <stdint.h>

static inline uint64_t load_7(const uint8_t *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  result |= ((uint64_t)in[3]) << 24;
  result |= ((uint64_t)in[4]) << 32;
  result |= ((uint64_t)in[5]) << 40;
  result |= ((uint64_t)in[6]) << 48;
  return result;
}

