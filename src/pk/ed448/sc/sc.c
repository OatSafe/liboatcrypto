#include "sc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STORE8(v, a, b)                                                        \
  {                                                                            \
    (v)[0] = (uint64_t)a;                                                      \
    (v)[1] = (uint64_t)a >> 8;                                                 \
    (v)[2] = (uint64_t)a >> 16;                                                \
    (v)[3] = (uint64_t)(a >> 24) | (uint64_t)(b << 4);                         \
    (v)[4] = (uint64_t)b >> 4;                                                 \
    (v)[5] = (uint64_t)b >> 12;                                                \
    (v)[6] = (uint64_t)b >> 20;                                                \
  }

static uint64_t load_4(const uint8_t *in)
{
  uint64_t result;
  result = (uint64_t)in[0];
  result |= ((uint64_t)in[1]) << 8;
  result |= ((uint64_t)in[2]) << 16;
  result |= ((uint64_t)in[3]) << 24;
  return result;
}

#define CARRY_SIGNED64(a, b, c)                                                \
  {                                                                            \
    c = (a + (int64_t)(1 << 27)) >> 28;                                        \
    b += c;                                                                    \
    a -= c << 28;                                                              \
  }

#define CARRY_SIGNED64_ALL(h, c)                                               \
  {                                                                            \
    CARRY_SIGNED64(h##0, h##1, c);                                             \
    CARRY_SIGNED64(h##1, h##2, c);                                             \
    CARRY_SIGNED64(h##2, h##3, c);                                             \
    CARRY_SIGNED64(h##3, h##4, c);                                             \
    CARRY_SIGNED64(h##4, h##5, c);                                             \
    CARRY_SIGNED64(h##5, h##6, c);                                             \
    CARRY_SIGNED64(h##6, h##7, c);                                             \
    CARRY_SIGNED64(h##7, h##8, c);                                             \
    CARRY_SIGNED64(h##8, h##9, c);                                             \
    CARRY_SIGNED64(h##9, h##10, c);                                            \
    CARRY_SIGNED64(h##10, h##11, c);                                           \
    CARRY_SIGNED64(h##11, h##12, c);                                           \
    CARRY_SIGNED64(h##12, h##13, c);                                           \
    CARRY_SIGNED64(h##13, h##14, c);                                           \
    CARRY_SIGNED64(h##14, h##15, c);                                           \
  }

void sc_muladd(uint8_t *s, const uint8_t *a, const uint8_t *b,
               const uint8_t *c)
{
  int64_t carry;
  int64_t a0 = load_4(a + 0) & UINT64_C(0x0FFFFFFF);
  int64_t a1 = (load_4(a + 3) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a2 = load_4(a + 7) & UINT64_C(0x0FFFFFFF);
  int64_t a3 = (load_4(a + 10) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a4 = load_4(a + 14) & UINT64_C(0x0FFFFFFF);
  int64_t a5 = (load_4(a + 17) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a6 = load_4(a + 21) & UINT64_C(0x0FFFFFFF);
  int64_t a7 = (load_4(a + 24) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a8 = load_4(a + 28) & UINT64_C(0x0FFFFFFF);
  int64_t a9 = (load_4(a + 31) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a10 = load_4(a + 35) & UINT64_C(0x0FFFFFFF);
  int64_t a11 = (load_4(a + 38) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a12 = load_4(a + 42) & UINT64_C(0x0FFFFFFF);
  int64_t a13 = (load_4(a + 45) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t a14 = load_4(a + 49) & UINT64_C(0x0FFFFFFF);
  int64_t a15 = (load_4(a + 52) >> 4) & UINT64_C(0x0FFFFFFF);

  int64_t b0 = load_4(b + 0) & UINT64_C(0x0FFFFFFF);
  int64_t b1 = (load_4(b + 3) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b2 = load_4(b + 7) & UINT64_C(0x0FFFFFFF);
  int64_t b3 = (load_4(b + 10) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b4 = load_4(b + 14) & UINT64_C(0x0FFFFFFF);
  int64_t b5 = (load_4(b + 17) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b6 = load_4(b + 21) & UINT64_C(0x0FFFFFFF);
  int64_t b7 = (load_4(b + 24) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b8 = load_4(b + 28) & UINT64_C(0x0FFFFFFF);
  int64_t b9 = (load_4(b + 31) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b10 = load_4(b + 35) & UINT64_C(0x0FFFFFFF);
  int64_t b11 = (load_4(b + 38) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b12 = load_4(b + 42) & UINT64_C(0x0FFFFFFF);
  int64_t b13 = (load_4(b + 45) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t b14 = load_4(b + 49) & UINT64_C(0x0FFFFFFF);
  int64_t b15 = (load_4(b + 52) >> 4) & UINT64_C(0x0FFFFFFF);

  int64_t c0 = load_4(c + 0) & UINT64_C(0x0FFFFFFF);
  int64_t c1 = (load_4(c + 3) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c2 = load_4(c + 7) & UINT64_C(0x0FFFFFFF);
  int64_t c3 = (load_4(c + 10) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c4 = load_4(c + 14) & UINT64_C(0x0FFFFFFF);
  int64_t c5 = (load_4(c + 17) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c6 = load_4(c + 21) & UINT64_C(0x0FFFFFFF);
  int64_t c7 = (load_4(c + 24) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c8 = load_4(c + 28) & UINT64_C(0x0FFFFFFF);
  int64_t c9 = (load_4(c + 31) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c10 = load_4(c + 35) & UINT64_C(0x0FFFFFFF);
  int64_t c11 = (load_4(c + 38) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c12 = load_4(c + 42) & UINT64_C(0x0FFFFFFF);
  int64_t c13 = (load_4(c + 45) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t c14 = load_4(c + 49) & UINT64_C(0x0FFFFFFF);
  int64_t c15 = (load_4(c + 52) >> 4) & UINT64_C(0x0FFFFFFF);

  int64_t s0;
  int64_t s1;
  int64_t s2;
  int64_t s3;
  int64_t s4;
  int64_t s5;
  int64_t s6;
  int64_t s7;
  int64_t s8;
  int64_t s9;
  int64_t s10;
  int64_t s11;
  int64_t s12;
  int64_t s13;
  int64_t s14;
  int64_t s15;
  int64_t s16;
  int64_t s17;
  int64_t s18;
  int64_t s19;
  int64_t s20;
  int64_t s21;
  int64_t s22;
  int64_t s23;
  int64_t s24;
  int64_t s25;
  int64_t s26;
  int64_t s27;
  int64_t s28;
  int64_t s29;
  int64_t s30;
  int64_t s31;
  int64_t s32;

  s0 = c0 + a0 * b0;
  s1 = c1 + a0 * b1 + a1 * b0;
  s2 = c2 + a0 * b2 + a1 * b1 + a2 * b0;
  s3 = c3 + a0 * b3 + a1 * b2 + a2 * b1 + a3 * b0;
  s4 = c4 + a0 * b4 + a1 * b3 + a2 * b2 + a3 * b1 + a4 * b0;
  s5 = c5 + a0 * b5 + a1 * b4 + a2 * b3 + a3 * b2 + a4 * b1 + a5 * b0;
  s6 = c6 + a0 * b6 + a1 * b5 + a2 * b4 + a3 * b3 + a4 * b2 + a5 * b1 + a6 * b0;
  s7 = c7 + a0 * b7 + a1 * b6 + a2 * b5 + a3 * b4 + a4 * b3 + a5 * b2 +
       a6 * b1 + a7 * b0;
  s8 = c8 + a0 * b8 + a1 * b7 + a2 * b6 + a3 * b5 + a4 * b4 + a5 * b3 +
       a6 * b2 + a7 * b1 + a8 * b0;
  s9 = c9 + a0 * b9 + a1 * b8 + a2 * b7 + a3 * b6 + a4 * b5 + a5 * b4 +
       a6 * b3 + a7 * b2 + a8 * b1 + a9 * b0;
  s10 = c10 + a0 * b10 + a1 * b9 + a2 * b8 + a3 * b7 + a4 * b6 + a5 * b5 +
        a6 * b4 + a7 * b3 + a8 * b2 + a9 * b1 + a10 * b0;
  s11 = c11 + a0 * b11 + a1 * b10 + a2 * b9 + a3 * b8 + a4 * b7 + a5 * b6 +
        a6 * b5 + a7 * b4 + a8 * b3 + a9 * b2 + a10 * b1 + a11 * b0;
  s12 = c12 + a0 * b12 + a1 * b11 + a2 * b10 + a3 * b9 + a4 * b8 + a5 * b7 +
        a6 * b6 + a7 * b5 + a8 * b4 + a9 * b3 + a10 * b2 + a11 * b1 + a12 * b0;
  s13 = c13 + a0 * b13 + a1 * b12 + a2 * b11 + a3 * b10 + a4 * b9 + a5 * b8 +
        a6 * b7 + a7 * b6 + a8 * b5 + a9 * b4 + a10 * b3 + a11 * b2 + a12 * b1 +
        a13 * b0;
  s14 = c14 + a0 * b14 + a1 * b13 + a2 * b12 + a3 * b11 + a4 * b10 + a5 * b9 +
        a6 * b8 + a7 * b7 + a8 * b6 + a9 * b5 + a10 * b4 + a11 * b3 + a12 * b2 +
        a13 * b1 + a14 * b0;
  s15 = c15 + a0 * b15 + a1 * b14 + a2 * b13 + a3 * b12 + a4 * b11 + a5 * b10 +
        a6 * b9 + a7 * b8 + a8 * b7 + a9 * b6 + a10 * b5 + a11 * b4 + a12 * b3 +
        a13 * b2 + a14 * b1 + a15 * b0;
  s16 = a1 * b15 + a2 * b14 + a3 * b13 + a4 * b12 + a5 * b11 + a6 * b10 +
        a7 * b9 + a8 * b8 + a9 * b7 + a10 * b6 + a11 * b5 + a12 * b4 +
        a13 * b3 + a14 * b2 + a15 * b1;
  s17 = a2 * b15 + a3 * b14 + a4 * b13 + a5 * b12 + a6 * b11 + a7 * b10 +
        a8 * b9 + a9 * b8 + a10 * b7 + a11 * b6 + a12 * b5 + a13 * b4 +
        a14 * b3 + a15 * b2;
  s18 = a3 * b15 + a4 * b14 + a5 * b13 + a6 * b12 + a7 * b11 + a8 * b10 +
        a9 * b9 + a10 * b8 + a11 * b7 + a12 * b6 + a13 * b5 + a14 * b4 +
        a15 * b3;
  s19 = a4 * b15 + a5 * b14 + a6 * b13 + a7 * b12 + a8 * b11 + a9 * b10 +
        a10 * b9 + a11 * b8 + a12 * b7 + a13 * b6 + a14 * b5 + a15 * b4;
  s20 = a5 * b15 + a6 * b14 + a7 * b13 + a8 * b12 + a9 * b11 + a10 * b10 +
        a11 * b9 + a12 * b8 + a13 * b7 + a14 * b6 + a15 * b5;
  s21 = a6 * b15 + a7 * b14 + a8 * b13 + a9 * b12 + a10 * b11 + a11 * b10 +
        a12 * b9 + a13 * b8 + a14 * b7 + a15 * b6;
  s22 = a7 * b15 + a8 * b14 + a9 * b13 + a10 * b12 + a11 * b11 + a12 * b10 +
        a13 * b9 + a14 * b8 + a15 * b7;
  s23 = a8 * b15 + a9 * b14 + a10 * b13 + a11 * b12 + a12 * b11 + a13 * b10 +
        a14 * b9 + a15 * b8;
  s24 = a9 * b15 + a10 * b14 + a11 * b13 + a12 * b12 + a13 * b11 + a14 * b10 +
        a15 * b9;
  s25 = a10 * b15 + a11 * b14 + a12 * b13 + a13 * b12 + a14 * b11 + a15 * b10;
  s26 = a11 * b15 + a12 * b14 + a13 * b13 + a14 * b12 + a15 * b11;
  s27 = a12 * b15 + a13 * b14 + a14 * b13 + a15 * b12;
  s28 = a13 * b15 + a14 * b14 + a15 * b13;
  s29 = a14 * b15 + a15 * b14;
  s30 = a15 * b15;
  s31 = 0;
  s32 = 0;

  carry = (s0 + (int64_t)(1 << 27)) >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = (s1 + (int64_t)(1 << 27)) >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = (s2 + (int64_t)(1 << 27)) >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = (s3 + (int64_t)(1 << 27)) >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = (s4 + (int64_t)(1 << 27)) >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = (s5 + (int64_t)(1 << 27)) >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = (s6 + (int64_t)(1 << 27)) >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = (s7 + (int64_t)(1 << 27)) >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = (s8 + (int64_t)(1 << 27)) >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;
  carry = (s15 + (int64_t)(1 << 27)) >> 28;
  s16 += carry;
  s15 -= carry << 28;
  carry = (s16 + (int64_t)(1 << 27)) >> 28;
  s17 += carry;
  s16 -= carry << 28;
  carry = (s17 + (int64_t)(1 << 27)) >> 28;
  s18 += carry;
  s17 -= carry << 28;
  carry = (s18 + (int64_t)(1 << 27)) >> 28;
  s19 += carry;
  s18 -= carry << 28;
  carry = (s19 + (int64_t)(1 << 27)) >> 28;
  s20 += carry;
  s19 -= carry << 28;
  carry = (s20 + (int64_t)(1 << 27)) >> 28;
  s21 += carry;
  s20 -= carry << 28;
  carry = (s21 + (int64_t)(1 << 27)) >> 28;
  s22 += carry;
  s21 -= carry << 28;
  carry = (s22 + (int64_t)(1 << 27)) >> 28;
  s23 += carry;
  s22 -= carry << 28;
  carry = (s23 + (int64_t)(1 << 27)) >> 28;
  s24 += carry;
  s23 -= carry << 28;
  carry = (s24 + (int64_t)(1 << 27)) >> 28;
  s25 += carry;
  s24 -= carry << 28;
  carry = (s25 + (int64_t)(1 << 27)) >> 28;
  s26 += carry;
  s25 -= carry << 28;
  carry = (s26 + (int64_t)(1 << 27)) >> 28;
  s27 += carry;
  s26 -= carry << 28;
  carry = (s27 + (int64_t)(1 << 27)) >> 28;
  s28 += carry;
  s27 -= carry << 28;
  carry = (s28 + (int64_t)(1 << 27)) >> 28;
  s29 += carry;
  s28 -= carry << 28;
  carry = (s29 + (int64_t)(1 << 27)) >> 28;
  s30 += carry;
  s29 -= carry << 28;
  carry = (s30 + (int64_t)(1 << 27)) >> 28;
  s31 += carry;
  s30 -= carry << 28;
  carry = (s31 + (int64_t)(1 << 27)) >> 28;
  s32 += carry;
  s31 -= carry << 28;

  s16 += s32 * 43969588;
  s17 += s32 * 30366549;
  s18 -= s32 * 104682638;
  s19 -= s32 * 10265457;
  s20 += s32 * 96434765;
  s21 -= s32 * 40613262;
  s22 -= s32 * 118569837;
  s23 += s32 * 13465350;
  s24 += s32 * 2;
  s32 = 0;

  s15 += s31 * 43969588;
  s16 += s31 * 30366549;
  s17 -= s31 * 104682638;
  s18 -= s31 * 10265457;
  s19 += s31 * 96434765;
  s20 -= s31 * 40613262;
  s21 -= s31 * 118569837;
  s22 += s31 * 13465350;
  s23 += s31 * 2;
  s31 = 0;

  s14 += s30 * 43969588;
  s15 += s30 * 30366549;
  s16 -= s30 * 104682638;
  s17 -= s30 * 10265457;
  s18 += s30 * 96434765;
  s19 -= s30 * 40613262;
  s20 -= s30 * 118569837;
  s21 += s30 * 13465350;
  s22 += s30 * 2;
  s30 = 0;

  s13 += s29 * 43969588;
  s14 += s29 * 30366549;
  s15 -= s29 * 104682638;
  s16 -= s29 * 10265457;
  s17 += s29 * 96434765;
  s18 -= s29 * 40613262;
  s19 -= s29 * 118569837;
  s20 += s29 * 13465350;
  s21 += s29 * 2;
  s29 = 0;

  s12 += s28 * 43969588;
  s13 += s28 * 30366549;
  s14 -= s28 * 104682638;
  s15 -= s28 * 10265457;
  s16 += s28 * 96434765;
  s17 -= s28 * 40613262;
  s18 -= s28 * 118569837;
  s19 += s28 * 13465350;
  s20 += s28 * 2;
  s28 = 0;

  s11 += s27 * 43969588;
  s12 += s27 * 30366549;
  s13 -= s27 * 104682638;
  s14 -= s27 * 10265457;
  s15 += s27 * 96434765;
  s16 -= s27 * 40613262;
  s17 -= s27 * 118569837;
  s18 += s27 * 13465350;
  s19 += s27 * 2;
  s27 = 0;

  s10 += s26 * 43969588;
  s11 += s26 * 30366549;
  s12 -= s26 * 104682638;
  s13 -= s26 * 10265457;
  s14 += s26 * 96434765;
  s15 -= s26 * 40613262;
  s16 -= s26 * 118569837;
  s17 += s26 * 13465350;
  s18 += s26 * 2;
  s26 = 0;

  s9 += s25 * 43969588;
  s10 += s25 * 30366549;
  s11 -= s25 * 104682638;
  s12 -= s25 * 10265457;
  s13 += s25 * 96434765;
  s14 -= s25 * 40613262;
  s15 -= s25 * 118569837;
  s16 += s25 * 13465350;
  s17 += s25 * 2;
  s25 = 0;

  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;
  carry = (s15 + (int64_t)(1 << 27)) >> 28;
  s16 += carry;
  s15 -= carry << 28;
  carry = (s16 + (int64_t)(1 << 27)) >> 28;
  s17 += carry;
  s16 -= carry << 28;
  carry = (s17 + (int64_t)(1 << 27)) >> 28;
  s18 += carry;
  s17 -= carry << 28;
  carry = (s18 + (int64_t)(1 << 27)) >> 28;
  s19 += carry;
  s18 -= carry << 28;
  carry = (s19 + (int64_t)(1 << 27)) >> 28;
  s20 += carry;
  s19 -= carry << 28;
  carry = (s20 + (int64_t)(1 << 27)) >> 28;
  s21 += carry;
  s20 -= carry << 28;
  carry = (s21 + (int64_t)(1 << 27)) >> 28;
  s22 += carry;
  s21 -= carry << 28;
  carry = (s22 + (int64_t)(1 << 27)) >> 28;
  s23 += carry;
  s22 -= carry << 28;
  carry = (s23 + (int64_t)(1 << 27)) >> 28;
  s24 += carry;
  s23 -= carry << 28;

  s8 += s24 * 43969588;
  s9 += s24 * 30366549;
  s10 -= s24 * 104682638;
  s11 -= s24 * 10265457;
  s12 += s24 * 96434765;
  s13 -= s24 * 40613262;
  s14 -= s24 * 118569837;
  s15 += s24 * 13465350;
  s16 += s24 * 2;
  s24 = 0;

  s7 += s23 * 43969588;
  s8 += s23 * 30366549;
  s9 -= s23 * 104682638;
  s10 -= s23 * 10265457;
  s11 += s23 * 96434765;
  s12 -= s23 * 40613262;
  s13 -= s23 * 118569837;
  s14 += s23 * 13465350;
  s15 += s23 * 2;
  s23 = 0;

  s6 += s22 * 43969588;
  s7 += s22 * 30366549;
  s8 -= s22 * 104682638;
  s9 -= s22 * 10265457;
  s10 += s22 * 96434765;
  s11 -= s22 * 40613262;
  s12 -= s22 * 118569837;
  s13 += s22 * 13465350;
  s14 += s22 * 2;
  s22 = 0;

  s5 += s21 * 43969588;
  s6 += s21 * 30366549;
  s7 -= s21 * 104682638;
  s8 -= s21 * 10265457;
  s9 += s21 * 96434765;
  s10 -= s21 * 40613262;
  s11 -= s21 * 118569837;
  s12 += s21 * 13465350;
  s13 += s21 * 2;
  s21 = 0;

  s4 += s20 * 43969588;
  s5 += s20 * 30366549;
  s6 -= s20 * 104682638;
  s7 -= s20 * 10265457;
  s8 += s20 * 96434765;
  s9 -= s20 * 40613262;
  s10 -= s20 * 118569837;
  s11 += s20 * 13465350;
  s12 += s20 * 2;
  s20 = 0;

  s3 += s19 * 43969588;
  s4 += s19 * 30366549;
  s5 -= s19 * 104682638;
  s6 -= s19 * 10265457;
  s7 += s19 * 96434765;
  s8 -= s19 * 40613262;
  s9 -= s19 * 118569837;
  s10 += s19 * 13465350;
  s11 += s19 * 2;
  s19 = 0;

  s2 += s18 * 43969588;
  s3 += s18 * 30366549;
  s4 -= s18 * 104682638;
  s5 -= s18 * 10265457;
  s6 += s18 * 96434765;
  s7 -= s18 * 40613262;
  s8 -= s18 * 118569837;
  s9 += s18 * 13465350;
  s10 += s18 * 2;
  s18 = 0;

  s1 += s17 * 43969588;
  s2 += s17 * 30366549;
  s3 -= s17 * 104682638;
  s4 -= s17 * 10265457;
  s5 += s17 * 96434765;
  s6 -= s17 * 40613262;
  s7 -= s17 * 118569837;
  s8 += s17 * 13465350;
  s9 += s17 * 2;
  s17 = 0;

  carry = (s1 + (int64_t)(1 << 27)) >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = (s2 + (int64_t)(1 << 27)) >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = (s3 + (int64_t)(1 << 27)) >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = (s4 + (int64_t)(1 << 27)) >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = (s5 + (int64_t)(1 << 27)) >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = (s6 + (int64_t)(1 << 27)) >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = (s7 + (int64_t)(1 << 27)) >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = (s8 + (int64_t)(1 << 27)) >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;
  carry = (s15 + (int64_t)(1 << 27)) >> 28;
  s16 += carry;
  s15 -= carry << 28;

  s0 += s16 * 43969588;
  s1 += s16 * 30366549;
  s2 -= s16 * 104682638;
  s3 -= s16 * 10265457;
  s4 += s16 * 96434765;
  s5 -= s16 * 40613262;
  s6 -= s16 * 118569837;
  s7 += s16 * 13465350;
  s8 += s16 * 2;
  s16 = 0;

  carry = (s0 + (int64_t)(1 << 27)) >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = (s1 + (int64_t)(1 << 27)) >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = (s2 + (int64_t)(1 << 27)) >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = (s3 + (int64_t)(1 << 27)) >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = (s4 + (int64_t)(1 << 27)) >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = (s5 + (int64_t)(1 << 27)) >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = (s6 + (int64_t)(1 << 27)) >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = (s7 + (int64_t)(1 << 27)) >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = (s8 + (int64_t)(1 << 27)) >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;

  carry = (s15 + (int64_t)(1 << 25)) >> 26;
  s0 += carry * 78101261;
  s1 -= carry * 126626091;
  s2 -= carry * 93279523;
  s3 += carry * 64542500;
  s4 -= carry * 110109037;
  s5 -= carry * 77262179;
  s6 += carry * 104575269;
  s7 -= carry * 130851391;
  s8 += carry;
  s15 -= carry << 26;

  carry = s0 >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = s1 >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = s2 >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = s3 >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = s4 >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = s5 >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = s6 >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = s7 >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = s8 >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = s9 >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = s10 >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = s11 >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = s12 >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = s13 >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = s14 >> 28;
  s15 += carry;
  s14 -= carry << 28;

  carry = s15 >> 26;
  s0 += carry * 78101261;
  s1 -= carry * 126626091;
  s2 -= carry * 93279523;
  s3 += carry * 64542500;
  s4 -= carry * 110109037;
  s5 -= carry * 77262179;
  s6 += carry * 104575269;
  s7 -= carry * 130851391;
  s8 += carry;
  s15 -= carry << 26;

  carry = s0 >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = s1 >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = s2 >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = s3 >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = s4 >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = s5 >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = s6 >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = s7 >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = s8 >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = s9 >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = s10 >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = s11 >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = s12 >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = s13 >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = s14 >> 28;
  s15 += carry;
  s14 -= carry << 28;

  STORE8(s, s0, s1);
  STORE8(s + 7, s2, s3);
  STORE8(s + 14, s4, s5);
  STORE8(s + 21, s6, s7);
  STORE8(s + 28, s8, s9);
  STORE8(s + 35, s10, s11);
  STORE8(s + 42, s12, s13);
  STORE8(s + 49, s14, s15);
  memset(s + 56, 0, 1);
}

void sc_reduce(uint8_t *s)
{
  int64_t s0 = load_4(s + 0) & UINT64_C(0x0FFFFFFF);
  int64_t s1 = (load_4(s + 3) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s2 = load_4(s + 7) & UINT64_C(0x0FFFFFFF);
  int64_t s3 = (load_4(s + 10) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s4 = load_4(s + 14) & UINT64_C(0x0FFFFFFF);
  int64_t s5 = (load_4(s + 17) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s6 = load_4(s + 21) & UINT64_C(0x0FFFFFFF);
  int64_t s7 = (load_4(s + 24) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s8 = load_4(s + 28) & UINT64_C(0x0FFFFFFF);
  int64_t s9 = (load_4(s + 31) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s10 = load_4(s + 35) & UINT64_C(0x0FFFFFFF);
  int64_t s11 = (load_4(s + 38) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s12 = load_4(s + 42) & UINT64_C(0x0FFFFFFF);
  int64_t s13 = (load_4(s + 45) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s14 = load_4(s + 49) & UINT64_C(0x0FFFFFFF);
  int64_t s15 = (load_4(s + 52) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s16 = load_4(s + 56) & UINT64_C(0x0FFFFFFF);
  int64_t s17 = (load_4(s + 59) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s18 = load_4(s + 63) & UINT64_C(0x0FFFFFFF);
  int64_t s19 = (load_4(s + 66) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s20 = load_4(s + 70) & UINT64_C(0x0FFFFFFF);
  int64_t s21 = (load_4(s + 73) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s22 = load_4(s + 77) & UINT64_C(0x0FFFFFFF);
  int64_t s23 = (load_4(s + 80) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s24 = load_4(s + 84) & UINT64_C(0x0FFFFFFF);
  int64_t s25 = (load_4(s + 87) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s26 = load_4(s + 91) & UINT64_C(0x0FFFFFFF);
  int64_t s27 = (load_4(s + 94) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s28 = load_4(s + 98) & UINT64_C(0x0FFFFFFF);
  int64_t s29 = (load_4(s + 101) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s30 = load_4(s + 105) & UINT64_C(0x0FFFFFFF);
  int64_t s31 = (load_4(s + 108) >> 4) & UINT64_C(0x0FFFFFFF);
  int64_t s32 = s[112] + (s[113] << 8);
  int64_t carry;

  s16 += s32 * 43969588;
  s17 += s32 * 30366549;
  s18 -= s32 * 104682638;
  s19 -= s32 * 10265457;
  s20 += s32 * 96434765;
  s21 -= s32 * 40613262;
  s22 -= s32 * 118569837;
  s23 += s32 * 13465350;
  s24 += s32 * 2;
  s32 = 0;

  s15 += s31 * 43969588;
  s16 += s31 * 30366549;
  s17 -= s31 * 104682638;
  s18 -= s31 * 10265457;
  s19 += s31 * 96434765;
  s20 -= s31 * 40613262;
  s21 -= s31 * 118569837;
  s22 += s31 * 13465350;
  s23 += s31 * 2;
  s31 = 0;

  s14 += s30 * 43969588;
  s15 += s30 * 30366549;
  s16 -= s30 * 104682638;
  s17 -= s30 * 10265457;
  s18 += s30 * 96434765;
  s19 -= s30 * 40613262;
  s20 -= s30 * 118569837;
  s21 += s30 * 13465350;
  s22 += s30 * 2;
  s30 = 0;

  s13 += s29 * 43969588;
  s14 += s29 * 30366549;
  s15 -= s29 * 104682638;
  s16 -= s29 * 10265457;
  s17 += s29 * 96434765;
  s18 -= s29 * 40613262;
  s19 -= s29 * 118569837;
  s20 += s29 * 13465350;
  s21 += s29 * 2;
  s29 = 0;

  s12 += s28 * 43969588;
  s13 += s28 * 30366549;
  s14 -= s28 * 104682638;
  s15 -= s28 * 10265457;
  s16 += s28 * 96434765;
  s17 -= s28 * 40613262;
  s18 -= s28 * 118569837;
  s19 += s28 * 13465350;
  s20 += s28 * 2;
  s28 = 0;

  s11 += s27 * 43969588;
  s12 += s27 * 30366549;
  s13 -= s27 * 104682638;
  s14 -= s27 * 10265457;
  s15 += s27 * 96434765;
  s16 -= s27 * 40613262;
  s17 -= s27 * 118569837;
  s18 += s27 * 13465350;
  s19 += s27 * 2;
  s27 = 0;

  s10 += s26 * 43969588;
  s11 += s26 * 30366549;
  s12 -= s26 * 104682638;
  s13 -= s26 * 10265457;
  s14 += s26 * 96434765;
  s15 -= s26 * 40613262;
  s16 -= s26 * 118569837;
  s17 += s26 * 13465350;
  s18 += s26 * 2;
  s26 = 0;

  s9 += s25 * 43969588;
  s10 += s25 * 30366549;
  s11 -= s25 * 104682638;
  s12 -= s25 * 10265457;
  s13 += s25 * 96434765;
  s14 -= s25 * 40613262;
  s15 -= s25 * 118569837;
  s16 += s25 * 13465350;
  s17 += s25 * 2;
  s25 = 0;

  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;
  carry = (s15 + (int64_t)(1 << 27)) >> 28;
  s16 += carry;
  s15 -= carry << 28;
  carry = (s16 + (int64_t)(1 << 27)) >> 28;
  s17 += carry;
  s16 -= carry << 28;
  carry = (s17 + (int64_t)(1 << 27)) >> 28;
  s18 += carry;
  s17 -= carry << 28;
  carry = (s18 + (int64_t)(1 << 27)) >> 28;
  s19 += carry;
  s18 -= carry << 28;
  carry = (s19 + (int64_t)(1 << 27)) >> 28;
  s20 += carry;
  s19 -= carry << 28;
  carry = (s20 + (int64_t)(1 << 27)) >> 28;
  s21 += carry;
  s20 -= carry << 28;
  carry = (s21 + (int64_t)(1 << 27)) >> 28;
  s22 += carry;
  s21 -= carry << 28;
  carry = (s22 + (int64_t)(1 << 27)) >> 28;
  s23 += carry;
  s22 -= carry << 28;
  carry = (s23 + (int64_t)(1 << 27)) >> 28;
  s24 += carry;
  s23 -= carry << 28;

  s8 += s24 * 43969588;
  s9 += s24 * 30366549;
  s10 -= s24 * 104682638;
  s11 -= s24 * 10265457;
  s12 += s24 * 96434765;
  s13 -= s24 * 40613262;
  s14 -= s24 * 118569837;
  s15 += s24 * 13465350;
  s16 += s24 * 2;
  s24 = 0;

  s7 += s23 * 43969588;
  s8 += s23 * 30366549;
  s9 -= s23 * 104682638;
  s10 -= s23 * 10265457;
  s11 += s23 * 96434765;
  s12 -= s23 * 40613262;
  s13 -= s23 * 118569837;
  s14 += s23 * 13465350;
  s15 += s23 * 2;
  s23 = 0;

  s6 += s22 * 43969588;
  s7 += s22 * 30366549;
  s8 -= s22 * 104682638;
  s9 -= s22 * 10265457;
  s10 += s22 * 96434765;
  s11 -= s22 * 40613262;
  s12 -= s22 * 118569837;
  s13 += s22 * 13465350;
  s14 += s22 * 2;
  s22 = 0;

  s5 += s21 * 43969588;
  s6 += s21 * 30366549;
  s7 -= s21 * 104682638;
  s8 -= s21 * 10265457;
  s9 += s21 * 96434765;
  s10 -= s21 * 40613262;
  s11 -= s21 * 118569837;
  s12 += s21 * 13465350;
  s13 += s21 * 2;
  s21 = 0;

  s4 += s20 * 43969588;
  s5 += s20 * 30366549;
  s6 -= s20 * 104682638;
  s7 -= s20 * 10265457;
  s8 += s20 * 96434765;
  s9 -= s20 * 40613262;
  s10 -= s20 * 118569837;
  s11 += s20 * 13465350;
  s12 += s20 * 2;
  s20 = 0;

  s3 += s19 * 43969588;
  s4 += s19 * 30366549;
  s5 -= s19 * 104682638;
  s6 -= s19 * 10265457;
  s7 += s19 * 96434765;
  s8 -= s19 * 40613262;
  s9 -= s19 * 118569837;
  s10 += s19 * 13465350;
  s11 += s19 * 2;
  s19 = 0;

  s2 += s18 * 43969588;
  s3 += s18 * 30366549;
  s4 -= s18 * 104682638;
  s5 -= s18 * 10265457;
  s6 += s18 * 96434765;
  s7 -= s18 * 40613262;
  s8 -= s18 * 118569837;
  s9 += s18 * 13465350;
  s10 += s18 * 2;
  s18 = 0;

  s1 += s17 * 43969588;
  s2 += s17 * 30366549;
  s3 -= s17 * 104682638;
  s4 -= s17 * 10265457;
  s5 += s17 * 96434765;
  s6 -= s17 * 40613262;
  s7 -= s17 * 118569837;
  s8 += s17 * 13465350;
  s9 += s17 * 2;
  s17 = 0;

  carry = (s1 + (int64_t)(1 << 27)) >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = (s2 + (int64_t)(1 << 27)) >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = (s3 + (int64_t)(1 << 27)) >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = (s4 + (int64_t)(1 << 27)) >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = (s5 + (int64_t)(1 << 27)) >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = (s6 + (int64_t)(1 << 27)) >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = (s7 + (int64_t)(1 << 27)) >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = (s8 + (int64_t)(1 << 27)) >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;
  carry = (s15 + (int64_t)(1 << 27)) >> 28;
  s16 += carry;
  s15 -= carry << 28;

  s0 += s16 * 43969588;
  s1 += s16 * 30366549;
  s2 -= s16 * 104682638;
  s3 -= s16 * 10265457;
  s4 += s16 * 96434765;
  s5 -= s16 * 40613262;
  s6 -= s16 * 118569837;
  s7 += s16 * 13465350;
  s8 += s16 * 2;
  s16 = 0;

  carry = (s0 + (int64_t)(1 << 27)) >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = (s1 + (int64_t)(1 << 27)) >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = (s2 + (int64_t)(1 << 27)) >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = (s3 + (int64_t)(1 << 27)) >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = (s4 + (int64_t)(1 << 27)) >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = (s5 + (int64_t)(1 << 27)) >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = (s6 + (int64_t)(1 << 27)) >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = (s7 + (int64_t)(1 << 27)) >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = (s8 + (int64_t)(1 << 27)) >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = (s9 + (int64_t)(1 << 27)) >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = (s10 + (int64_t)(1 << 27)) >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = (s11 + (int64_t)(1 << 27)) >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = (s12 + (int64_t)(1 << 27)) >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = (s13 + (int64_t)(1 << 27)) >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = (s14 + (int64_t)(1 << 27)) >> 28;
  s15 += carry;
  s14 -= carry << 28;

  carry = (s15 + (int64_t)(1 << 25)) >> 26;
  s0 += carry * 78101261;
  s1 -= carry * 126626091;
  s2 -= carry * 93279523;
  s3 += carry * 64542500;
  s4 -= carry * 110109037;
  s5 -= carry * 77262179;
  s6 += carry * 104575269;
  s7 -= carry * 130851391;
  s8 += carry;
  s15 -= carry << 26;

  carry = s0 >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = s1 >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = s2 >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = s3 >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = s4 >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = s5 >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = s6 >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = s7 >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = s8 >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = s9 >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = s10 >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = s11 >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = s12 >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = s13 >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = s14 >> 28;
  s15 += carry;
  s14 -= carry << 28;

  carry = s15 >> 26;
  s0 += carry * 78101261;
  s1 -= carry * 126626091;
  s2 -= carry * 93279523;
  s3 += carry * 64542500;
  s4 -= carry * 110109037;
  s5 -= carry * 77262179;
  s6 += carry * 104575269;
  s7 -= carry * 130851391;
  s8 += carry;
  s15 -= carry << 26;

  carry = s0 >> 28;
  s1 += carry;
  s0 -= carry << 28;
  carry = s1 >> 28;
  s2 += carry;
  s1 -= carry << 28;
  carry = s2 >> 28;
  s3 += carry;
  s2 -= carry << 28;
  carry = s3 >> 28;
  s4 += carry;
  s3 -= carry << 28;
  carry = s4 >> 28;
  s5 += carry;
  s4 -= carry << 28;
  carry = s5 >> 28;
  s6 += carry;
  s5 -= carry << 28;
  carry = s6 >> 28;
  s7 += carry;
  s6 -= carry << 28;
  carry = s7 >> 28;
  s8 += carry;
  s7 -= carry << 28;
  carry = s8 >> 28;
  s9 += carry;
  s8 -= carry << 28;
  carry = s9 >> 28;
  s10 += carry;
  s9 -= carry << 28;
  carry = s10 >> 28;
  s11 += carry;
  s10 -= carry << 28;
  carry = s11 >> 28;
  s12 += carry;
  s11 -= carry << 28;
  carry = s12 >> 28;
  s13 += carry;
  s12 -= carry << 28;
  carry = s13 >> 28;
  s14 += carry;
  s13 -= carry << 28;
  carry = s14 >> 28;
  s15 += carry;
  s14 -= carry << 28;

  STORE8(s, s0, s1);
  STORE8(s + 7, s2, s3);
  STORE8(s + 14, s4, s5);
  STORE8(s + 21, s6, s7);
  STORE8(s + 28, s8, s9);
  STORE8(s + 35, s10, s11);
  STORE8(s + 42, s12, s13);
  STORE8(s + 49, s14, s15);
  memset(s + 56, 0, 1);
}
