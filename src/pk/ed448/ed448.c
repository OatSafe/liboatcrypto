#include "mcrypto/pk/ed448.h"
#include "../fe448/fe.h"
#include "sc/sc.h"
#include "ge/ge.h"
#include "mcrypto/hash/sha3.h"
#include "mcrypto/random/system.h"
#include <stdint.h>
#include <string.h>

static void clamp(uint8_t *input)
{
  input[0] &= 0xFC;
  input[55] |= 0x80;
  input[56] = 0;
}

void mcrypto_ed448_kp_from_sk(mcrypto_ed448_keypair *kp, uint8_t sk[57])
{
  mcrypto_sha3_ctx sha3_ctx;
  mcrypto_sha3_init(&sha3_ctx, MCRYPTO_SHAKE256);
  uint8_t tmp[114];

  memcpy(kp->skey, sk, 57);

  mcrypto_sha3_update(&sha3_ctx, sk, 57);
  mcrypto_sha3_final(&sha3_ctx);
  mcrypto_sha3_hash(&sha3_ctx, tmp, 114);

  clamp(tmp);
  mcrypto_ed448_scalarmult_base(tmp, kp->pk.key);
}

int mcrypto_ed448_keygen(mcrypto_ed448_keypair *kp,
                         int (*randombytes)(uint8_t *buffer, size_t size))
{
  uint8_t key[57];

  if (randombytes == NULL) {
    randombytes = mcrypto_sysrandom_randombytes;
  }

  if (randombytes(key, 57) != 0) {
    return -1;
  }

  mcrypto_ed448_kp_from_sk(kp, key);

  return 0;
}

void mcrypto_ed448_scalarmult_base(uint8_t *input, uint8_t *output)
{
  ge_p3 P;
  ge_scalarmult_base(&P, input);
  ge_p3_tobytes(output, &P);
}

/*
 * Copyright (c) 2015 Orson Peters <orsonpeters@gmail.com>
 *
 * The codes in the function 'mcrypto_ed25519_sign' and
 * mcrypto_ed25519_verify are derived from Orson Peters works.
 * Please see NOTICE.
 */

void mcrypto_ed448_sign(uint8_t *sig, const uint8_t *msg, const size_t mlen,
                        const mcrypto_ed448_keypair *kp)
{
  mcrypto_sha3_ctx hash;
  uint8_t hram[114];
  uint8_t r[114];
  uint8_t skey[114];
  ge_p3 R;

  mcrypto_sha3_init(&hash, MCRYPTO_SHAKE256);
  mcrypto_sha3_update(&hash, kp->skey, 57);
  mcrypto_sha3_final(&hash);
  mcrypto_sha3_hash(&hash, skey, 114);
  clamp(skey);

  mcrypto_sha3_init(&hash, MCRYPTO_SHAKE256);
  mcrypto_sha3_update(&hash, skey + 57, 57);
  mcrypto_sha3_update(&hash, msg, mlen);
  mcrypto_sha3_final(&hash);
  mcrypto_sha3_hash(&hash, r, 114);

  sc_reduce(r);
  ge_scalarmult_base(&R, r);
  ge_p3_tobytes(sig, &R);

  mcrypto_sha3_init(&hash, MCRYPTO_SHAKE256);
  mcrypto_sha3_update(&hash, sig, 57);
  mcrypto_sha3_update(&hash, kp->pk.key, 57);
  mcrypto_sha3_update(&hash, msg, mlen);
  mcrypto_sha3_final(&hash);
  mcrypto_sha3_hash(&hash, hram, 114);

  sc_reduce(hram);
  sc_muladd(sig + 57, hram, skey, r);
}

int mcrypto_ed448_verify(const uint8_t *msg, size_t mlen, const uint8_t *sig,
                         const mcrypto_ed448_pubkey *pk)
{
  uint8_t h[114];
  uint8_t checker[57];
  mcrypto_sha3_ctx hash;
  ge_p3 A;
  ge_p2 R;

  /*if (sig[63] & 224) {
    return -1;
  } */

  if (ge_frombytes_negate_vartime(&A, pk->key) != 0) {
    return -1;
  }

  mcrypto_sha3_init(&hash, MCRYPTO_SHAKE256);
  mcrypto_sha3_update(&hash, sig, 57);
  mcrypto_sha3_update(&hash, pk->key, 57);
  mcrypto_sha3_update(&hash, msg, mlen);
  mcrypto_sha3_final(&hash);
  mcrypto_sha3_hash(&hash, h, 114);

  sc_reduce(h);
  ge_double_scalarmult_vartime(&R, h, &A, sig + 57) ;

  ge_p2_tobytes(checker, &R);

  if (memcmp(checker, sig, 57) != 0) {
    return -1;
  }

  return 0;
}
