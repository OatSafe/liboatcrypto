/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __linux__
#define _GNU_SOURCE
#endif

#include "mcrypto/random/system.h"
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

static FILE *fd;

#ifdef __linux__
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>
static int getrandom_available = 0;
#endif

static int is_init = 0;

#ifdef __linux__
static int linux_getrandom(uint8_t *buffer, size_t size)
{
  int read;
  while (size > 0) {
    if (size > 33554431)
      read = syscall(SYS_getrandom, buffer, 33554431, 0);
    else
      read = syscall(SYS_getrandom, buffer, size, 0);

    if (read == -1) {
      if (errno == EINTR || errno == EAGAIN) {
        /* just retry */
        continue;
      }

      /* something wrong happened */
      return -1;
    }

    buffer += read;
    size -= read;
  }

  return 0;
}
#endif /* End __linux__ */

static int getrandom_init()
{
  static const char *devs[] = {
    "/dev/urandom",
#ifndef __linux__
    "/dev/random", /* /dev/random is blocking in linux, we don't use that */
#endif             /* End __linux__ */
    NULL
  };

#ifdef __linux__
  uint8_t buf[50];
  size_t result;

  /* Check whether getrandom available */
  result = linux_getrandom(buf, 50);
  if (result != -1) {

    /* if it is available, just use it*/
    is_init = 1;
    getrandom_available = 1;
    return 0;
  }
#endif /* End __linux__ */

  const char **dev = devs;
  if (fd != NULL) {
    is_init = 1;
    return 0;
  }

  do {
    fd = fopen(*dev, "rb");
    dev++;
  } while (*dev != NULL && fd == NULL);

  if (fd == NULL) {
    return -1;
  }

  /* do not use buffer */
  setbuf(fd, NULL);
  is_init = 1;
  return 0;
}

__attribute__((destructor)) static void destruct()
{
  if (fd != NULL)
    fclose(fd);
  is_init = 0;
#ifdef __linux__
  getrandom_available = 0;
#endif /* End __linux__ */
}

int mcrypto_sysrandom_randombytes(uint8_t *buffer, size_t size)
{
  int res;
  size_t read;

  /* inialized? */
  if (!is_init) {
    res = getrandom_init();
    if (res != 0)
      return -1;
  }

#ifdef __linux__
  /* whether to use getrandom on Linux */
  if (getrandom_available) {
    return linux_getrandom(buffer, size);
  }
#endif /* End __linux__ */

  /* Check for fd */
  if (fd == NULL) {
    getrandom_init();
    if (fd == NULL)
      return -1;
  }

  while (size > 0 && !feof(fd)) {
    /* read from the random device */
    read = fread(buffer, size, 1, fd);
    buffer += read;
    size -= read;
  }

  /* cannot read enough bytes */
  if (size != 0)
    return -1;

  return 0;
}
