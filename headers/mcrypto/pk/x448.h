#ifndef _MCRYPTO_PK_X448_H
#define _MCRYPTO_PK_X448_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint8_t key[56];
} mcrypto_x448_pubkey;

typedef struct {
  uint8_t skey[56];
  mcrypto_x448_pubkey pk;
} mcrypto_x448_keypair;

int mcrypto_x448_keygen(mcrypto_x448_keypair *kp,
                          int (*randombytes)(uint8_t *buffer, size_t size));

void mcrypto_x448_kp_from_sk(mcrypto_x448_keypair *kp, uint8_t sk[56]);
void mcrypto_x448_scalarmult(const uint8_t *input, const uint8_t *scalar,
                               uint8_t *output);
void mcrypto_x448_scalarmult_base(const uint8_t *input, uint8_t *output);

#endif