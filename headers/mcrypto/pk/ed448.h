#ifndef _MCRYPTO_PK_ED448_H
#define _MCRYPTO_PK_ED448_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint8_t key[57];
} mcrypto_ed448_pubkey;

typedef struct {
  uint8_t skey[57];
  mcrypto_ed448_pubkey pk;
} mcrypto_ed448_keypair;

void mcrypto_ed448_kp_from_sk(mcrypto_ed448_keypair *kp, uint8_t sk[57]);
void mcrypto_ed448_scalarmult_base(uint8_t *input, uint8_t *output);
int mcrypto_ed448_keygen(mcrypto_ed448_keypair *kp,
                         int (*randombytes)(uint8_t *buffer, size_t size));
void mcrypto_ed448_sign(uint8_t *sig, const uint8_t *msg, const size_t mlen,
                        const mcrypto_ed448_keypair *kp);
int mcrypto_ed448_verify(const uint8_t *msg, size_t mlen, const uint8_t *sig,
                         const mcrypto_ed448_pubkey *pk);

#endif