#ifndef _MCRYPTO_PK_X25519_H
#define _MCRYPTO_PK_X25519_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint8_t key[32];
} mcrypto_x25519_pubkey;

typedef struct {
  uint8_t skey[32];
  mcrypto_x25519_pubkey pk;
} mcrypto_x25519_keypair;

int mcrypto_x25519_keygen(mcrypto_x25519_keypair *kp,
                          int (*randombytes)(uint8_t *buffer, size_t size));

void mcrypto_x25519_kp_from_sk(mcrypto_x25519_keypair *kp, uint8_t sk[32]);
void mcrypto_x25519_scalarmult(const uint8_t *input, const uint8_t *scalar,
                               uint8_t *output);
void mcrypto_x25519_scalarmult_base(const uint8_t *input, uint8_t *output);

#endif