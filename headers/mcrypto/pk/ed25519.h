#ifndef _MCRYPTO_PK_ED25519_H
#define _MCRYPTO_PK_ED25519_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
  uint8_t key[32];
} mcrypto_ed25519_pubkey;

typedef struct {
  uint8_t skey[32];
  mcrypto_ed25519_pubkey pk;
} mcrypto_ed25519_keypair;

void mcrypto_ed25519_kp_from_sk(mcrypto_ed25519_keypair *kp, uint8_t sk[256]);
void mcrypto_ed25519_scalarmult_base(uint8_t *input, uint8_t *output);
int mcrypto_ed25519_keygen(mcrypto_ed25519_keypair *kp,
                           int (*randombytes)(uint8_t *buffer, size_t size));
void mcrypto_ed25519_sign(uint8_t *sig, const uint8_t *msg, const size_t mlen,
                          const mcrypto_ed25519_keypair *kp);
int mcrypto_ed25519_verify(const uint8_t *msg, size_t mlen, const uint8_t *sig,
                           const mcrypto_ed25519_pubkey *kp);
#endif