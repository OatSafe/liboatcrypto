/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_STREAM_CHACHA20
#define _MCRYPTO_STREAM_CHACHA20
#include <stddef.h>
#include <stdint.h>

#define MCRYPTO_STREAM_CHACHA20_BLOCK_SIZE 64
#define MCRYPTO_STREAM_CHACHA20_KEY_SIZE 32
#define MCRYPTO_STREAM_CHACHA20_NONCE_SIZE 8
#define MCRYPTO_STREAM_XCHACHA20_NONCE_SIZE 24
#define MCRYPTO_STREAM_IEFT_CHACHA20_NONCE_SIZE 12

typedef struct {
  uint32_t state[16];
} mcrypto_chacha20_ctx;

/*
 * initialize the chacha20 context
 */
void mcrypto_chacha20_init(void *ctx);

/*
 * set the (chacha20) key to the chacha20 context
 * the size of the key is 32 bytes.
 */
void mcrypto_chacha20_key(void *ctx, uint8_t *key);

/*
 * set the nonce to the chacha20 context
 * if you want to use the ieft varient of chacha20, 
 * please call mcrypto_chacha20_ieft_nonce instead
 * the size of the nonce is 8 bytes.
 */
void mcrypto_chacha20_nonce(void *ctx, uint8_t *nonce);
/*
 * set the nonce to the chacha20 context
 * if you want to use the ieft varient of chacha20, 
 * please call this function to set the nonce
 * the size of the nonce is 24 bytes.
 */
void mcrypto_chacha20_ieft_nonce(void *ctx, uint8_t *nonce);
void mcrypto_chacha20_counter_qw(void *ctx, uint64_t counter);
void mcrypto_chacha20_counter(void *ctx, uint8_t *counter);
void mcrypto_chacha20_ieft_counterw(void *ctx, uint32_t counter);
void mcrypto_chacha20_ieft_counter(void *ctx, uint8_t *counter);
void mcrypto_chacha20_increase_counter(void *ctx);
void mcrypto_chacha20_ieft_increase_counter(void *ctx);
void mcrypto_xchacha20_setup(void *ctx, uint8_t *key, uint8_t *nonce);
void mcrypto_chacha20_crypt(void *ctx, uint8_t *input, uint8_t *output,
                            uint64_t length);
void mcrypto_chacha20_keystream(void *ctx, uint8_t *output, uint8_t length);
void mcrypto_chacha20_ieft_crypt(void *ctx, uint8_t *input, uint8_t *output,
                                 uint64_t length);
void mcrypto_chacha20_ieft_keystream(void *ctx, uint8_t *output,
                                     uint8_t length);

#endif
