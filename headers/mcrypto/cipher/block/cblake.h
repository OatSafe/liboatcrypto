/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_CIPHER_BLOCK_CBLAKE
#define _MCRYPTO_CIPHER_BLOCK_CBLAKE
#include <stdint.h>

typedef struct {
  uint64_t key[16];
  uint64_t tweak[4];
} mcrypto_cblake_ctx;

void mcrypto_cblake_key(mcrypto_cblake_ctx *ctx, uint8_t *key);
void mcrypto_cblake_tweak(mcrypto_cblake_ctx *ctx, uint8_t *tweak);
void mcrypto_cblake_encrypt(mcrypto_cblake_ctx *ctx, uint8_t *msg, uint8_t *out,
                            uint64_t length);
void mcrypto_cblake_decrypt(mcrypto_cblake_ctx *ctx, uint8_t *msg, uint8_t *out,
                            uint64_t length);

#endif