/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_CIPHER_BLOCK_THREEFISH
#define _MCRYPTO_CIPHER_BLOCK_THREEFISH
#include <stdint.h>

#define MCRYPTO_THREEFISH_CTXS(bit, w)                                         \
  typedef struct {                                                             \
    uint64_t tweak[3];                                                         \
    uint64_t key[w + 1];                                                       \
  } mcrypto_threefish##bit##_ctx;

MCRYPTO_THREEFISH_CTXS(1024, 16)
MCRYPTO_THREEFISH_CTXS(512, 8)
MCRYPTO_THREEFISH_CTXS(256, 4)
#undef MCRYPTO_THREEFISH_CTXS

#define MCRYPTO_THREEFISH_DECLS(bit)                                           \
  void mcrypto_threefish##bit##_keyw(mcrypto_threefish##bit##_ctx *ctx,        \
                                     uint64_t *key);                           \
  void mcrypto_threefish##bit##_key(mcrypto_threefish##bit##_ctx *ctx,         \
                                    uint8_t *key);                             \
  void mcrypto_threefish##bit##_tweakw(mcrypto_threefish##bit##_ctx *ctx,      \
                                       uint64_t *tweak);                       \
  void mcrypto_threefish##bit##_tweak(mcrypto_threefish##bit##_ctx *ctx,       \
                                      uint8_t *tweak);                         \
  void mcrypto_threefish##bit##_encryptw_one(                                  \
      mcrypto_threefish##bit##_ctx *ctx, uint64_t *input, uint64_t *output);   \
  void mcrypto_threefish##bit##_encryptw(mcrypto_threefish##bit##_ctx *ctx,    \
                                         uint64_t *input, uint64_t *output,    \
                                         uint64_t length);                     \
  void mcrypto_threefish##bit##_encrypt_one(mcrypto_threefish##bit##_ctx *ctx, \
                                            uint8_t *plaintext,                \
                                            uint8_t *ciphertext);              \
  void mcrypto_threefish##bit##_encrypt(mcrypto_threefish##bit##_ctx *ctx,     \
                                        uint8_t *plaintext,                    \
                                        uint8_t *ciphertext, uint64_t length); \
  void mcrypto_threefish##bit##_decryptw_one(                                  \
      mcrypto_threefish##bit##_ctx *ctx, uint64_t *input, uint64_t *output);   \
  void mcrypto_threefish##bit##_decrypt_one(mcrypto_threefish##bit##_ctx *ctx, \
                                            uint8_t *ciphertext,               \
                                            uint8_t *plaintext);               \
  void mcrypto_threefish##bit##_decrypt(mcrypto_threefish##bit##_ctx *ctx,     \
                                        uint8_t *ciphertext,                   \
                                        uint8_t *plaintext, uint64_t length);

MCRYPTO_THREEFISH_DECLS(1024)
MCRYPTO_THREEFISH_DECLS(512)
MCRYPTO_THREEFISH_DECLS(256)

#undef MCRYPTO_THREEFISH_DECLS

#endif