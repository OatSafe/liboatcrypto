#ifndef MCRYPTO_BITS_H
#define MCRYPTO_BITS_H

#ifdef __GNUC__
typedef unsigned int uint128_t __attribute__((mode(TI)));
typedef int int128_t __attribute__((mode(TI)));
#define MCRYPTO_HAVE_128BIT
#endif

#if __WORDSIZE == 64 || defined(_M_X64) || defined(__amd64__) ||               \
    defined(__x86_64__) || defined(__ppc64__) || defined(__powerpc64__)
#ifndef MCRYPTO_USE_32BIT
#define MCRYPTO_NATIVE_64BIT
#endif
#endif

#endif