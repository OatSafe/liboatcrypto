/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_CIPHER_H
#define _MCRYPTO_CIPHER_H
#include "mcrypto/cipher/stream/chacha20.h"
#include <stdint.h>

#define MCRYPTO_CIPHER_SIZE_KEY (UINT16_C(1))
#define MCRYPTO_CIPHER_SIZE_BLOCK (UINT16_C(1 << 1))
#define MCRYPTO_CIPHER_SIZE_NONCE (UINT16_C(1 << 2))
#define MCRYPTO_CIPHER_SIZE_IV (UINT16_C(1 << 3))
#define MCRYPTO_CIPHER_SIZE_TWEAK (UINT16_C(1 << 4))
#define MCRYPTO_CIPHER_SIZE_COUNTER (UINT16_C(1 << 5))

#define MCRYPTO_CIPHER_FLAG_BLOCK (UINT64_C(1))
#define MCRYPTO_CIPHER_FLAG_STREAM (UINT64_C(1) << 1)
#define MCRYPTO_CIPHER_FLAG_COUNTER_BASED (UINT64_C(1) << 2)
#define MCRYPTO_CIPHER_FLAG_COUNTER_LITTLE_ENDIAN (UINT64_C(1) << 3)
#define MCRYPTO_CIPHER_FLAG_COUNTER_BIG_ENDIAN (UINT64_C(1) << 4)
#define MCRYPTO_CIPHER_FLAG_CHAINING (UINT64_C(1) << 5)
#define MCRYPTO_CIPHER_FLAG_HAVE_KEY_STREAM (UINT64_C(1) << 6)
#define MCRYPTO_CIPHER_FLAG_PADDING (UINT64_C(1) << 7)
#define MCRYPTO_CIPHER_FLAG_STEALING (UINT64_C(1) << 8)
#define MCRYPTO_CIPHER_FLAG_PARALLELABLE (UINT64_T(1) << 9)

#define MCRYPTO_CIPHER_MODE_ENCRYPT (UINT64_T(1))
#define MCRYPTO_CIPHER_MODE_DECRYPT (UINT64_T(1) << 1)
#define MCRYPTO_CIPHER_MODE_KEYSTREAM (UINT64_T(1) << 2)
#define MCRYPTO_CIPHER_MODE_PARALLEL (UINT64_T(1) << 3)
enum {
  MCRYPTO_CIPHER_ENCRYPT = 0,
  MCRYPTO_CIPHER_DECRYPT,
  MCRYPTO_CIPHER_KEYSTREAM
};

/* Define Structures */
typedef struct mcrypto_cipher_desc mcrypto_cipher_desc;
typedef struct mcrypto_cipher_ctx mcrypto_cipher_ctx;
typedef struct mcrypto_cipher_varient mcrypto_cipher_varient;
typedef struct mcrypto_cipher_parameter mcrypto_cipher_parameter;

struct mcrypto_cipher_varient {
  uint64_t id;
  uint16_t changable;
  uint16_t dynamic;

  /* Default Sizes */
  uint_fast16_t block_size;
  uint_fast16_t key_size;
  uint_fast16_t nonce_size;
  uint_fast16_t iv_size;
  uint_fast16_t tweak_size;
  uint_fast16_t counter_size;

  /* minimum size*/
  uint_fast16_t min_block_size;
  uint_fast16_t min_key_size;
  uint_fast16_t min_nonce_size;
  uint_fast16_t min_iv_size;
  uint_fast16_t min_tweak_size;
  uint_fast16_t min_counter_size;

  /* maximum size*/
  uint_fast16_t max_block_size;
  uint_fast16_t max_key_size;
  uint_fast16_t max_nonce_size;
  uint_fast16_t max_iv_size;
  uint_fast16_t max_tweak_size;
  uint_fast16_t max_counter_size;

  /* dependency */
  uint16_t block_size_dep;
  uint16_t key_size_dep;
  uint16_t nonce_size_dep;
  uint16_t iv_size_dep;
  uint16_t tweak_size_dep;
  uint16_t counter_size_dep;
};

struct mcrypto_cipher_desc {
  /* Cipher name */
  char *name;

  /* cipher flags */
  uint64_t flags;

  /* varients for different sizes */
  mcrypto_cipher_varient **varients;

  /* Basic functions */
  /* init the context of the cipher */
  void *(*init)();
  int (*deinit)(void *ctx);
  int (*setup)(void *ctx,
               uint8_t *key,
               uint8_t *nonce,
               uint8_t *iv,
               uint8_t *counter,
               uint8_t *tweak,
               int mode);

  /* encrypt and encrypt_one should be implement either one of them */
  int (*encrypt)(void *ctx,
                 uint8_t *plaintext,
                 uint8_t *ciphertext,
                 uint64_t length);
  int (*encrypt_one)(void *ctx, uint8_t *plaintext, uint8_t *ciphertext);

  /* decrypt and decrypt_one should be implement either one of them */
  int (*decrypt)(void *ctx,
                 uint8_t *ciphertext,
                 uint8_t *plaintext,
                 uint64_t length);
  int (*decrypt_one)(void *ctx, uint8_t *ciphertext, uint8_t *plaintext);

  /*
   * if the cipher have a differnet encryption/decryption scheme on the last or
   * last two blocks, encrypt_final and decrypt_final must be implement.
   */
  int (*encrypt_final)(void *ctx,
                       uint8_t *plaintext,
                       uint8_t *ciphertext,
                       uint64_t length);

  int (*decrypt_final)(void *ctx,
                       uint8_t *ciphertext,
                       uint8_t *plaintext,
                       uint64_t length);

  /* parameter selection functions */
  /* select a varient */
  int (*select)(void *ctx, uint_fast16_t varient);

  /* select a cipher being chained */
  int (*select_cipher)(void *ctx, mcrypto_cipher_ctx *cipher);

  /* if keysize is zero, return the current selected keysize.
       if keysize is non-zero, return the actual keysize that is same or bigger
       than the paramater if the return is zero, the function failed */
  uint_fast16_t (*keysize)(void *ctx, uint_fast16_t size);
  uint_fast16_t (*blocksize)(void *ctx, uint_fast16_t size);
  uint_fast16_t (*countersize)(void *ctx, uint_fast16_t size);
  uint_fast16_t (*tweaksize)(void *ctx, uint_fast16_t size);
  uint_fast16_t (*noncesize)(void *ctx, uint_fast16_t size);
  uint_fast16_t (*ivesize)(void *ctx, uint_fast16_t size);

  /* get the maximum length of the cipher */
  uint64_t (*maxlength)(void *ctx);

  int (*copy)(void *ctx, void **dst);

  int (*change_mode)(void *ctx, int mode);

  /* change to a new key */
  int (*key)(void *ctx, uint8_t *key);

  /* change to a new nonce */
  int (*nonce)(void *ctx, uint8_t *nonce);

  /* change to a new counter */
  int (*counter)(void *ctx, uint8_t *counter);

  /* change to a new tweak */
  int (*tweak)(void *ctx, uint8_t *tweak);

  /* change to a new iv */
  int (*iv)(void *ctx, uint8_t *tweak);

  /* reset the cipher to the initial state */
  int (*reset)(void *ctx);

  int (*keystream)(void *ctx, uint8_t *stream, uint64_t length);

  /* seek to the block */
  int (*seek)(void *ctx, uint64_t block);

  /* seek depend on the previous blocks */
  int (*seek_input)(void *ctx, uint8_t *input);
  int (*seek_input_size)(void *ctx, uint8_t *input);


};

struct mcrypto_cipher_parameter {
  uint_fast16_t varient;
  uint_fast16_t keysize;
  uint_fast16_t blocksize;
  uint_fast16_t ivsize;
  uint_fast16_t tweaksize;
  uint_fast16_t countersize;
  uint_fast16_t noncesize;
  uint64_t user_defined;
  void* chained_cipher_ctx;
};

struct mcrypto_cipher_ctx {
  mcrypto_cipher_desc *desc;
  mcrypto_cipher_parameter parameter;

  int ready;
  void *ctx; /* pointer to the specific cipher ctx */
};

#endif
