/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO
#define _MCRYPTO

/* Status Codes */

enum {
  MCRYPTO_OK = 0,
  MCRYPTO_ERROR,
  MCRYPTO_NOT_SUPPORTED
};

#endif