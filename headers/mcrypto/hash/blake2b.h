/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_HASH_BLAKE2B_H
#define _MCRYPTO_HASH_BLAKE2B_H

#include <stdint.h>

typedef struct {
  uint64_t h[8];          /* internal state */
  uint64_t compressed[2]; /* counter */
  uint_fast8_t buflen;
  uint8_t buffer[128]; /* the buffer */
  uint8_t hash_length; /* the length of the hash */
} mcrypto_blake2b_ctx;

int mcrypto_blake2b_init(mcrypto_blake2b_ctx *ctx, uint8_t *key, uint8_t keylen,
                         uint8_t hashlen);
void mcrypto_blake2b_update(mcrypto_blake2b_ctx *ctx, uint8_t *msg,
                            uint64_t length);
int mcrypto_blake2b_final(mcrypto_blake2b_ctx *ctx, uint8_t *out,
                          uint8_t out_length);

int mcrypto_blake2b_easy(uint8_t *key, uint8_t key_length, uint8_t *msg,
                         uint64_t length, uint8_t hash_length,
                         uint8_t *hash_out);

#endif