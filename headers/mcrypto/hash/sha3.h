#ifndef _MCRYPTO_HASH_SHA3_H
#define _MCRYPTO_HASH_SHA3_H
#include <stdint.h>
#include <stddef.h>

typedef struct {
  uint64_t state[5][5];
  uint8_t buffer[200];
  uint8_t buflen_pos;
  int varient;
} mcrypto_sha3_ctx;

enum MCRYPTO_SHA3_VARIENT {
    MCRYPTO_SHAKE128 = 0,
    MCRYPTO_SHAKE256,
    MCRYPTO_SHAKE512,
    MCRYPTO_SHA3_224,
    MCRYPTO_SHA3_256,
    MCRYPTO_SHA3_384,
    MCRYPTO_SHA3_512
};

int mcrypto_sha3_init(mcrypto_sha3_ctx *ctx, const unsigned int varient);
void mcrypto_sha3_update(mcrypto_sha3_ctx *ctx, const uint8_t *input, size_t length);
void mcrypto_sha3_final(mcrypto_sha3_ctx *ctx);
void mcrypto_sha3_hash(mcrypto_sha3_ctx *ctx, uint8_t *hash, size_t length);

#endif