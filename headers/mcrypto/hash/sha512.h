/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_HASH_SHA512_H
#define _MCRYPTO_HASH_SHA512_H
#include <stdint.h>
#include <stddef.h>

typedef struct {
  uint64_t h[8];
  uint8_t buflen;
  uint8_t buffer[128];
  uint64_t counter[2];
} mcrypto_sha512_ctx;

void mcrypto_sha512_init(mcrypto_sha512_ctx *ctx);
void mcrypto_sha512_update(mcrypto_sha512_ctx *ctx, const uint8_t *msg,
                          size_t mlen);
void mcrypto_sha512_final(mcrypto_sha512_ctx *ctx, uint8_t output[64]);

#endif