#ifndef _MCRYPTO_UTILS_SAFEBINARY_H
#define _MCRYPTO_UTILS_SAFEBINARY_H
#include <stdint.h>

typedef struct {
  uint32_t flag;
  uint8_t *nonce;
  uint8_t *data;
} mcrypto_safe_binary;


#endif