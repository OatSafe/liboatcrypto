/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Useful macros */
#ifndef _MCRYPTO_MACRO_H
#define _MCRYPTO_MACRO_H

#include "endian.h"
#include <stdint.h>
#include <string.h>

#define ROTL32(v, n) ((uint32_t)(v) << (n) | (uint32_t)(v) >> (32 - (n)))
#define ROTR32(v, n) ((uint32_t)(v) >> (n) | (uint32_t)(v) << (32 - (n)))
#define ROTL64(v, n) ((uint64_t)(v) << (n) | (uint64_t)(v) >> (64 - (n)))
#define ROTR64(v, n) ((uint64_t)(v) >> (n) | (uint64_t)(v) << (64 - (n)))

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define LOAD32_LE _mcrypto_load32_le
#define STORE32_LE _mcrypto_store32_le
#define LOAD64_LE _mcrypto_load64_le
#define STORE64_LE _mcrypto_store64_le

#define LOAD32_BE _mcrypto_load32_be
#define STORE32_BE _mcrypto_store32_be
#define LOAD64_BE _mcrypto_load64_be
#define STORE64_BE _mcrypto_store64_be

inline uint32_t LOAD32_LE(const uint8_t *p)
{
#ifdef _NATIVE_LITTLE_ENDIAN
  uint32_t w;
  memcpy(&w, p, 4);
  return w;
#else
  return ((uint32_t)(p[0]) << 0 | (uint32_t)(p[1]) << 8 |
          (uint32_t)(p[2]) << 16 | (uint32_t)(p[3]) << 24);
#endif
}

inline void STORE32_LE(uint8_t *p, const uint32_t v)
{
#ifdef _NATIVE_LITTLE_ENDIAN
  memcpy(p, &v, 4);
#else
  p[0] = ((uint32_t)v >> 0) & 0xff;
  p[1] = ((uint32_t)v >> 8) & 0xff;
  p[2] = ((uint32_t)v >> 16) & 0xff;
  p[3] = ((uint32_t)v >> 24) & 0xff;
#endif
}

inline uint64_t LOAD64_LE(const uint8_t *p)
{
#ifdef _NATIVE_LITTLE_ENDIAN
  uint64_t w;
  memcpy(&w, p, 8);
  return w;
#else
  return ((uint64_t)((p)[0]) << 0 | (uint64_t)((p)[1]) << 8 |
          (uint64_t)((p)[2]) << 16 | (uint64_t)((p)[3]) << 24 |
          (uint64_t)((p)[4]) << 32 | (uint64_t)((p)[5]) << 40 |
          (uint64_t)((p)[6]) << 48 | (uint64_t)((p)[7]) << 56);
#endif
}

inline void STORE64_LE(uint8_t *p, const uint64_t v)
{
#ifdef _NATIVE_LITTLE_ENDIAN
  memcpy(p, &v, 8);
#else
  (p)[0] = ((uint64_t)(v) >> 0) & 0xff;
  (p)[1] = ((uint64_t)(v) >> 8) & 0xff;
  (p)[2] = ((uint64_t)(v) >> 16) & 0xff;
  (p)[3] = ((uint64_t)(v) >> 24) & 0xff;
  (p)[4] = ((uint64_t)(v) >> 32) & 0xff;
  (p)[5] = ((uint64_t)(v) >> 40) & 0xff;
  (p)[6] = ((uint64_t)(v) >> 48) & 0xff;
  (p)[7] = ((uint64_t)(v) >> 56) & 0xff;
#endif
}

inline uint32_t LOAD32_BE(const uint8_t *p)
{
#ifdef _NATIVE_BIG_ENDIAN
  uint32_t w;
  memcpy(&w, p, 4);
  return w;
#else
  return ((uint32_t)(p[3]) << 0 | (uint32_t)(p[2]) << 8 |
          (uint32_t)(p[1]) << 16 | (uint32_t)(p[0]) << 24);
#endif
}

inline void STORE32_BE(uint8_t *p, const uint32_t v)
{
#ifdef _NATIVE_BIG_ENDIAN
  memcpy(p, &v, 4);
#else
  p[3] = ((uint32_t)v >> 0) & 0xff;
  p[2] = ((uint32_t)v >> 8) & 0xff;
  p[1] = ((uint32_t)v >> 16) & 0xff;
  p[0] = ((uint32_t)v >> 24) & 0xff;
#endif
}

inline uint64_t LOAD64_BE(const uint8_t *p)
{
#ifdef _NATIVE_BIG_ENDIAN
  uint64_t w;
  memcpy(&w, p, 8);
  return w;
#else
  return ((uint64_t)((p)[7]) << 0 | (uint64_t)((p)[6]) << 8 |
          (uint64_t)((p)[5]) << 16 | (uint64_t)((p)[4]) << 24 |
          (uint64_t)((p)[3]) << 32 | (uint64_t)((p)[2]) << 40 |
          (uint64_t)((p)[1]) << 48 | (uint64_t)((p)[0]) << 56);
#endif
}

inline void STORE64_BE(uint8_t *p, const uint64_t v)
{
#ifdef _NATIVE_BIG_ENDIAN
  memcpy(p, &v, 8);
#else
  (p)[7] = ((uint64_t)(v) >> 0) & 0xff;
  (p)[6] = ((uint64_t)(v) >> 8) & 0xff;
  (p)[5] = ((uint64_t)(v) >> 16) & 0xff;
  (p)[4] = ((uint64_t)(v) >> 24) & 0xff;
  (p)[3] = ((uint64_t)(v) >> 32) & 0xff;
  (p)[2] = ((uint64_t)(v) >> 40) & 0xff;
  (p)[1] = ((uint64_t)(v) >> 48) & 0xff;
  (p)[0] = ((uint64_t)(v) >> 56) & 0xff;
#endif
}

#endif