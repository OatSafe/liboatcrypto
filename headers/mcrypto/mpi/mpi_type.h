/*
 * Copyright 2017 Yan-Jie Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#ifndef _MCRYPTO_MPI_MPI_TYPE_H
#define _MCRYPTO_MPI_MPI_TYPE_H
#include <stdint.h>
#include <stddef.h>

typedef uint16_t mcrypto_mpi_uhint_t;
typedef uint32_t mcrypto_mpi_uint_t;
typedef uint64_t mcrypto_mpi_word_t;

#define UHINT_SIZE sizeof(mcrypto_mpi_uhint_t);
#define UINT_SIZE sizeof(mcrypto_mpi_uint_t)
#define WORD_SIZE sizeof(mcrypto_mpi_word_t)

typedef struct {

    /* the limbs allocated */
    size_t nlimbs;

    /* indicate negative */
    uint8_t neg;

    /* Flags */
    uint16_t flags;

    /* chunks */
    mcrypto_mpi_uint_t* limbs;

} mcrypto_mpi;

#endif
