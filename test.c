#include <mcrypto/pk/ed448.h>
#include <mcrypto/random/system.h>
#include <stdio.h>

int main()
{
  mcrypto_ed448_keypair kp;
  uint8_t msg[12] = { 0 };
  uint8_t sk[57] = {0};
  uint8_t sig[114];
  int result;

  for (int i = 0; i < 20000; i++) {
    mcrypto_ed448_kp_from_sk(&kp, sk);
    mcrypto_ed448_sign(sig, msg, 12, &kp);
    result = mcrypto_ed448_verify(msg, 12, sig, &kp.pk);
    if (result != 0)
      printf("Error!\n");
  }
  return 0;
}