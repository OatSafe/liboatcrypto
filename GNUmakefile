MAKEFLAGS += --no-print-directory
MKDIR = mkdir -p
RM = rm -rf
MV = mv
STRIP = strip

SRCS = cipher/stream/chacha20.c \
       cipher/block/threefish.c \
       hash/blake2b.c \
       hash/sha512.c \
	   hash/sha3.c \
       cipher/block/cblake.c \
       pk/fe25519/fe.c \
       pk/ed25519/ge/ge.c \
       pk/ed25519/ed25519.c \
	   pk/ed25519/sc/sc.c \
	   pk/ed25519/sc/sc_64.c \
	   pk/fe448/fe.c \
	   pk/ed448/ge/ge.c \
	   pk/ed448/ed448.c \
	   pk/ed448/sc/sc.c \
	   pk/x25519/x25519.c \
	   pk/x25519/scalarmult.c \
	   pk/x448/x448.c \
	   pk/x448/scalarmult.c \
       random/system.c \
	   internal.c

OBJS = $(addprefix obj/, $(SRCS:.c=.o))
DEP = $(OBJS:.o=.d)

.PHONY = all clean

CFLAGS += -Iheaders -fPIC -O3 -Wall -std=gnu99
LDFLAGS += -O3

LIBMCRYPTO := bin/libmcrypto.so


default: all

-include $(DEP)

obj/%.o : src/%.c
	@echo "  Compiling    $<"
	@$(MKDIR) $(dir $@)
	@$(CC) -c $(CFLAGS) -MMD -o $@ $<


$(LIBMCRYPTO) : $(OBJS)
	@echo "  Linking      $@"
	@$(MKDIR) $(dir $@)
	@$(CC) $^ $(LDFLAGS) -shared -o $@

all: $(LIBMCRYPTO)

clean:
	@echo Cleaning...
	@$(RM) obj bin
